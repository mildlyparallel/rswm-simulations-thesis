#!/bin/bash

set -ex

sim="./build-native/rswmsimul"
sim_args="-r 5"

dags_dir="${1:-dags/}"

output_dir="results"
results_all="$output_dir/results.csv"

rm -rf "$results_all" || true

for cf in $(find "$dags_dir" -name combs.txt)
do
	dag="$(dirname "$cf")"
	dag_rel=$(realpath --relative-to "$dags_dir" "$dag")

	out="$output_dir/$dag_rel"

	mkdir -p "$out"

	$sim --dag "$dag" --output "$out" $sim_args

	dag_res_all="$out/results.csv"

	cat "$out"/results-*csv > "$dag_res_all"

	nrows=$(cat "$dag_res_all" | wc -l)
	nrows=$(( $nrows - 1))

	echo dag > res.tmp
	printf "\"$dag\"\n%.0s" $(seq 1 $nrows) >> res.tmp

	paste -d, res.tmp "$dag_res_all" > res2.tmp
	mv res2.tmp $dag_res_all

	rm -rf res.tmp

	if [ -e "$results_all" ] 
	then
		cat "$dag_res_all" | tail -n1 >> "$results_all"
	else
		cp "$dag_res_all" "$results_all"
	fi
done

