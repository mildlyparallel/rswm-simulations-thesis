#!/usr/bin/env Rscript

library("optparse")

option_list = list(
	make_option(
		c("-i", "--input"),
		type="character",
		default='results.csv',
		help="Simulation results",
		metavar="character"
	),

	make_option(
		c("-o", "--output"),
		type="character",
		default=".",
		help="Output image [default= %default]",
		metavar="character"
	)
);
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

cat('Reading', opt$input, '\n')

data <- read.csv(opt$input)

rho <- data[, 1:3]
meta <- colnames(data)[1:3]
strategies <- colnames(data)[-(1:3)]
strategies <- strategies[-which(strategies == "optimal")]

rho_all <- NULL

for (stg in strategies) {
	r <- data[, stg] / data$optimal
	rho <- cbind(rho, r)

	ra <- c(min(r), mean(r), max(r))
	rho_all <- rbind(rho_all, ra)
}

colnames(rho) <- c(meta, strategies)
colnames(rho_all) <- c('min', 'mean', 'max')
rownames(rho_all) <- strategies

outp <- paste0(opt$output, "/rho.csv")
cat('Writing', outp, '\n')
write.csv(rho, outp)

outp <- paste0(opt$output, "/rho-all.csv")
cat('Writing', outp, '\n')
write.csv(rho_all, outp)

outp <- paste0(opt$output, "/rho-max.csv")
cat('Writing', outp, '\n')
write.csv(t(rho_all[, 'max']), outp, row.names = F)

outp <- paste0(opt$output, "/rho-avg.csv")
cat('Writing', outp, '\n')
write.csv(t(rho_all[, 'mean']), outp, row.names = F)
