#!/bin/bash


src="$(realpath "$(dirname "$0")")"

inpdir="results"
outdir=""
plot_all_systems=false
plot_outliers=false

OPTIND=1
while getopts "h?i:o:sl" opt; do
	case "$opt" in
		h|\?)
			echo "$0 -i <input_dir> -o <output_idr> [-s] [-l] [-- sys_id...]"
			exit 0
			;;
		i)
			inpdir="$OPTARG"
			;;
		o)
			outdir="$OPTARG"
			;;
		l)
			plot_outliers=true
			;;
		s)
			plot_all_systems=true
			;;
	esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

[ -z "$outdir" ] && outdir="$inpdir"

mkdir -p "$outdir"

cat $inpdir/results-*.csv > $inpdir/results.csv

# rm -rf "$outdir/*.png"

$src/plot-ratio-vs-size.R -i "$inpdir/results.csv" -o "$outdir/tp-ratio-vs-size.png" --outliers "$outdir/outliers.csv"

function plot_system() {
	n="$(basename "$1")"
	$src/plot-system.R -i "$1" -o "${outdir}/${n/.csv/.png}"
}

if $plot_all_systems
then
	for f in $inpdir/system-*.csv
	do
		plot_system "$f"
	done
elif $plot_outliers
then
	for i in $(cat "$outdir/outliers.csv" | tail -n+2 | head -n 10)
	do
		f="$inpdir/system-$i.csv"
		plot_system "$f"
	done
else
	for f in $@
	do
		plot_system "$f"
	done
fi

