This repo contains numerical simulations code for MDPI paper.

## Compilation

The project depends on `meson` build system and `lpsolve55` library.

To compile it run:

```
mkdir build && cd build
meson ..
meson compile
```

After compilation, you will get an executable in `build` directory.

## Usage

The following CLI arguemnts are supported:

```
  -r, --runs N                  Number of input systems generated for each paramter values set
  -o, --output DIR              Output directory for input systems and simul results
  --seed N                      Random seed value
  --tasks-min tasks             Min value for the range of the number of task
  --tasks-max tasks             Max value for the range of the number of task
  --work-min W                  Min value for work units distribution
  --work-max W                  Max value for work units distribution
  --slope-scale-min K           Min value for speed distribution
  --slope-scale-max K           Max value for speed distribution
```

After running simulation, it produces the following files in the output directory (`--output <DIR>`):

* `input.dat` -- input value of the simulations run
* `system-*.csv` -- generated input systems (vector b, base speed (in ideal cond.) and matrix A)
* `speedup-task-*.csv` -- a list of task speed values in each combination
* `speedup-comb-*.csv` -- a list of combinations speed values
* `results-*.dat` -- results produced by each CPU thread

Results in the paper were produced by running 

```bash
./build/rswmsimul -r 200 \
	--tasks-min 10 --tasks-max 11 \
	--slope-scale-min $min_slope --slope-scale-max $max_slope
```

Where `$min_slope` and `$max_slope` could take the following values:

* (0.45; 0.55)
* (0.55; 0.65)
* (0.65; 0.75)
* (0.75; 0.85)
* (0.85; 0.95)

# Acknowledgements 

Research has been supported by the RFBR grant No. 19-37-90138.

