#pragma once

#include <vector>
#include <fstream>

class Strategy;
class SystemStationary;

class Runner
{
public:
	Runner();

	virtual ~Runner();

	void add(Strategy *strategy);

	void open(const std::string &filepath);

	void print_header();

	void run(const SystemStationary &system, bool is_fairness = false);

protected:

private:
	std::vector<Strategy *> m_strategies;

	std::ofstream m_out;
};
