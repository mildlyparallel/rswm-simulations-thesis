#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFCSP : public StrategyOffline
{
public:
	StrategyFCSP();

	virtual ~StrategyFCSP();

	virtual void start();

	virtual const char *name() const;

protected:

private:
};
