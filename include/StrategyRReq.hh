#pragma once

#include "StrategyOffline.hh"

class StrategyRReq : public StrategyOffline
{
public:
	StrategyRReq();

	virtual ~StrategyRReq();

	virtual void start();

	virtual const char *name() const;

	void set_time_unit(double time_unit);

	double get_estimate() const;

protected:

private:
	double m_time_unit;

};

