#pragma once

#include <iostream>
#include <bitset>
#include <set>
#include <cassert>
#include <cstdint>

template <size_t N>
class Bitset
{
public:
	inline static const constexpr size_t max_size = N;

	Bitset() {};

	Bitset(const std::initializer_list<size_t> &ids)
	{
		for (auto i : ids) {
			assert(i < N);
			m_data[i] = true;
		}
	}

	inline
	void set(size_t i, bool v = true)
	{
		assert(i < size());
		m_data[i] = v;
	}

	inline
	void clear(size_t i)
	{
		set(i, false);
	}

	inline
	void clear()
	{
		m_data.reset();
	}

	inline
	void set_all(size_t n = max_size)
	{
		for (size_t i = 0; i < n; ++i)
			m_data[i] = true;
	}

	inline
	uint64_t to_ullong() const
	{
		return m_data.to_ullong();
	}

	inline
	bool get(size_t i) const
	{
		assert(i < size());
		return m_data[i];
	}

	inline
	size_t count() const
	{
		return m_data.count();
	}

	inline
	bool empty() const
	{
		return count() == 0;
	}

	inline
	bool operator[] (size_t i) const
	{
		return get(i);
	}

	static constexpr size_t size() {
		return N;
	}

	bool operator== (const Bitset &other) const {
		for (size_t i = 0; i < size(); ++i) {
			if (m_data[i] != other.m_data[i])
				return false;
		}

		return true;
	}

	bool operator< (const Bitset &other) const {
		for (size_t i = 0; i < size(); ++i) {
			if (m_data[i] == other.m_data[i])
				continue;
			if (m_data[i] == false && other.m_data[i] == true)
				return true;
			return false;
		}

		return false;
	}

	inline
	bool operator() (const Bitset &lhs, const Bitset &rhs) const {
		return lhs < rhs;
	}

	inline
	void swap(size_t i, size_t j)
	{
		bool t = m_data[i];
		m_data[i] = m_data[j];
		m_data[j] = t;
	}

	void reverse(size_t st, size_t en)
	{
		while ((st != en) && (st != --en)) {
			swap(st++, en);
		}
	}

	bool next_permutation(size_t st = 0, size_t en = 0)
	{
		if (en == 0)
			en = N;

		if (st == en)
			return false;

		size_t i = en;
		if (st == --i)
			return false;

		while (true) {
			size_t i1, i2;

			i1 = i;
			if (m_data[--i] < m_data[i1]) {
				i2 = en;

				while (!(m_data[i] < m_data[--i2]));

				swap(i, i2);
				reverse(i1, en);

				return true;
			}

			if (i == st) {
				reverse(st, en);
				return false;
			}
		}
	}

	bool next_combination(size_t st = 0, size_t en = 0)
	{
		if (en == 0)
			en = N;

		for (size_t ii = en; ii > st; ii--) {
			size_t i = ii - 1;

			if (m_data[i] == false) {
				m_data[i] = true;
				return true;
			}

			m_data[i] = false;
		}

		return false;
	}

	bool next_combination_by_size(size_t st = 0, size_t en = 0)
	{
		if (en == 0)
			en = N;

		if (!empty() && next_permutation(st, en))
			return true;

		size_t n = 0;
		for (size_t i = st; i < en; ++i)
			n += m_data[i];

		if (n == (en - st))
			return false;

		n++;

		for (size_t i1 = en; i1 > st; i1--) {
			size_t i = i1 - 1;
			if (n > 0) {
				m_data[i] = 1;
				n--;
			} else {
				m_data[i] = 0;
			}
		}

		return true;
	}

	inline
	bool subset_of(const Bitset<N> &other) const
	{
		return (other.m_data & m_data) == m_data;
	}

	inline
	Bitset<N> operator&(const Bitset<N> &other) const
	{
		Bitset<N> b;
		b.m_data = other.m_data & m_data;
		return b;
	}

	inline
	void operator&=(const Bitset<N> &other)
	{
		m_data &= other.m_data;
	}

	inline
	void operator&=(size_t i)
	{
		Bitset<N> other({i});
		m_data &= other.m_data;
	}

	inline
	Bitset<N> operator|(const Bitset<N> &other) const
	{
		Bitset<N> b;
		b.m_data = other.m_data | m_data;
		return b;
	}

	inline
	void operator|=(const Bitset<N> &other)
	{
		m_data |= other.m_data;
	}

	inline
	void operator|=(size_t i)
	{
		Bitset<N> other({i});
		m_data |= other.m_data;
	}

	inline
	Bitset<N> operator~() const
	{
		Bitset<N> b;
		b.m_data = ~m_data;
		return b;
	}

	inline
	operator bool() const
	{
		return m_data.count() > 0;
	}

	template <typename T>
	inline
	bool subset_of_any(const T &sets) const
	{
		for (const auto &s : sets) {
			if (subset_of(s))
				return true;
		}

		return false;
	}

protected:

private:
	std::bitset<N> m_data;;
};

template<size_t N>
std::ostream &operator<<(std::ostream &os, const Bitset<N> b)
{
	bool comma = false;
	os << "{";
	for (size_t i = 0; i < N; ++i) {
		if (!b[i])
			continue;
		if (comma)
			os << ",";
		os << i;
		comma = true;
	}
	os << "}";
	return os;

}
