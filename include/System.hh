#pragma once

#include <vector>
#include <set>

#include "Task.hh"

class TaskGraph;

class System
{
public:
	System();

	System(size_t n);

	virtual ~System();

	inline
	Task &operator[](size_t i)
	{
		return m_tasks[i];
	}

	inline
	const Task &operator[](size_t i) const
	{
		return m_tasks[i];
	}

	void init_total_work(std::function<double()> fn);

	void init_total_work(std::function<double(const Task &)> fn);

	void init_total_work(double p, double dp = 0);

	void init_total_work(size_t i, double p);

	void init_priority(std::function<double()> fn);

	void init_priority(std::function<double(const Task &)> fn);

	void init_priority(double p, double dp = 0);

	void init_priority(size_t i, double p);

	void set_precedence(const TaskGraph &graph);

	const std::vector<Task> &tasks() const;

	size_t size() const;

	unsigned get_id() const;

	void set_id(unsigned id);

	const std::vector<std::vector<Taskset>> &nonblocking_sets() const;

	const std::vector<Taskset> &feasible_combinations() const;

	void set_seed(int seed);

	int get_seed() const;

protected:
	unsigned m_id;

	std::vector<Task> m_tasks;

	std::vector<Taskset> m_feasible_ts;

	std::vector<std::vector<Taskset>> m_nonblocking_sets;

	int m_seed = 0;
};

