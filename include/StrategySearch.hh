#pragma once

#include <vector>
#include <array>
#include <set>
#include <tuple>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"
#include "Interpolator.hh"

class StrategySearch : public StrategyOffline
{
public:
	StrategySearch();

	virtual ~StrategySearch();

	virtual void start();

	virtual const char *name() const;

	void solve(const SystemStationary &tasks);

	void set_quartile(double q);

	void set_explore_coeff(double e);

	void set_fairness(double f);

	void set_efficiency(double e);

	void set_interval(double dt);

	void set_name(const std::string &name);

	void open_log_file(const std::string &p);

protected:

private:
	inline static double m_speed_eps = 1e-17;

	static double dnorm(double x, double a, double v);

	static double pnorm(double x);

	void init();

	void collect_all_ideal();

	double find_max_acq_fun() const;

	double measure_exact(Taskset ts) const;

	std::tuple<double, double> measure(Taskset ts) const;

	double measure_ucb(Taskset ts) const;

	double expected_improvement(Taskset ts) const;

	void run_and_measure(Taskset ts);

	Taskset run_next();

	std::tuple<double, double> measure_est_error() const;

	double m_fairness = 0;

	double m_efficiency = 0;

	double m_time_interval = 1;

	std::vector<Interpolator> m_interpolators;

	std::vector<double> m_speed_sum;

	double m_last_acq_fun_val = 0;

	double m_quratile = 0.1;

	double m_explore = 0;

	std::string m_name;

	std::ofstream m_log;

	std::set<Taskset> m_measured_ts;
};

