#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFT : public StrategyOffline
{
public:
	StrategyFT();

	virtual ~StrategyFT();

	virtual void start();

	virtual const char *name() const;

protected:

private:
};
