#pragma once

#include <functional>

#include "System.hh"

class SystemDynamic : public System
{
public:
	SystemDynamic(size_t n);

	virtual ~SystemDynamic();

	void init_base_rate(Task::rate_fn rate);

	void init_rate_scale(std::function<Task::rate_fn (taskset)> scale_gen_fn);

	void init_rate_scale(std::function<Task::rate_fn (taskset, const Task &)> scale_gen_fn);

protected:

private:
};

