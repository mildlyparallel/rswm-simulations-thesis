#pragma once

#include <vector>
#include <map>
#include <ostream>
#include <set>
#include <limits>
#include <functional>

#include "Task.hh"

class CombinationGraph
{
public:
	static const constexpr size_t npos = std::numeric_limits<size_t>::max();

	void add_node();

	void add_edge(size_t u, size_t v);

	bool have_loop() const;

	void remove_edges(size_t u);

	inline
	size_t nr_nodes() const {
		return m_edges.size();
	}

	inline
	size_t nr_edges() const {
		return m_nr_edges;
	}

private:
	void dfs(
		size_t u,
		std::function<bool (size_t u, size_t v)> enter_fn,
		std::function<void (size_t u)> leave_fn = nullptr
	) const;

	size_t m_nr_edges = 0;

	std::vector<std::vector<size_t> > m_edges;
};
