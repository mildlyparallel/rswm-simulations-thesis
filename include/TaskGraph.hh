#pragma once

#include <vector>
#include <random>
#include <map>
#include <ostream>
#include <string>
#include <set>
#include <tuple>

#include "Task.hh"
#include "Graph.hh"
#include "CombinationGraph.hh"

class TaskGraph : public Graph
{
public:
	// using CombinationsList = std::vector<std::vector<Nodeset>>;

	virtual std::string get_node_label(size_t u) const;

	void add_task(const Task &task);

	const Task &operator[](size_t i) const
	{
		assert(i < m_tasks.size());
		return m_tasks[i];
	}

	void add_tasks_no_prec(size_t n);

	void add_tasks(size_t n);

	void find_any_lockfree(std::default_random_engine &gen);

	void list_feasible();

	const std::vector<Taskset> &get_feasible() const;

	const std::set<Taskset> &get_lockfree() const;

	void read_dot(const std::string &p);

protected:

private:
	// void create_combination_graph();

	// void fvs_to_combinations();

	// void find_loops();

	// void find_fvs();

	void write_combinations(const std::string &file) const;

	void read_combinations(const std::string &file);

	void write_meta(const std::string &file) const;

	std::vector<Task> m_tasks;

	std::vector<Taskset> m_feasible;
	std::set<Taskset> m_lockfree;

	// size_t m_nr_loops = 0;

	// CombinationGraph m_comb_graph;

	// CombinationsList m_combinations;
};

