#pragma once

#include <functional>
#include <bitset>
#include <map>

#include "Bitset.hh"

using Taskset = Bitset<128>;

class Task
{
public:
	inline static const double WORK_EPS = 1e-6;
	inline static const double RATE_EPS = 1e-6;
	static const constexpr size_t RATE_LOG_SIZE = 3;

	static const constexpr size_t MAX_TASKS = 15;

	using rate_fn = std::function<double (double)>;

	Task();

	virtual ~Task();

	void set_id(size_t i);

	size_t get_id() const;

	void set_work_total(double w);

	double get_work_total() const;

	void set_base_rate_fn(rate_fn fn);

	double get_base_rate(double w) const;

	void set_rate_scale(Taskset tasks, double v);

	void set_rate_scale_fn(Taskset tasks, rate_fn fn);

	double get_rate_scale(Taskset tasks, double w) const;

	double get_rate(Taskset tasks, double w) const;

	void set_release_time(double r);

	double get_release_time() const;

	void set_priority(double p);

	double get_priority() const;

	void set_successors(Taskset tasks);

	Taskset get_successors() const;

protected:
	double m_work_total = 0;

	double m_release_time = 0;

	double m_priority = 1;

private:
	size_t m_id = 0;

	rate_fn m_base_rate_fn;

	Taskset m_successors;

	std::map<Taskset, rate_fn> m_rate_scale;
};
