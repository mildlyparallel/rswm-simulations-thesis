#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFair : public StrategyOffline
{
public:
	StrategyFair();

	virtual ~StrategyFair();

	virtual void start();

	virtual const char *name() const;

	void set_interval(double dt);

protected:

private:
	std::vector<std::pair<Taskset, double>> solve() const;

	static double normalize(std::vector<std::pair<Taskset, double>> &ans);

	static double normalize(std::vector<double> &ans);

	double m_time_interval = 1;
};
