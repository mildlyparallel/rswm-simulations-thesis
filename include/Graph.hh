#pragma once

#include <vector>
#include <string>
#include <mutex>
#include <ostream>
#include <set>
#include <functional>

#include "Bitset.hh"

class Graph
{
public:
	using Nodeset = Bitset<128>;

	Graph(size_t nodes = 0);

	virtual ~Graph();

	inline
	size_t nr_nodes() const {
		return m_edges.size();
	}

	inline
	size_t nr_edges() const {
		return m_nr_edges;
	}

	size_t add_node();

	void add_edge(size_t u, size_t v);

	void remove_edges(size_t u);

	bool have_edge(size_t u, size_t v) const;

	void reduce();

	void complement();

	bool reachable(size_t u, size_t v) const;

	bool reachable(const Nodeset &nodes) const;

	bool is_leaf(size_t u) const;

	bool is_leaf(const Nodeset &nodes) const;

	bool is_root(size_t v) const;

	bool is_root(const Nodeset &nodes) const;

	bool reachable_any(const Nodeset &u, const Nodeset &v) const;

	void print_dot_directed(
		const std::string &file,
		const Nodeset &filter = {}
	) const;

	void write(const std::string &file) const;

	void read(const std::string &file);

	bool is_tree(const Nodeset &nodes) const;

	const std::vector<Nodeset> &list_maximal_fvs();

	const std::vector<Nodeset> &get_fvs() const;

	virtual std::string get_node_label(size_t u) const;

	void generage_dag(
		std::function<size_t()> breadth_fn,
		std::function<bool(size_t, size_t)> edge_fn
	);


	const std::vector<Nodeset> &list_loops();

	bool have_loop() const;

protected:
	void dfs(
		size_t u,
		std::function<bool (size_t u, size_t v)> enter_fn,
		std::function<void (size_t u)> leave_fn = nullptr
	) const;

	void print_dot_nodes(std::ostream &out, const Nodeset &filter) const;

	void add_loop(const Nodeset &nodes);

	bool have_loop(const Nodeset &nodes) const;

	std::set<std::pair<size_t, size_t>> reduce_on_filtered(const Nodeset &filter) const;

	std::vector<Nodeset> m_edges;

	std::set<std::pair<size_t, size_t>> m_links;

	std::vector<Nodeset> m_fvs;

	std::vector<Nodeset> m_loops;

	size_t m_nr_edges = 0;
};

