#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyRR : public StrategyOffline
{
public:
	StrategyRR();

	virtual ~StrategyRR();

	virtual void start();

	virtual const char *name() const;

	enum AccFun {
		ScaledSpeed = 0,
		ScaledSpeed2,
		ScaledSpeed3,
		ScaledSpeed4,
		ScaledSpeed5,
		ScaledSpeedMMF,
		ScaledSpeedJian,
		LinearSpeed,
		Utility,
		Angle,
		Variance,
		Median,
		MinWork,
		WorkSum,
		WorkSum2
	};

	void set_acq_fun(AccFun fn);

	void set_interval(double dt);

	bool validate(const std::vector<Taskset> &sched);

	double get_scaled_speed_3(Taskset ts) const;

	double get_linear_speed(Taskset ts) const;

protected:

private:
	double measure(Taskset ts) const;

	double measure_work_sum(Taskset ts) const;

	double measure_work_sum_2(Taskset ts) const;

	double measure_scaled_speed(Taskset ts) const;

	double measure_scaled_speed_2(Taskset ts) const;

	double measure_scaled_speed_3(Taskset ts) const;

	double measure_scaled_speed_4(Taskset ts) const;

	double measure_scaled_speed_5(Taskset ts) const;

	double measure_linear_speed(Taskset ts) const;

	double measure_scaled_speed_mmf(Taskset ts) const;

	double measure_scaled_speed_jian(Taskset ts) const;

	double measure_utility(Taskset ts) const;

	double measure_angle(Taskset ts) const;

	double mesaure_variance(Taskset ts) const;

	double measure_median(Taskset ts) const;

	double measure_min_work(Taskset ts) const;

	double find_min_speed_sum() const;

	AccFun m_fun = MinWork;

	double m_time_interval = 1;

	std::vector<double> m_weight;
};
