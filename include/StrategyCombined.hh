#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyCombined : public StrategyOffline
{
public:
	StrategyCombined();

	virtual ~StrategyCombined();

	virtual void start();

	virtual const char *name() const;

	void set_interval(double dt);

	void set_efficiency(double alpha);

	void set_fairness(double beta);

	void set_name(const std::string &name);

protected:

private:
	double measure(Taskset ts) const;

	inline static double m_speed_eps = 1e-17;

	double m_time_interval = 1;

	double m_fairness = 0;

	double m_efficiency = 0;

	std::vector<double> m_speed_sum;

	std::string m_name;
};
