#pragma once

#include <vector>
#include <fstream>

#include "ScheduledTask.hh"

class System;

class Strategy
{
public:
	Strategy();

	virtual ~Strategy();

	virtual void set_input(const System *sys);

	virtual void start() = 0;

	double get_makespan() const;

	double get_flowtime() const;

	double get_fairness() const;

	double get_fairness_jian() const;

	double get_avg_speed() const;

	void open_log(const std::string &file);

	virtual const char *name() const = 0;

	virtual void reset();

	void stop_after_first(bool stop);

protected:
	double now() const;

	size_t size() const;

	bool any_completed(Taskset ts) const;

	bool all_completed(Taskset ts) const;

	bool all_completed() const;

	bool all_runnable(Taskset ts) const;

	bool all_runnable() const;

	bool all_schedulable(Taskset ts) const;

	size_t get_next_schedulable() const;

	size_t get_next_max_priority() const;

	Taskset get_schedulable() const;

	Taskset get_completed() const;

	void update_successros_for_completed(Taskset ts);

	void set_successors_runnable(size_t task_id);

	bool all_prec_completed(size_t task_id) const;

	std::vector<Taskset> get_schedulable_combinations() const;

	void log(Taskset ts);

	double m_now = 0;

	std::vector<ScheduledTask> m_tasks;

	const System *m_system = nullptr;

	bool m_stop_after_first = false;

private:

	std::ofstream m_log;
};

