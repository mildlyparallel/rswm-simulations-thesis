#pragma once

#include <vector>
#include <tuple>
#include <fstream>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyREC : public StrategyOffline
{
public:
	StrategyREC();

	virtual ~StrategyREC();

	virtual void start();

	virtual const char *name() const;

protected:

private:
	std::tuple<Taskset, double, size_t> measure(
		const std::array<double, Taskset::max_size> &work,
		Taskset skipset,
		size_t depth = 0,
		size_t iter = 0,
		Taskset prev_ts = {},
		size_t prev_id = 0,
		double cmax = 0
	);

	double max_runtime(
		const std::array<double, Taskset::max_size> &work,
		Taskset ts
	) const;

	std::ofstream m_dot;

};
