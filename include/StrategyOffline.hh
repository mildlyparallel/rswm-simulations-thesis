#pragma once

#include <vector>
#include <fstream>
#include <map>

#include "Strategy.hh"

class SystemStationary;

class StrategyOffline : public Strategy
{
public:
	StrategyOffline();

	virtual ~StrategyOffline();

	virtual void reset();

	double get_subset_time(Taskset ts) const;

// protected:
	double run(Taskset ts);

	double run(Taskset ts, double dt);

	double get_rate_scale(Taskset ts) const;

	double get_rate_scale(size_t task_id, Taskset ts) const;

	double get_rate(Taskset ts) const;

	double get_rate(size_t task_id, Taskset ts) const;

	double min_remaining_work() const;

	double min_remaining_work(Taskset ts) const;

	double min_remaining_time(Taskset ts) const;

	size_t get_next_min_work() const;

	double get_work_total(size_t task_id) const;

	double get_work_remain(size_t task_id) const;

	double get_priority(size_t task_id) const;

private:

	std::map<Taskset, double> m_subset_time;
};

