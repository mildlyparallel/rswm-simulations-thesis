#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyOptimal : public StrategyOffline
{
public:
	StrategyOptimal();

	virtual ~StrategyOptimal();

	virtual const char *name() const;

	virtual void start();

protected:

private:
	struct Answer {
		double makespan;
		std::vector<std::pair<Taskset, double>> distr;
	};

	Answer solve(const std::vector<Taskset> &tss) const;

	Answer solve() const;
};
