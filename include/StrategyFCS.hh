#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFCS : public StrategyOffline
{
public:
	StrategyFCS();

	virtual ~StrategyFCS();

	virtual void start();

	virtual const char *name() const;

	enum AccFun {
		CombSpeed = 0,
		SpeedEfficiencySq,
		SpeedEfficiencyExp,
	};

	void set_acq_fun(AccFun fn);

	bool validate(const std::vector<Taskset> &sched);

	void set_efficiency(double alpha);

protected:

private:
	double measure(Taskset ts) const;

	double measure_speed(Taskset ts) const;

	double measure_eff_sq(Taskset ts) const;

	double measure_eff_exp(Taskset ts) const;

	AccFun m_fun = CombSpeed;

	double m_efficiency = 0;
};
