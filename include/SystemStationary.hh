#pragma once

#include <functional>
#include <vector>

#include "System.hh"

class SystemStationary : public System
{
public:
	SystemStationary();

	SystemStationary(size_t n);

	virtual ~SystemStationary();

	void init_base_rate(double r, double dr = 0);

	void init_base_rate(std::function<double (const Task &)> rate_gen_fn);

	void init_rate_scale(std::function<double (Taskset)> scale_gen_fn);

	void init_rate_scale(std::function<double (Taskset, const Task &)> scale_gen_fn);

	void set_rate_scale(size_t task_id, Taskset ts, double scale);

	double get_rate_scale(size_t task_id, Taskset ts) const;

	double get_work_total(size_t task_id) const;

	void init_scale_rnd(std::function<double (size_t task_id)> scale_fn);

	void print() const;
	
	bool validate() const;

	bool validate(Taskset ts) const;

	std::vector<std::pair<Taskset, double>> solve() const;

	void write(const std::string &outdir) const;

	void write_by_task(const std::string &outdir) const;

	void write_by_comb(const std::string &outdir) const;

	double find_min_scale(size_t task_id, Taskset ts) const;

protected:

private:

	void print_val(const std::string &label, std::function<double ()> fn) const;

	void print_row(const std::string &label, std::function<double (const Task &task)> fn) const;

	void print_comb(const std::string &label, std::function<double (const Task &task, Taskset)> fn) const;

};

