#pragma once

#include "StrategyOffline.hh"

class StrategyNaive : public StrategyOffline
{
public:
	StrategyNaive();

	virtual ~StrategyNaive();

	virtual void start();

	virtual const char *name() const;

protected:

private:

};
