#pragma once

#include <vector>
#include <fstream>

#include "Strategy.hh"

class StrategyOnline : public Strategy
{
public:
	static const constexpr double TICK_INTERVAL = 1;

	StrategyOnline();

	virtual ~StrategyOnline();

protected:
	void tick(Taskset ts);

	uint64_t run(Taskset ts);

	uint64_t run(Taskset ts, uint64_t nr_ticks);

	double get_rate_last(size_t task_id) const;

	double get_rate_avg(size_t task_id) const;

	double get_rate_last(Taskset ts) const;

	double get_rate_avg(Taskset ts) const;

private:
};

