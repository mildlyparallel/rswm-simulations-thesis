#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFlow : public StrategyOffline
{
public:
	StrategyFlow();

	virtual ~StrategyFlow();

	virtual const char *name() const;

	virtual void start();

protected:

private:
	std::vector<std::pair<Task::taskset, double>> m_distr;

	struct Answer {
		double obj;
		std::vector<size_t> order;
		std::vector<double> distr;
	};

	void solve(Answer &ans);

	void solve_one(Answer &ans);

	bool is_interrupted(const Answer &ans);

	bool is_in_order(const Answer &ans);
};
