#pragma once

#include <string>

class Config
{
public:
	size_t nr_threads = 0;
	size_t nr_tasks = 4;
	size_t nr_runs = 2;
	int seed = 123;
	std::string output = "results";
	std::string input;

	double max_slope_scale = 1;
	double min_slope_scale = 0;
	double max_resources = 1;
	double min_resources = 0;
	double slope_noise_mean = 0;
	double slope_noise_dev = 0.01;
	double min_work = 100;
	double max_work = 200;
	double min_priority = 1;
	double max_priority = 10;
	double confidence_scale = 0.2;
	double time_interval = 1;
	bool fairness = false;

	double edge_prob = 0.8;
	double node_min_breadth = 1;
	double node_max_breadth = 4;
	std::string dag_dir;

	void configure(int argc, const char **argv);

	void write();

protected:

private:

};
