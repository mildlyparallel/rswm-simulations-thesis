#include <cassert>
#include <iostream>
#include <limits>

#include "StrategyOffline.hh"

#include "SystemStationary.hh"

StrategyOffline::StrategyOffline()
{  }

StrategyOffline::~StrategyOffline()
{  }

double StrategyOffline::run(Taskset ts)
{
	return run(ts, min_remaining_time(ts));
}

double StrategyOffline::run(Taskset ts, double dt)
{
	assert(dt > 0);
	assert(all_runnable(ts));
	assert(!all_completed(ts));

	dt = std::min(dt, min_remaining_time(ts));

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		m_tasks[i].run(ts, m_now, dt);
	}

	log(ts);

	m_now += dt;

	m_subset_time[ts] += dt;

	update_successros_for_completed(ts);

	return dt;
}

double StrategyOffline::get_rate_scale(Taskset ts) const
{
	double r = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		r += get_rate_scale(i, ts);
	}

	return r;
}

double StrategyOffline::get_rate_scale(size_t task_id, Taskset ts) const
{
	return m_tasks[task_id].get_rate_scale(ts);
}

double StrategyOffline::get_rate(Taskset ts) const
{
	double r = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		r += get_rate(i, ts);
	}

	return r;
}

double StrategyOffline::get_rate(size_t task_id, Taskset ts) const
{
	return m_tasks[task_id].get_rate(ts);
}

double StrategyOffline::min_remaining_work() const
{
	double min_w = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_tasks.size(); ++i) {

		double w = m_tasks[i].get_work_total() - m_tasks[i].get_work_done();
		if (w < min_w)
			min_w = w;
	}

	return min_w;
}

double StrategyOffline::min_remaining_work(Taskset ts) const
{
	if (any_completed(ts))
		return 0;

	double min_w = std::numeric_limits<double>::max();
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double w = m_tasks[i].get_work_total() - m_tasks[i].get_work_done();
		if (w < min_w)
			min_w = w;
	}

	return min_w;
}

double StrategyOffline::min_remaining_time(Taskset ts) const
{
	if (any_completed(ts))
		return 0;

	double min_t = std::numeric_limits<double>::max();
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double w = m_tasks[i].get_work_total() - m_tasks[i].get_work_done();
		double r = get_rate(i, ts);
		if (r == 0)
			continue;
		double t = w / r;

		if (t < min_t)
			min_t = t;
	}

	return min_t;
}

size_t StrategyOffline::get_next_min_work() const
{
	size_t task_id = m_tasks.size() + 1;
	double min_w = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!m_tasks[i].runnable())
			continue;
		if (m_tasks[i].completed())
			continue;

		if (m_tasks[i].get_work_done() < min_w) {
			min_w = m_tasks[i].get_work_done();
			task_id = i;
		}
	}

	return task_id;

}

double StrategyOffline::get_subset_time(Taskset ts) const
{
	auto it = m_subset_time.find(ts);
	if (it == m_subset_time.end())
		return 0;
	return it->second;
}

double StrategyOffline::get_work_remain(size_t task_id) const
{
	return m_tasks[task_id].get_work_total() - m_tasks[task_id].get_work_done();
}

double StrategyOffline::get_work_total(size_t task_id) const
{
	return m_tasks[task_id].get_work_total();
}

double StrategyOffline::get_priority(size_t task_id) const
{
	return m_tasks[task_id].get_priority();
}

void StrategyOffline::reset()
{
	Strategy::reset();

	m_subset_time.clear();
}


