#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <cassert>
#include <random>

#include "Config.hh"
#include "SystemStationary.hh"
#include "TaskGraph.hh"
#include "StrategyCombined.hh"

#include <Problem.hh>
#include <armadillo>
#include <lpsolve/lp_lib.h>

void fill_system_rnd(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	sys.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	std::uniform_real_distribution<double> slope_coeff(
		config.min_slope_scale,
		config.max_slope_scale
	);

	double slope_distr_min[Taskset::max_size];

	for (size_t i = 0; i < sys.size(); ++i)
		slope_distr_min[i] = slope_coeff(gen);

	sys.init_scale_rnd([&] (size_t task_id) -> double {
		std::uniform_real_distribution<double> slope_coeff(slope_distr_min[task_id], 1);
		return slope_coeff(gen);
	});
}

TaskGraph create_nodes(size_t n) {
	TaskGraph g;

	for (size_t i = 0; i < n; ++i) {
		Task task;
		task.set_id(i);
		g.add_task(task);
	}

	return g;
}

TaskGraph get_graph_1() {
	TaskGraph g = create_nodes(4);

	g.add_edge(0, 1);
	g.add_edge(2, 3);

	return g;
}

TaskGraph get_graph_2() {
	TaskGraph g = create_nodes(6);

	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(3, 4);
	g.add_edge(4, 5);

	return g;
}

TaskGraph get_graph_3() {
	TaskGraph g = create_nodes(6);

	g.add_edge(0, 1);
	g.add_edge(2, 3);
	g.add_edge(4, 5);

	return g;
}

TaskGraph get_graph_4() {
	size_t k = 6;
	TaskGraph g = create_nodes(k * k);

	for (size_t i = 0; i < k; ++i) {
		for (size_t j = 0; j < k; ++j) {
			size_t u = j * k + i;

			for (size_t i1 = i+1; i1 < k; ++i1) {
				for (size_t j1 = 0; j1 < k; ++j1) {
					if (j1 < j && j - j1 >= i1 - i )
						continue;

					size_t v = j1 * k + i1;
					g.add_edge(u, v);
				}
			}
		}
	}

	return g;
}

TaskGraph get_graph_5() {
	TaskGraph graph;

	size_t k = 3;
	size_t nr_tasks = 2 * k + 1;
	TaskGraph g = create_nodes(nr_tasks);

	for (size_t i = 0; i < k; ++i) {
		g.add_edge(2 * i + 1, 2 * i + 2);
	}

	return g;
}

TaskGraph get_graph_random(
	const Config &config,
	std::default_random_engine &rndgen
) {

	std::uniform_int_distribution<int> distr_breadth(config.node_min_breadth, config.node_max_breadth);
	std::uniform_real_distribution<double> distr_prob(0., 1.);

	TaskGraph g = create_nodes(config.nr_tasks);

	g.generage_dag(
		[&]() {
			return distr_breadth(rndgen);
		},
		[&](size_t, size_t) {
			return distr_prob(rndgen) < config.edge_prob;
		}
	);

	return g;
}

std::tuple<std::vector<Taskset>, double> solve(
	const SystemStationary &sys,
	const std::set<Taskset> &tss
) {
	size_t nr_tasks = sys.size();
	size_t nr_combs = tss.size();

	using namespace lpcpp;

	Problem lp(nr_combs);

	lp.set_verbose(Problem::Important);

	for (size_t i = 0; i < nr_tasks; ++i) {
		size_t j = 0;
		for (auto ts : tss) {
			lp[j] = sys.get_rate_scale(i, ts);
			j++;
		}

		lp.add_constraint(Problem::Equal, sys.get_work_total(i));
	}

	lp.set_bound_lower_all(0);

	lp.fill_row(1);
	lp.set_objective_minim();

	// lp.print_lp();
	double opt = lp.optimize();

	std::vector<Taskset> basis;

	size_t j = 0;
	for (auto ts : tss) {
		j++;
		if (lp[j-1] <= 1e-7)
			continue;

		basis.push_back(ts);
	}

	std::cerr << "basis.size() = " << basis.size() << std::endl;
	std::cerr << "nr_tasks = " << nr_tasks << std::endl;

	assert(basis.size() == nr_tasks);

	return {basis, opt};
}

void reduce_non_basis(
	SystemStationary &sys,
	const std::set<Taskset> &lockfree,
	const std::vector<Taskset> &basis
) {
	size_t n = sys.size();

	arma::mat A(n, n);
	for (size_t j = 0; j < n; j++) {
		for (size_t i = 0; i < n; ++i)
			A(i, j) = sys.get_rate_scale(i, basis[j]);
	}

	arma::mat Ainv = arma::inv(A);

	arma::vec a_new(n);

	for (auto &comb : sys.feasible_combinations()) {
		if (lockfree.contains(comb))
			continue;

		for (size_t i = 0; i < n; ++i)
			a_new(i) = sys.get_rate_scale(i, comb);

		bool found = false;

		for (size_t i = 0; i < 10; ++i) {
			double delta = 1 - arma::sum(Ainv * a_new);
			// std::cerr << i << " " << arma::sum(a_new) << " " << delta << std::endl;
			if (delta > 0) {
				found = true;
				break;
			}

			a_new *= 0.90;
		}

		assert(found);

		for (size_t i = 0; i < n; ++i)
			sys.set_rate_scale(i, comb, a_new(i));

		for (auto &comb2 : sys.feasible_combinations()) {
			if (lockfree.contains(comb2))
				continue;
			if (comb == comb2)
				continue;
			if (!comb.subset_of(comb2))
				continue;
			for (size_t i = 0; i < n; ++i) {
				if (!comb[i] || !comb2[i])
					continue;

				double a1 = sys.get_rate_scale(i, comb);
				double a2 = sys.get_rate_scale(i, comb2);

				if (a2 < a1)
					continue;

				sys.set_rate_scale(i, comb2, a1);
			}
		}
	}
}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	std::default_random_engine rndgen;
	rndgen.seed(config.seed);

	TaskGraph tasks;
	if (!config.input.empty())
		tasks.read_dot(config.input);
	else
		tasks = get_graph_4();

	std::cerr << "tasks.nr_nodes() = " << tasks.nr_nodes() << std::endl;
	std::cerr << "tasks.nr_edges() = " << tasks.nr_edges() << std::endl;

	tasks.print_dot_directed("graph.dot");

	tasks.reduce();
	tasks.complement();
	tasks.list_feasible();
	tasks.find_any_lockfree(rndgen);
	
	// std::cerr << "Found lock free" << std::endl;
	// for (auto ts : tasks.get_lockfree()) {
	// 	std::cerr << ts << std::endl;
	// }

	size_t id = 0;

	SystemStationary sys(tasks.nr_nodes());
	sys.set_id(id);
	sys.set_precedence(tasks);
	sys.set_seed(config.seed + id);

	fill_system_rnd(config, sys, rndgen);

	const std::set<Taskset> &lockfree = tasks.get_lockfree();

	auto [basis, opt] = solve(sys, lockfree);

	reduce_non_basis(sys, lockfree, basis);

	StrategyCombined stg_fcs;
	stg_fcs.set_input(&sys);
	stg_fcs.set_interval(1e9);
	stg_fcs.set_fairness(0);
	stg_fcs.set_efficiency(0);
	stg_fcs.start();

	StrategyCombined stg_pf;
	stg_pf.set_input(&sys);
	stg_pf.set_fairness(1);
	stg_pf.set_efficiency(0);
	stg_pf.start();

	StrategyCombined stg_pe;
	stg_pe.set_input(&sys);
	stg_pe.set_interval(1e9);
	stg_pe.set_fairness(0);
	stg_pe.set_efficiency(1);
	stg_pe.start();

	std::ofstream out(config.output + "/results.csv");
	out << "nodes,";
	out << "edges,";
	out << "combinations,";
	out << "opt,";
	out << "fcs,";
	out << "pf,";
	out << "pe\n";

	out << tasks.nr_nodes() << ",";
	out << tasks.nr_edges() << ",";
	out << tasks.get_feasible().size() << ",";
	out << opt << ",";
	out << stg_fcs.get_makespan() << ",";
	out << stg_pf.get_makespan() << ",";
	out << stg_pe.get_makespan() << "\n";

	// sys.print();
	// std::cerr << "Validating ";
	// bool tasks_ok = sys.validate();
	// if (tasks_ok) {
	// 	std::cerr << " Ok" << std::endl;
	// } else {
	// 	std::cerr << " Not valid" << std::endl;
	// }

	return 0;
}

