#include <cassert>
#include <iostream>

#include "StrategyOnline.hh"

#include "System.hh"

StrategyOnline::StrategyOnline()
{  }

StrategyOnline::~StrategyOnline()
{  }

void StrategyOnline::tick(Taskset ts)
{
	assert(!all_completed(ts));
	assert(all_runnable(ts));

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		m_tasks[i].run(ts, now(), TICK_INTERVAL);
	}

	log(ts);

	m_now += TICK_INTERVAL;
}

uint64_t StrategyOnline::run(Taskset ts, uint64_t nr_ticks)
{
	uint64_t n = 0;

	for (; n < nr_ticks; ++n) {
		if (any_completed(ts))
			break;

		tick(ts);
	}

	update_successros_for_completed(ts);

	return n;
}

uint64_t StrategyOnline::run(Taskset ts)
{
	uint64_t n = 0;

	for (; !any_completed(ts); ++n) {
		tick(ts);
	}

	update_successros_for_completed(ts);

	return n;
}

double StrategyOnline::get_rate_last(size_t task_id) const
{
	return m_tasks[task_id].get_rate_last();
}

double StrategyOnline::get_rate_avg(size_t task_id) const
{
	return m_tasks[task_id].get_rate_avg();
}

double StrategyOnline::get_rate_last(Taskset ts) const
{
	double r = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		r += get_rate_last(i);
	}

	return r;
}

double StrategyOnline::get_rate_avg(Taskset ts) const
{
	double r = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		r += get_rate_avg(i);
	}

	return r;
}

