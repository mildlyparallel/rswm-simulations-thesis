#include <iostream>
#include <thread>
#include <iomanip>
#include <fstream>
#include <filesystem>

#include <Dashh.hh>
#include <Utils.hh>

#include "Config.hh"
#include "Task.hh"

void Config::configure(int argc, const char **argv)
{
	using namespace dashh;

	Command cmd;
	cmd << Option(nr_threads, "-w, --workers", "N", "Number of threads");
	cmd << Option(nr_runs, "-r, --runs", "N", "Number of input systems generated for each paramter values set");
	cmd << Option(output, "-o, --output", "DIR", "Output directory for input systems and simul results");
	cmd << Option(seed, "--seed", "N", "Random seed value");
	cmd << Option(nr_tasks, "--tasks", "tasks", "Number of task");
	cmd << Option(min_work, "--work-min", "W", "Min value for work units distribution");
	cmd << Option(max_work, "--work-max", "W", "Max value for work units distribution");
	cmd << Option(min_priority, "--priority-min", "P", "Min value for priorities distribution");
	cmd << Option(max_priority, "--priority-max", "P", "Max value for priorities distribution");
	cmd << Option(min_slope_scale, "--slope-scale-min", "K", "Min value for speed distribution");
	cmd << Option(max_slope_scale, "--slope-scale-max", "K", "Max value for speed distribution");
	cmd << Option(min_resources, "--resources-min", "K", "Min value for resources distribution");
	cmd << Option(max_resources, "--resources-max", "K", "Max value for resources distribution");
	cmd << Option(time_interval, "--time-interval", "dT", "RR time interval");
	cmd << Option(input, "--input", "path", "Problem instance");
	cmd << Flag(fairness, "--fairness", "Report fairness instead of makespan");

	cmd << Option(edge_prob, "--edge-prob", "P", "Edge probability in DAG");
	cmd << Option(node_min_breadth, "--breadth-min", "B", "Min number of nodes in row");
	cmd << Option(node_max_breadth, "--breadth-max", "B", "Max number of nodes in row");
	cmd << Option(dag_dir, "--dag", "PATH", "directory with dag files");
	cmd << Flag(PageHelp(), "-h");
	Dashh t(&cmd);
	t.parse(argc, argv);

	if (nr_tasks > Taskset::max_size) {
		std::cerr << nr_tasks << " tasks is too much" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	if (nr_threads == 0)
		nr_threads = std::thread::hardware_concurrency();

	std::filesystem::create_directory(output);

	std::cout << "nr_threads       = " << nr_threads << std::endl;
	// std::cout << "slope_noise      = " << "Norm(" << slope_noise_mean << ", " << slope_noise_dev << ")" << std::endl;
	std::cout << "slope_scale      = " << "Unif(" << min_slope_scale << ", " << max_slope_scale << ")" << std::endl;
	std::cout << "resources        = " << "Unif(" << min_resources << ", " << max_resources << ")" << std::endl;
	std::cout << "nr_runs          = " << nr_runs << std::endl;
	std::cout << "nr_tasks         = " << nr_tasks << std::endl;
	std::cout << "work             = " << "Unif(" << min_work << ", " << max_work << ")" << std::endl;
	std::cout << "priority         = " << "Unif(" << min_priority << ", " << max_priority << ")" << std::endl;
	std::cout << "edge_prob        = " << edge_prob << std::endl;
	std::cout << "breadth          = " << "Unif(" << node_min_breadth << ", " << node_max_breadth << ")" << std::endl;
	std::cout << "dag_dir          = " << dag_dir << std::endl;
	std::cout << "seed             = " << seed << std::endl;
	std::cout << "time_interval    = " << time_interval << std::endl;
	std::cout << "fairness         = " << fairness << std::endl;

	std::cout << "output           = " << output << std::endl;

	write();
}

void Config::write()
{
	std::ofstream out(output + "/input.dat");

	out << "nr_tasks " << nr_tasks << "\n";
	out << "nr_runs " << nr_runs << "\n";
	out << "seed " << seed << "\n";
	out << "max_slope_scale " << max_slope_scale << "\n";
	out << "min_slope_scale " << min_slope_scale << "\n";
	out << "slope_noise_mean " << slope_noise_mean << "\n";
	out << "slope_noise_dev " << slope_noise_dev << "\n";
	out << "time_interval " << time_interval << "\n";
	out << "min_work " << min_work << "\n";
	out << "max_work " << max_work << "\n";
	out << "min_priority " << min_priority << "\n";
	out << "max_priority " << max_priority << "\n";
	out << "edge_prob " << edge_prob << "\n";
	out << "node_min_breadth " << node_min_breadth << "\n";
	out << "node_max_breadth " << node_max_breadth << "\n";
	out << "dag_dir " << dag_dir << "\n";
	out << "fairness " << fairness << "\n";
}

