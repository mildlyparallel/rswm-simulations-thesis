#include <cassert>
#include <iostream>
#include <limits>

#include "Strategy.hh"
#include "System.hh"

Strategy::Strategy()
{  }

Strategy::~Strategy()
{  }

void Strategy::set_input(const System *sys)
{
	assert(sys);

	reset();

	m_system = sys;

	for (auto &t : sys->tasks())
		m_tasks.push_back(t);

	for (size_t i = 0; i < m_tasks.size(); ++i)
		m_tasks[i].set_runnable(true);

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		Taskset ts = m_tasks[i].get_successors();

		for (size_t j = 0; j < m_tasks.size(); ++j) {
			if (!ts[j])
				continue;
			
			m_tasks[j].set_runnable(false);
		}
	}
}

double Strategy::now() const
{
	return m_now;
}

size_t Strategy::size() const
{
	return m_tasks.size();
}

bool Strategy::any_completed(Taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		if (m_tasks[i].completed())
			return true;
	}

	return false;
}

bool Strategy::all_runnable(Taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		if (!m_tasks[i].runnable())
			return false;
	}

	return true;
}

bool Strategy::all_runnable() const
{
	for (auto &task : m_tasks) {
		if (!task.runnable())
			return false;
	}

	return true;
}

bool Strategy::all_schedulable(Taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;
		if (!m_tasks[i].runnable())
			return false;
		if (m_tasks[i].completed())
			return false;
	}

	return true;
}

bool Strategy::all_completed(Taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		if (!m_tasks[i].completed())
			return false;
	}

	return true;
}

bool Strategy::all_completed() const
{
	for (auto &task : m_tasks) {
		if (!task.completed())
			return false;
	}

	return true;
}

Taskset Strategy::get_schedulable() const
{
	const auto tss = m_system->feasible_combinations();
	for (auto it = tss.rbegin(); it != tss.rend(); it++) {
		if (all_schedulable(*it))
			return *it;
	}

	return {};
}

size_t Strategy::get_next_schedulable() const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed())
			continue;
		if (!m_tasks[i].runnable())
			continue;
		return i;
	}

	return m_tasks.size() + 1;
}

size_t Strategy::get_next_max_priority() const
{
	double max_prio = 0;
	size_t task_id = m_tasks.size() + 1;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed())
			continue;
		if (!m_tasks[i].runnable())
			continue;

		if (m_tasks[i].get_priority() > max_prio) {
			max_prio = m_tasks[i].get_priority();
			task_id = i;
		}
	}

	return task_id;
}

Taskset Strategy::get_completed() const
{
	Taskset ret;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed())
			ret.set(i);
	}

	return ret;
}

double Strategy::get_makespan() const
{
	double makespan = 0;

	for (auto &t : m_tasks) {
		if (t.get_complition_time() > makespan)
			makespan = t.get_complition_time();
	}

	return makespan;
}

double Strategy::get_flowtime() const
{
	double flowtime = 0;

	for (auto &t : m_tasks)
		flowtime += t.get_complition_time() * t.get_priority();

	return flowtime;
}

double Strategy::get_fairness() const
{
	double w_max = 0;
	double w_min = std::numeric_limits<double>::max();

	for (auto &t : m_tasks) {
		if (!t.runnable())
			continue;
		double w = t.get_work_done();
		if (w > w_max)
			w_max = w;
		if (w < w_min)
			w_min = w;
	}

	if (w_max == 0)
		return 1;

	return w_min / w_max;
}

double Strategy::get_fairness_jian() const
{
	double w_sum = 0;
	double w2_sum = 0;
	double n = 0;

	for (auto &t : m_tasks) {
		if (!t.runnable())
			continue;

		n++;

		double w = t.get_work_done();
		w_sum += w;
		w2_sum += w * w;
	}

	if (w_sum == 0 || n == 0)
		return 1;

	return w_sum * w_sum / (w2_sum * n);
}

double Strategy::get_avg_speed() const
{
	double w_sum = 0;

	for (auto &t : m_tasks) {
		// if (!t.runnable())
			// continue;

		double w = t.get_work_done();
		w_sum += w;
	}

	if (now() == 0)
		return 0;

	return w_sum / now();
}

void Strategy::open_log(const std::string &file)
{
	assert(!m_log.is_open());

	m_log.open(file);

	m_log << "time";
	for (size_t i = 0; i < m_tasks.size(); ++i)
		m_log << ",task" << (i+1);

	m_log << std::endl;
}

void Strategy::log(Taskset ts)
{
	if (!m_log.good())
		return;

	m_log << now();
	
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		m_log << ",";
		if (ts[i])
			m_log << m_tasks[i].get_rate_last();
		else
			m_log << 0;
	}

	m_log << std::endl;
}

void Strategy::reset()
{
	m_tasks.clear();
	m_now = 0;
	m_system = nullptr;
}

void Strategy::update_successros_for_completed(Taskset ts)
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;
		if (!m_tasks[i].completed())
			continue;

		set_successors_runnable(i);
	}
}

bool Strategy::all_prec_completed(size_t task_id) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		Taskset ts = m_tasks[i].get_successors();

		if (!ts[task_id])
			continue;

		if (!m_tasks[i].completed())
			return false;
	}

	return true;
}

void Strategy::set_successors_runnable(size_t task_id)
{
	assert(m_tasks[task_id].completed());

	Taskset ts_v = m_tasks[task_id].get_successors();

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts_v[i])
			continue;

		if (all_prec_completed(i))
			m_tasks[i].set_runnable(true);
	}
}

std::vector<Taskset> Strategy::get_schedulable_combinations() const
{
	std::vector<Taskset> tss; 

	for (auto &ts : m_system->feasible_combinations()) {
		if (all_schedulable(ts))
			tss.push_back(ts);
	}

	return tss;
}

void Strategy::stop_after_first(bool stop)
{
	m_stop_after_first = stop;
}
