#include <cassert>
#include <iostream>

#include "ScheduledTask.hh"

ScheduledTask::ScheduledTask()
{  }

ScheduledTask::~ScheduledTask()
{  }

ScheduledTask::ScheduledTask(const Task &t)
: Task(t)
{ }

double ScheduledTask::get_rate(Taskset tasks) const
{
	return Task::get_rate(tasks, m_work_done);
}

double ScheduledTask::get_rate_scale(Taskset tasks) const
{
	return Task::get_rate_scale(tasks, m_work_done);
}

void ScheduledTask::reset()
{
	m_complition_time = 0;
	m_completed = false;
	m_work_done = 0;
	m_rates_pos = 0;
}

void ScheduledTask::advance(double time_at_end, double delta_work)
{
	assert(!completed());
	assert(delta_work >= 0);

	m_work_done += delta_work;

	if (m_work_done > m_work_total - WORK_EPS) {
		m_completed = true;
		m_complition_time = time_at_end;
	}
}

double ScheduledTask::run(Taskset tasks, double time_now, double delta_time)
{
	assert(runnable());
	assert(delta_time >= 0);

	double rate = get_rate(tasks);
	push_rate(rate);

	double dw = delta_time * rate;
	advance(time_now + delta_time, dw);

	return rate;
}

bool ScheduledTask::completed() const
{
	return m_completed;
}

void ScheduledTask::push_rate(double rate)
{
	m_rates[m_rates_pos & m_rate_history_size_mask] = rate;
	m_rates_pos++;
}

double ScheduledTask::get_rate_last() const
{
	assert(m_rates_pos > 0);

	size_t n = (m_rates_pos - 1) & m_rate_history_size_mask;
	return m_rates[n];
}

double ScheduledTask::get_rate_avg() const
{
	if (m_rates_pos == 0)
		return 0;

	double s = 0;

	if (m_rates_pos < m_rates.size()) {
		for (size_t i = 0; i < m_rates_pos; ++i)
			s += m_rates[i];
		return s / m_rates_pos;
	} 

	for (size_t i = 0; i < m_rates.size(); ++i) {
		size_t n = (m_rates_pos - i - 1) & m_rate_history_size_mask;
		s += m_rates[n];
	}

	return s / m_rates.size();
}

double ScheduledTask::get_complition_time() const
{
	return m_complition_time;
}

double ScheduledTask::get_work_done() const
{
	return m_work_done;
}

bool ScheduledTask::runnable() const
{
	return m_runnable;
}

void ScheduledTask::set_runnable(bool r)
{
	m_runnable = r;
}
