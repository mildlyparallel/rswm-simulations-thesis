#include <cassert>
#include <iostream>

#include "StrategyFCSP.hh"

StrategyFCSP::StrategyFCSP()
{  }

StrategyFCSP::~StrategyFCSP()
{  }

const char *StrategyFCSP::name() const
{
	return "prio-fcs";
}

void StrategyFCSP::start()
{
	while (!all_completed()) {
		Taskset max_comb;
		double max_scale = 0;

		size_t max_prio_tid = get_next_max_priority();
		assert(max_prio_tid < m_tasks.size());

		Taskset ts;
		while (ts.next_combination(0, m_tasks.size())) {
			if (!ts[max_prio_tid])
				continue;

			if (any_completed(ts))
				continue;

			double scale = get_rate_scale(ts);
			if (scale > max_scale) {
				max_scale = scale;
				max_comb = ts;
			} else if (scale == max_scale && ts.count() > max_comb.count()) {
				max_comb = ts;
			}
		}

		assert(max_comb != 0);

		double dt = min_remaining_time(max_comb);

		// std::cerr << max_comb << " " << dt << std::endl;
		run(max_comb, dt);
	}

	// std::cerr << __func__ << ":" << __LINE__ << " " <<  std::endl;

	assert(all_completed());
}
