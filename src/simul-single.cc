#include <iostream>
#include <random>
#include <thread>
#include <atomic>
#include <queue>
#include <mutex>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <sstream>

#include "Config.hh"
#include "TaskGraph.hh"
#include "Runner.hh"
#include "SystemStationary.hh"

#include "StrategySequential.hh"
#include "StrategyNaive.hh"
#include "StrategyOptimal.hh"
#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyFair.hh"
#include "StrategyCombined.hh"
#include "StrategySearch.hh"

SystemStationary read_system(const Config &config)
{
	std::ifstream in(config.input);

	size_t nr_tasks = 0;
	in >> nr_tasks;

	TaskGraph tasks;
	if (config.dag_dir.empty())
		tasks.add_tasks_no_prec(nr_tasks);

	SystemStationary sys(nr_tasks);
	sys.set_precedence(tasks);
	sys.set_id(1);

	sys.init_base_rate(1);

	for (size_t i = 0; i < nr_tasks; ++i) {
		double w = 1;
		in >> w;
		sys[i].set_work_total(w);
	}

	std::vector<double> vals(nr_tasks);
	std::vector<double> ideal(nr_tasks);

	for (size_t row = 0; row < (1 << nr_tasks) - 1; ++row) {
		std::fill(vals.begin(), vals.end(), 0);

		Taskset ts;

		double a0 = 0;
		size_t i0 = 0;
		for (size_t i = 0; i < nr_tasks; ++i) {
			in >> vals[i];
			if (vals[i] > 0) {
				ts.set(i);
				a0 = vals[i];
				i0 = i;
			}
		}

		std::cerr << row << " : ";
		for (size_t i = 0; i < nr_tasks; ++i) {
			std::cerr << vals[i] << " ";
		}
		std::cerr << std::endl;

		if (ts.count() == 1)
			ideal[i0] = a0;

		for (size_t i = 0; i < nr_tasks; ++i) {
			sys[i].set_rate_scale(ts, vals[i]);
		}
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			double a = sys[i].get_rate_scale(ts, 0);
			sys[i].set_rate_scale(ts, a / ideal[i]);
		}
	}

	Taskset ts_sup;
	while (ts_sup.next_combination(0, nr_tasks)) {
		Taskset ts_sub;
		while (ts_sub.next_combination(0, nr_tasks)) {
			if (!ts_sub.subset_of(ts_sup))
				continue;

			for (size_t i = 0; i < nr_tasks; ++i) {
				if (!ts_sub[i])
					continue;

				double v_sup = sys[i].get_rate_scale(ts_sup, 0);
				double v_sub = sys[i].get_rate_scale(ts_sub, 0);

				if (v_sup <= v_sub)
					continue;

				sys[i].set_rate_scale(ts_sup, v_sub);
			}
		}
	}

	// sys.print();

	return sys;
}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	SystemStationary sys = read_system(config);

	// bool tasks_ok = sys.validate();
	// if (tasks_ok) {
	// 	std::cerr << " valid" << std::endl;
	// } else {
	// 	std::cerr << " not valid" << std::endl;
	// }

	StrategyOptimal stg_opt;
	stg_opt.set_input(&sys);
	stg_opt.start();
	double opt = stg_opt.get_makespan();

	// StrategySequential stg_seq;
	// stg_seq.set_input(&sys);
	// stg_seq.start();
	// std::cerr << "seq " << stg_seq.get_makespan()/opt << std::endl;
    //
	// StrategyNaive stg_par;
	// stg_par.set_input(&sys);
	// stg_par.start();
	// std::cerr << "par " << stg_par.get_makespan()/opt << std::endl;

	StrategyCombined stg_fcs;
	stg_fcs.set_input(&sys);
	stg_fcs.set_interval(1e9);
	stg_fcs.set_fairness(0);
	stg_fcs.set_efficiency(0);
	stg_fcs.start();

	StrategyCombined stg_pf;
	stg_pf.set_input(&sys);
	stg_pf.set_fairness(1);
	stg_pf.set_efficiency(0);
	stg_pf.start();

	StrategyCombined stg_pe;
	stg_pe.set_input(&sys);
	stg_pe.set_interval(1e9);
	stg_pe.set_fairness(0);
	stg_pe.set_efficiency(1);
	stg_pe.start();

	double qq[4] = {1.5, 1, 0.5, 0.1};

	std::ofstream out("search-all.csv");

	out << "q,opt,fcs,pf,pe,ucb_fcs,ucb_pf,ucb_pe\n";

	for (size_t i = 0; i < 4; ++i) {
		double q = qq[i];
		std::cerr << "q = " << q << std::endl;

		out << q << ",";
		out << opt << ",";
		out << stg_fcs.get_makespan() << ",";
		out << stg_pf.get_makespan() << ",";
		out << stg_pe.get_makespan() << ",";

		std::stringstream ss;

		StrategySearch stg_s_fcs;
		stg_s_fcs.set_input(&sys);
		ss.str("");
		ss << "search-fcs-" << i << ".csv";
		stg_s_fcs.open_log_file(ss.str());
		stg_s_fcs.set_quartile(q);
		stg_s_fcs.set_fairness(0);
		stg_s_fcs.set_efficiency(0);
		stg_s_fcs.start();

		out << stg_s_fcs.get_makespan() << ",";

		StrategySearch stg_s_pf;
		stg_s_pf.set_input(&sys);
		ss.str("");
		ss << "search-pf-" << i << ".csv";
		stg_s_pf.open_log_file(ss.str());
		stg_s_pf.set_quartile(q);
		stg_s_pf.set_fairness(1);
		stg_s_pf.set_efficiency(0);
		stg_s_pf.start();

		out << stg_s_pf.get_makespan() << ",";

		StrategySearch stg_s_pe;
		stg_s_pe.set_input(&sys);
		ss.str("");
		ss << "search-pe-" << i << ".csv";
		stg_s_pe.open_log_file(ss.str());
		stg_s_pe.set_quartile(q);
		stg_s_pe.set_fairness(0);
		stg_s_pe.set_efficiency(1);
		stg_s_pe.start();

		out << stg_s_pe.get_makespan() << "\n";
	}

	return 0;
}

