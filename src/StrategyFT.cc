#include <cassert>
#include <iostream>

#include "StrategyFT.hh"

StrategyFT::StrategyFT()
{  }

StrategyFT::~StrategyFT()
{  }

const char *StrategyFT::name() const
{
	return "ft";
}

void StrategyFT::start()
{
	size_t nr_combinations = (1 << m_tasks.size()) - 1;

	while (!all_completed()) {
		taskset max_comb;
		double max_scale = 0;

		for (size_t j = 1; j <= nr_combinations; ++j) {
			taskset ts(j);
			if (any_completed(ts))
				continue;

			double v = 0;
			for (size_t i = 0; i < m_tasks.size(); ++i) {
				if (!ts[i])
					continue;
				v += get_priority(i) * get_rate_scale(i, ts);
			}

			if (v > max_scale) {
				max_scale = v;
				max_comb = ts;
			} else if (v == max_scale && ts.count() > max_comb.count()) {
				max_comb = ts;
			}
		}

		assert(max_comb != 0);

		double dt = min_remaining_time(max_comb);

		// std::cerr << max_comb << " " << dt << std::endl;
		run(max_comb, dt);
	}

	// std::cerr << __func__ << ":" << __LINE__ << " " <<  std::endl;

	assert(all_completed());
}
