#include <iostream>
#include <random>
#include <thread>
#include <atomic>
#include <queue>
#include <mutex>
#include <sstream>
#include <chrono>
#include <iomanip>

#include "Config.hh"
#include "TaskGraph.hh"
#include "Runner.hh"
#include "SystemStationary.hh"

#include "StrategySequential.hh"
#include "StrategyNaive.hh"
#include "StrategyOptimal.hh"
#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyFair.hh"

void fill_system_rnd(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	sys.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	std::uniform_real_distribution<double> slope_coeff(
		config.min_slope_scale,
		config.max_slope_scale
	);

	double slope_distr_min[Taskset::max_size];

	for (size_t i = 0; i < sys.size(); ++i)
		slope_distr_min[i] = slope_coeff(gen);

	for (size_t i = 0; i < sys.size(); ++i)
		sys[i].set_rate_scale(Taskset({i}), 1);

	for (const auto &ts : sys.feasible_combinations()) {
		if (ts.count() == 1)
			continue;

		for (size_t i = 0; i < sys.size(); ++i) {
			if (!ts[i])
				continue;

			// std::normal_distribution<double> slope_coeff((1. + slope_distr_min[i])/2, 0.05);
			std::uniform_real_distribution<double> slope_coeff(slope_distr_min[i], 1);
			double slope = std::clamp(slope_coeff(gen), 0., 1.);

			double min_speed = sys.find_min_scale(i, ts);
			double s = slope * min_speed;
			sys[i].set_rate_scale(ts, s);
		}
	}
}

void fill_system_rnd_res(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	sys.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	// std::uniform_real_distribution<double> res_distr(config.min_resources, config.max_resources);
	std::uniform_real_distribution<double> res_distr(0.2, 0.1);
	std::uniform_real_distribution<double> res_distr2(0.8, 1);

	std::vector<double> res1(sys.size());
	std::vector<double> res2(sys.size());
	for (size_t i = 0; i < sys.size(); ++i) {
		res1[i] = res_distr(gen);
		res2[i] = res_distr2(gen);
	}

	std::sort(res1.begin(), res1.end());
	std::sort(res2.begin(), res2.end());

	// std::uniform_real_distribution<double> slope_distr(config.min_slope_scale, config.max_slope_scale);
	std::uniform_real_distribution<double> slope_distr(0.8, 0.9);
	std::uniform_real_distribution<double> slope_distr2(0.2, 0.9);

	std::vector<double> slope1(sys.size());
	std::vector<double> slope2(sys.size());
	for (size_t i = 0; i < sys.size(); ++i) {
		slope1[i] = slope_distr(gen);
		slope2[i] = slope_distr2(gen);
	}

	// std::cerr << "r1: ";
	// for (size_t i = 0; i < sys.size(); ++i)
	// 	std::cerr << res1[i] << " ";
	// std::cerr << std::endl;
    //
	// std::cerr << "r2: ";
	// for (size_t i = 0; i < sys.size(); ++i)
	// 	std::cerr << res2[i] << " ";
	// std::cerr << std::endl;
    //
	// std::cerr << "s1: ";
	// for (size_t i = 0; i < sys.size(); ++i)
	// 	std::cerr << slope1[i] << " ";
	// std::cerr << std::endl;
    //
	// std::cerr << "s2: ";
	// for (size_t i = 0; i < sys.size(); ++i)
	// 	std::cerr << slope2[i] << " ";
	// std::cerr << std::endl;

	for (auto &ts : sys.feasible_combinations()) {
		double left1 = 1;
		double left2 = 1;
		size_t left_tasks = ts.count();

		for (size_t i = 0; i < sys.size(); ++i) {
			if (!ts[i])
				continue;

			double r1 = res1[i];
			double r2 = res2[i];
			double s1 = std::min(r1, left1 / left_tasks);
			double s2 = std::min(r2, left2 / left_tasks);

			double a = 1. - slope1[i] * (r1 - s1) - slope2[i] * (r2 - s2);
			// if (ts.count() == 3 && i == 0)
			// 	std::cerr << "r = " << s1 << " " << s2 << std::endl;

			sys[i].set_rate_scale(ts, std::clamp(a, 1e-6, 1.));

			left1 -= s1;
			left2 -= s2;
			left_tasks--;
		}
	}

	// sys.print();

	// std::cerr << "Validating ";
	// bool tasks_ok = sys.validate();
	// if (tasks_ok) {
	// 	std::cerr << " Ok" << std::endl;
	// } else {
	// 	std::cerr << " Not valid" << std::endl;
	// 	std::abort();
	// }
    //
	// exit(0);
}

SystemStationary create_instance(
	size_t id,
	const TaskGraph &tasks,
	const Config &config
) {
	SystemStationary sys(tasks.nr_nodes());

	sys.set_id(id);
	sys.set_precedence(tasks);
	sys.set_seed(config.seed + id);

	std::default_random_engine rndgen;
	rndgen.seed(sys.get_seed());

	fill_system_rnd(config, sys, rndgen);

	// sys.write(config.output);
	// sys.write_by_task(config.output);
	// sys.write_by_comb(config.output);

	return sys;
}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	TaskGraph tasks;
	if (config.dag_dir.empty())
		tasks.add_tasks_no_prec(config.nr_tasks);

	std::cout << "Total simulations:" << config.nr_runs << std::endl;

	for (size_t i = 0; i < config.nr_runs; ++i) {
		SystemStationary sys = create_instance(1 + i, tasks, config);
		// sys.write_by_comb(config.output);
		sys.write_by_task(config.output);
	}

	std::cout << "\nDone.\n";

	return 0;
}

