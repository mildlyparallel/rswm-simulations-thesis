#include <cassert>
#include <limits>
#include <iostream>

#include <Problem.hh>

#include "StrategyOptimal.hh"

StrategyOptimal::StrategyOptimal()
{  }

StrategyOptimal::~StrategyOptimal()
{  }

const char *StrategyOptimal::name() const
{
	return "optimal";
}

void StrategyOptimal::start()
{
	Answer ans = solve();

	std::set<Taskset> completed;

	bool found = true;

#ifndef NDEBUG
	std::cerr << std::endl;
	std::cerr << "Optimal" << std::endl;
#endif

	do {
		found = false;

		for (auto [ts, dt] : ans.distr) {
			if (completed.contains(ts))
				continue;

			if (!all_schedulable(ts))
				continue;

			found = true;

#ifndef NDEBUG
			std::cerr << ts << " " << dt << std::endl;
#endif

			run(ts, dt);
			completed.insert(ts);
			break;
		}
	} while (found);

	assert(all_completed());
}

StrategyOptimal::Answer StrategyOptimal::solve() const
{
	assert(m_system);

	// Answer best;
	// best.makespan = std::numeric_limits<double>::max();

	Answer best = solve(m_system->feasible_combinations());

	// for (auto tss : m_system->nonblocking_sets()) {
	// 	Answer ans = solve(tss);
    //
	// 	if (ans.makespan < best.makespan)
	// 		best = ans;
	// }

	return best;
}

StrategyOptimal::Answer StrategyOptimal::solve(const std::vector<Taskset> &tss) const
{
	size_t nr_tasks = size();
	size_t nr_combs = tss.size();

	using namespace lpcpp;

	Problem lp(nr_combs);

	lp.set_verbose(Problem::Important);

	for (size_t i = 0; i < nr_tasks; ++i) {

		for (size_t j = 0; j < nr_combs; ++j)
			lp[j] = get_rate_scale(i, tss[j]);

		lp.add_constraint(Problem::Equal, get_work_total(i));
	}

	lp.set_bound_lower_all(0);

	lp.fill_row(1);
	lp.set_objective_minim();

	// lp.print_lp();

	Answer ans;
	ans.makespan = lp.optimize();

	// std::cerr << "ans.makespan = " << ans.makespan << std::endl;

	for (size_t j = 0; j < nr_combs; j++) {
		if (lp[j] <= 1e-7)
			continue;

		ans.distr.push_back({tss[j], lp[j]});

		// std::cerr << tss[j] << "  " << lp[j] << "\n";

		// for (size_t j = 0; j < ts.size(); ++j) {
		// 	if (!ts[j])
		// 		continue;
		// 	std::cerr << get_rate_scale(i, ts) << " ";
		// }
		// std::cerr << std::endl;
	}

	return ans;
}

