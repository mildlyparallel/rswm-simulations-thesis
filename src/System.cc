#include <cassert>

#include "System.hh"
#include "TaskGraph.hh"

System::System()
{  }

System::System(size_t n)
: m_tasks(n)
{
	for (size_t i = 0; i < n; ++i)
		m_tasks[i].set_id(i);
}

System::~System()
{  }

const std::vector<Task> &System::tasks() const
{
	return m_tasks;
}

void System::init_total_work(std::function<double()> fn)
{
	for (auto &t : m_tasks)
		t.set_work_total(fn());
}

void System::init_total_work(std::function<double(const Task &)> fn)
{
	for (auto &t : m_tasks)
		t.set_work_total(fn(t));
}

void System::init_total_work(double p, double dp)
{
	for (auto &t : m_tasks) {
		t.set_work_total(p);
		p += dp;
	}
}

void System::init_total_work(size_t i, double p)
{
	assert(i < m_tasks.size());
	m_tasks[i].set_work_total(p);
}

void System::init_priority(std::function<double()> fn)
{
	for (auto &t : m_tasks)
		t.set_priority(fn());
}

void System::init_priority(std::function<double(const Task &)> fn)
{
	for (auto &t : m_tasks)
		t.set_priority(fn(t));
}

void System::init_priority(double p, double dp)
{
	for (auto &t : m_tasks) {
		t.set_priority(p);
		p += dp;
	}
}

void System::init_priority(size_t i, double p)
{
	assert(i < m_tasks.size());
	m_tasks[i].set_priority(p);
}

size_t System::size() const
{
	return m_tasks.size();
}

void System::set_id(unsigned id)
{
	m_id = id;
}

unsigned System::get_id() const
{
	return m_id;
}

void System::set_precedence(const TaskGraph &graph)
{
	assert(graph.nr_nodes() == m_tasks.size());

	for (size_t u = 0; u < graph.nr_nodes(); ++u) {
		Taskset ts;

		for (size_t v = 0; v < graph.nr_nodes(); ++v) {
			if (graph.have_edge(u, v))
				ts.set(v);
		}

		m_tasks[u].set_successors(ts);
	}

	m_feasible_ts = graph.get_feasible();
}

const std::vector<std::vector<Taskset>> &System::nonblocking_sets() const
{
	return m_nonblocking_sets;
}

const std::vector<Taskset> &System::feasible_combinations() const
{
	return m_feasible_ts;
}

void System::set_seed(int seed)
{
	m_seed = seed;
}

int System::get_seed() const
{
	return m_seed;
}
