#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "StrategyCombined.hh"

StrategyCombined::StrategyCombined()
{  }

StrategyCombined::~StrategyCombined()
{  }

const char *StrategyCombined::name() const
{
	if (!m_name.empty())
		return m_name.c_str();
	std::stringstream ss;
	ss << "comb-e" << m_efficiency << "-f" <<  m_fairness;
	static std::string s;
	s = ss.str();
	return s.c_str();
}

void StrategyCombined::set_name(const std::string &name)
{
	m_name = name;
}

double StrategyCombined::measure(Taskset ts) const
{
	double f = 0;

	double s = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		if (!ts[i])
			continue;

		s += get_rate_scale(i, ts);

		double a = std::pow(m_speed_sum[i], m_fairness);
		f += get_rate_scale(i, ts) / a;
	}

	f *= std::pow(s / ts.count(), m_efficiency);

	return f;
}

void StrategyCombined::start()
{
	m_speed_sum.resize(m_tasks.size());

	std::fill(m_speed_sum.begin(), m_speed_sum.end(), m_speed_eps);

	while (!all_completed()) {
		Taskset max_ts;
		double max_f = std::numeric_limits<double>::lowest();

		for (auto ts : m_system->feasible_combinations()) {
			if (!all_schedulable(ts))
				continue;

			double f = measure(ts);;

			if (f >= max_f) {
				max_f = f;
				max_ts = ts;
			}
		}

		// std::cerr << max_ts << " " << max_f << std::endl;

		assert(!max_ts.empty());

		double dt = std::min(min_remaining_time(max_ts), m_time_interval);

		run(max_ts, dt);

		for (size_t i = 0; i < m_speed_sum.size(); ++i) {
			if (max_ts[i])
				m_speed_sum[i] += get_rate_scale(i, max_ts);
		}

		if (any_completed(max_ts)) {
			std::fill(m_speed_sum.begin(), m_speed_sum.end(), m_speed_eps);
		}
	}
}

void StrategyCombined::set_interval(double dt)
{
	m_time_interval = dt;
}

void StrategyCombined::set_efficiency(double alpha)
{
	m_efficiency = alpha;
}

void StrategyCombined::set_fairness(double beta)
{
	m_fairness = beta;
}

