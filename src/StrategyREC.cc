#include <cassert>
#include <limits>
#include <iostream>

#include "StrategyREC.hh"

StrategyREC::StrategyREC()
: m_dot("rec.dot")
{  }

StrategyREC::~StrategyREC()
{  }

const char *StrategyREC::name() const
{
	return "rec";
}

double StrategyREC::max_runtime(
	const std::array<double, Taskset::max_size> &work,
	Taskset ts
) const {
	double min_time = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double t = work[i] / get_rate_scale(i, ts);

		if (t < min_time) {
			min_time = t;
		}

	}

	return min_time;
}

void print_ts(std::ostream &out, Taskset ts)
{
	// out << "{";
	bool comma = false;
	for (size_t i = 0; i < ts.size(); ++i) {
		if (!ts[i])
			continue;
		if (comma)
			out << ",";
		// out << "T_" << (i+1);
		out << (i+1);
		comma = true;
	}

	// out << "}";
}

std::tuple<Taskset, double, size_t> StrategyREC::measure(
	const std::array<double, Taskset::max_size> &work,
	Taskset schedulable,
	size_t depth,
	size_t iter,
	Taskset prev_ts,
	size_t prev_id,
	double cmax
) {
	size_t pp = prev_id + 1;

	if (schedulable.empty()) {
		m_dot << pp << " [label=\"t=" << cmax << "\"]\n";
		m_dot << prev_id << " -> " << pp << "\n";
		pp++;
		return {Taskset(), 0, pp};
	}

	Taskset min_ts;
	double min_estimate = std::numeric_limits<double>::max();

	// static size_t pp = 0;

	for (auto ts : m_system->feasible_combinations()) {
		if (!ts.subset_of(schedulable))
			continue;

		double dt = max_runtime(work, ts);

		Taskset next_schedulable = schedulable;
		std::array<double, Taskset::max_size> next_work(work);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			next_work[i] -= dt * get_rate_scale(i, ts);

			if (next_work[i] < 1e-3)
				next_schedulable.clear(i);
		}


		if (iter == 0) {
			if (depth == 0)
				m_dot << "\n";

			m_dot << pp << " [label=\"";
			print_ts(m_dot, ts);
			m_dot << "\"];\n";
			m_dot << prev_id << " -> " << pp; 
			m_dot << " [label=" << dt << "]\n";
		}

		auto [next_ts, next_dt, pp1] = measure(next_work, next_schedulable, depth + 1, iter, ts, pp, cmax + dt);
		pp = pp1;

		pp++;

		double estimate = dt + next_dt;
		if (estimate <= min_estimate) {
			min_estimate = estimate;
			min_ts = ts;
		}
	}

	return {min_ts, min_estimate, pp};
}

void StrategyREC::start()
{
	std::cerr << std::endl;
	std::cerr << "Non-intrrupt optimal" << std::endl;
	size_t iter = 0;

	m_dot << "digraph {\n";
	m_dot << "0 [label=\"\"]\n";

	while (!all_completed()) {
		std::array<double, Taskset::max_size> work;
		for (size_t i = 0; i < m_tasks.size(); ++i)
			work[i] = get_work_remain(i);

		auto [found, est, pp] = measure(work, get_schedulable(), 0, iter, {}, 0);

		assert(!found.empty());

		double dt = min_remaining_time(found);
		double b = dt * get_rate_scale(found);

		std::cerr << "Found: " << found << " b:" << b << " t:" << dt << std::endl;
		std::cerr << std::endl;

		run(found, dt);
		iter++;
	}

	m_dot << "}";
}


