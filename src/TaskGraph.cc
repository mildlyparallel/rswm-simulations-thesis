#include <cassert>
#include <fstream>
#include <sstream>
#include <chrono>
#include <random>
#include <iostream>
#include <algorithm>

#include "TaskGraph.hh"
#include "CombinationGraph.hh"

std::string TaskGraph::get_node_label(size_t u) const
{
	return std::to_string(m_tasks[u].get_id());
}

void TaskGraph::add_task(const Task &task)
{
	m_tasks.push_back(task);
	add_node();
}

void TaskGraph::add_tasks_no_prec(size_t n)
{
	assert(nr_nodes() == 0);
	assert(m_tasks.empty());

	for (size_t i = 0; i < n; ++i) {
		Task task;
		task.set_id(i);
		add_task(task);
	}

	m_feasible.clear();

	Taskset comb;
	while (comb.next_combination(0, nr_nodes())) {
		m_feasible.push_back(comb);
	}
}

void TaskGraph::add_tasks(size_t n)
{
	assert(nr_nodes() == 0);
	assert(m_tasks.empty());

	for (size_t i = 0; i < n; ++i) {
		Task task;
		task.set_id(i);
		add_task(task);
	}
}

void TaskGraph::read_dot(const std::string &p)
{
	std::ifstream in(p);
	in.ignore(std::numeric_limits<std::streamsize>::max(), '{');

	std::vector<std::pair<size_t, size_t>> edges;
	size_t max_node = 0;

	while (in.good()) {
		size_t u = 0;
		size_t v = 0;
		std::string delim;

		in >> u >> delim >> v >> delim;
		if (u == v)
			break;

		if (u > max_node)
			max_node = u;
		if (v > max_node)
			max_node = v;

		edges.push_back({u, v});
	}

	add_tasks(max_node + 1);

	for (auto [u, v] : edges) {
		add_edge(u, v);
	}
}

// void TaskGraph::create_combination_graph()
// {
// 	Nodeset nodes;
//
// 	while (nodes.next_combination(0, nr_nodes())) {
// 		if (nodes.count() <= 1)
// 			continue;
//
// 		if (is_root(nodes) || is_leaf(nodes))
// 			continue;
//
// 		if (reachable(nodes))
// 			continue;
//
// 		m_comb_graph.add(nodes);
// 	}

// 	for (size_t u = 0; u < m_comb_graph.nr_nodes(); ++u) {
// 		for (size_t v = 0; v < m_comb_graph.nr_nodes(); ++v) {
// 			if (u == v)
// 				continue;
//
// 			if (reachable_any(m_comb_graph[u], m_comb_graph[v]))
// 				m_comb_graph.add_edge(u, v);
// 		}
// 	}
//
// 	std::cout << "Combinations: " << m_comb_graph.nr_nodes() << std::endl;
// 	std::cout << "Combination edges: " << m_comb_graph.nr_edges() << std::endl;
// }

// void TaskGraph::fvs_to_combinations()
// {
// 	m_combinations.clear();
//
// 	for (const auto &nodes : m_comb_graph.get_fvs()) {
// 		std::vector<Nodeset> taskcombs;
// 		for (size_t i = 0; i < m_tasks.size(); ++i) {
// 			taskcombs.push_back(Nodeset({i}));
// 		}
//
// 		for (size_t i = 0; i < m_comb_graph.nr_nodes(); ++i) {
// 			if (nodes[i])
// 				taskcombs.push_back(m_comb_graph[i]);
// 		}
//
// 		m_combinations.push_back(std::move(taskcombs));
// 	}
// }

// void TaskGraph::find_loops()
// {
// 	std::cout << "Listing loops ... ";
// 	std::cout.flush();
//
// 	auto st = std::chrono::high_resolution_clock::now();
//
// 	auto loops = m_comb_graph.list_loops();
//
// 	auto en = std::chrono::high_resolution_clock::now();
// 	double dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count() / 1e3;
//
// 	std::cout << "Done (" << dt << " sec)" << std::endl;
//
// 	m_nr_loops = loops.size();
//
// 	std::cout << "Found " << m_nr_loops << " loops" << std::endl;
// }

// void TaskGraph::find_fvs()
// {
// 	std::cout << "Listing FVS ... ";
// 	std::cout.flush();
//
// 	auto st = std::chrono::high_resolution_clock::now();
//
// 	auto fvs = m_comb_graph.list_maximal_fvs();
//
// 	auto en = std::chrono::high_resolution_clock::now();
// 	double dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count() / 1e3;
//
// 	std::cout << "Done (" << dt << " sec)" << std::endl;
//
// 	std::cout << "Found " << fvs.size() << " FVS" << std::endl;
// }

// const TaskGraph::CombinationsList &TaskGraph::find_combinations()
// {
// 	if (!m_combinations.empty())
// 		return m_combinations;
//
// 	create_combination_graph();
//
// 	find_loops();
//
// 	find_fvs();
//
// 	fvs_to_combinations();
//
// 	return m_combinations;
// }

const std::vector<Taskset> &TaskGraph::get_feasible() const
{
	return m_feasible;
}

void TaskGraph::list_feasible()
{
	m_feasible.clear();
	m_lockfree.clear();

	std::cout << "Finding feasible combinations" << std::endl;

	Taskset comb;
	while (comb.next_combination(0, nr_nodes())) {
		if (comb.size() <= 1) {
			m_feasible.push_back(comb);
			m_lockfree.insert(comb);
			continue;
		}

		if (is_root(comb) || is_leaf(comb)) {
			m_feasible.push_back(comb);
			m_lockfree.insert(comb);
			continue;
		}

		if (reachable(comb))
			continue;

		m_feasible.push_back(comb);
		std::cout << "Found " << m_feasible.size() << "\r";
	}

	std::cout << std::endl;
}

void TaskGraph::find_any_lockfree(std::default_random_engine &gen)
{
	std::set<size_t> skipped;

	assert(!m_feasible.empty());

	CombinationGraph fvs;

	std::cerr << "Shuffling" << std::endl;

	std::shuffle(m_feasible.begin(), m_feasible.end(), gen);

	std::cerr << "Finding FVS" << std::endl;

	for (size_t u = 0; u < m_feasible.size(); u++) {
		auto cu = m_feasible[u];
		fvs.add_node();

		for (size_t v = 0; v < fvs.nr_nodes(); ++v) {
			auto cv = m_feasible[v];
			if (cu == cv)
				continue;

			if (skipped.contains(v))
				continue;

			if (reachable_any(cu, cv))
				fvs.add_edge(u, v);

			if (reachable_any(cv, cu))
				fvs.add_edge(v, u);
		}

		if (fvs.have_loop()) {
			skipped.insert(u);
			fvs.remove_edges(u);
		} else {
			m_lockfree.insert(cu);
		}
	}
}

const std::set<Taskset> &TaskGraph::get_lockfree() const
{
	return m_lockfree;
}

// const TaskGraph::CombinationsList &TaskGraph::get_combinations() const
// {
// 	return m_combinations;
// }
//
// void TaskGraph::write_combinations(const std::string &file) const
// {
// 	std::ofstream out(file);
// 	out << m_combinations.size() << "\n";
//
// 	for (const auto &v : m_combinations) {
// 		out << v.size() << "\n";
//
// 		for (const auto &nodes : v) {
// 			out << "  " << nodes.count() << " ";
// 			for (size_t i = 0; i < nodes.size(); ++i) {
// 				if (nodes[i])
// 					out << i << " ";
// 			}
// 			out << "\n";
// 		}
//
// 		out << "\n";
// 	}
// }

// void TaskGraph::read_combinations(const std::string &file)
// {
// 	std::ifstream in(file);
//
// 	size_t combsets = 0;
// 	in >> combsets;
//
// 	m_combinations.clear();
//
// 	m_combinations.resize(combsets);
//
// 	for (size_t i = 0; i < combsets; ++i) {
// 		size_t setsize = 0;
// 		in >> setsize;
//
// 		m_combinations[i].resize(setsize);
//
// 		for (size_t j = 0; j < setsize; ++j) {
// 			size_t nodesize = 0;
// 			in >> nodesize;
//
// 			Nodeset nodes;
//
// 			for (size_t k = 0; k < nodesize; ++k) {
// 				size_t id = 0;
// 				in >> id;
//
// 				nodes.set(id);
// 			}
//
// 			m_combinations[i][j] = nodes;
// 		}
// 	}
// }

// void TaskGraph::write_meta(const std::string &file) const
// {
// 	std::ofstream out(file);
//
// 	out << "tasks_nodes " << nr_nodes() << "\n";
// 	out << "tasks_edges " << nr_edges() << "\n";
// 	// out << "combs_nodes " << m_comb_graph.nr_nodes() << "\n";
// 	// out << "combs_edges " << m_comb_graph.nr_edges() << "\n";
// 	out << "nr_loops " << m_nr_loops << "\n";
// 	out << "nr_fvs " << m_combinations.size() << "\n";
//
// 	bool fvs_size_eq = true;
// 	size_t fvs_size = 0;
//
// 	for (auto fvs : m_combinations) {
// 		if (fvs_size == 0)
// 			fvs_size = fvs.size();
// 		if (fvs_size != fvs.size()) {
// 			fvs_size_eq = false;
// 			break;
// 		}
// 	}
//
// 	out << "fvs_size " << fvs_size << "\n";
// 	out << "fvs_size_eq " << fvs_size_eq << "\n";
// }

// void TaskGraph::write_all(const std::string &dir) const
// {
// 	Graph::write(dir + "/tasks.graph");
// 	print_dot_directed(dir + "/tasks.dot");
//
// 	write_combinations(dir + "/combs.txt");
//
// 	write_meta(dir + "/meta.txt");
//
// 	// if (m_comb_graph.nr_nodes() > 30)
// 	// 	return;
//     //
// 	// m_comb_graph.write(dir + "/combs.graph");
// 	// m_comb_graph.print_dot_directed(dir + "/combs.dot");
//     //
// 	// size_t n = 0;
// 	// for (auto nodes : m_comb_graph.get_fvs()) {
// 	// 	std::stringstream file;
// 	// 	file << dir << "/fvs-" << n << ".dot";
// 	// 	m_comb_graph.print_dot_directed(file.str(), nodes);
// 	// 	n++;
//     //
// 	// 	if (n == 10)
// 	// 		break;
// 	// }
// }

// void TaskGraph::read_all(const std::string &dir)
// {
// 	Graph::read(dir + "/tasks.graph");
//
// 	read_combinations(dir + "/combs.txt");
// }

