#include <cassert>
#include <algorithm>
#include <limits>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "StrategyRR.hh"

StrategyRR::StrategyRR()
{  }

StrategyRR::~StrategyRR()
{  }

const char *StrategyRR::name() const
{
	if (m_fun == WorkSum)
		return "rr-work-sum";
	if (m_fun == WorkSum2)
		return "rr-work-sum-2";
	if (m_fun == ScaledSpeed)
		return "rr-scaled-speed";
	if (m_fun == ScaledSpeed2)
		return "rr-scaled-speed-2";
	if (m_fun == ScaledSpeed3)
		return "rr-scaled-speed-3";
	if (m_fun == ScaledSpeed4)
		return "rr-scaled-speed-4";
	if (m_fun == ScaledSpeed5)
		return "rr-scaled-speed-5";
	if (m_fun == ScaledSpeedMMF)
		return "rr-scaled-speed-mmf";
	if (m_fun == ScaledSpeedJian)
		return "rr-scaled-speed-jian";
	if (m_fun == LinearSpeed)
		return "rr-linear";
	if (m_fun == Utility)
		return "rr-util";
	if (m_fun == Angle)
		return "rr-angle";
	if (m_fun == Variance)
		return "rr-var";
	if (m_fun == Median)
		return "rr-med";
	if (m_fun == MinWork)
		return "rr-minw";
	return "unknown";
}

double StrategyRR::measure_min_work(Taskset ts) const
{
	double min_w = std::numeric_limits<double>::max();

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);

		if (w < min_w)
			min_w = w;
	}

	assert(min_w < std::numeric_limits<double>::max());
	return min_w;
}

double StrategyRR::measure_scaled_speed(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);

		if (w == 0)
			continue;

		double s = get_rate_scale(i, ts) / w;

		scale += s;
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::measure_work_sum(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);

		scale += w;
	}

	return scale;
}

double StrategyRR::measure_work_sum_2(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);
		w = 1/w;

		scale += w*w;
	}

	return scale;
}

double StrategyRR::measure_scaled_speed_2(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		// double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i];

		if (w == 0) {
			scale += get_rate_scale(i, ts);
			continue;
		}

		scale += get_rate_scale(i, ts) / w;
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::get_scaled_speed_3(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double a = m_weight[i];

		scale += get_rate_scale(i, ts) / a;
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::measure_scaled_speed_3(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		if (!ts[i])
			continue;

		double a = 1. + m_weight[i] / m_time_interval;

		scale += get_rate_scale(i, ts) / a;
		// scale += 1. / a;
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::get_linear_speed(Taskset ts) const
{
	double as = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		as += m_weight[i];
	}

	as /= ts.count();

	return get_rate_scale(ts) / (as);
}

double StrategyRR::measure_linear_speed(Taskset ts) const
{
	double as = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		if (ts[i])
			as += m_weight[i] / m_time_interval;
	}

	as /= ts.count();

	return get_rate_scale(ts) / (as);
}

double StrategyRR::measure_utility(Taskset ts) const
{
	double as = 0;
	// double as = 1;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double a = 0;
		a = m_weight[i] + m_time_interval * get_rate_scale(i, ts);

		// as *= 1. + a;
		as += std::log(1. + a);
		// as += a;
	}

	return as;
	// return std::log(as);
}

double StrategyRR::measure_angle(Taskset ts) const
{
	double as = 0;
	double len = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double r = m_weight[i] + m_time_interval * get_rate_scale(i, ts);

		len += r * r;

		as += r;
	}

	len = std::sqrt(len);

	return as / len;
}

double StrategyRR::find_min_speed_sum() const
{
	double min_a = 100;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double a = m_weight[i] / m_time_interval;
		a = std::max(a, 1.);

		if (a < min_a)
			min_a = a;
	}

	return min_a;
}

double StrategyRR::measure_scaled_speed_4(Taskset ts) const
{
	double scale = 0;

	double min_sum = find_min_speed_sum();

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double a = m_weight[i] / m_time_interval;
		a = std::max(a, 1.);

		if (min_sum > 2)
			a -= 1;

		scale += get_rate_scale(i, ts) / a;
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::measure_scaled_speed_5(Taskset ts) const
{
	double scale = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double a = 1. + m_weight[i] / m_time_interval;

		scale -= a / get_rate_scale(i, ts);
	}

	if (scale == 0)
		return get_rate_scale(ts);

	return scale;
}

double StrategyRR::measure_scaled_speed_mmf(Taskset ts) const
{
	double max_r = 0, min_r = 1e9;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double r = m_weight[i] + dt * get_rate_scale(i, ts);

		if (r > max_r)
			max_r = r;
		if (r < min_r)
			min_r = r;
	}

	double a = get_rate_scale(ts);

	if (max_r == 0)
		return a;

	double theta = min_r / max_r;

	return a * theta;
}

double StrategyRR::measure_scaled_speed_jian(Taskset ts) const
{
	double r_sum = 0;
	double r2_sum = 0;
	double n = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double r = m_weight[i] + dt * get_rate_scale(i, ts);

		r_sum += r;
		r2_sum += r * r;
		n += 1;
	}

	double a = get_rate_scale(ts);

	if (r_sum == 0 || n == 0)
		return a;

	double theta = r_sum * r_sum / (r2_sum * n);

	return a * theta;
}

double StrategyRR::mesaure_variance(Taskset ts) const
{
	double mean = 0;
	double var = 0;
	size_t n = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		n++;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);
		mean += w;
	}

	if (n == 1)
		return 0;

	mean /= n;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);
		var += (w - mean) * (w - mean);
	}

	var /= n - 1;

	return -var;
}

double StrategyRR::measure_median(Taskset ts) const
{
	std::vector<double> wnext;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		double dt = std::min(min_remaining_time(ts), m_time_interval);
		double w = m_weight[i] + dt * get_rate_scale(i, ts);
		wnext.push_back(w);
	}

	std::sort(wnext.begin(), wnext.end());

	return wnext[wnext.size() / 2];
}

double StrategyRR::measure(Taskset ts) const
{
	double scale = 0;

	if (m_fun == WorkSum)
		scale = measure_work_sum(ts);
	if (m_fun == WorkSum2)
		scale = measure_work_sum_2(ts);
	if (m_fun == ScaledSpeed)
		scale = measure_scaled_speed(ts);
	if (m_fun == ScaledSpeed2)
		scale = measure_scaled_speed_2(ts);
	if (m_fun == ScaledSpeed3)
		scale = measure_scaled_speed_3(ts);
	if (m_fun == ScaledSpeed4)
		scale = measure_scaled_speed_4(ts);
	if (m_fun == ScaledSpeed5)
		scale = measure_scaled_speed_5(ts);
	if (m_fun == ScaledSpeedMMF)
		scale = measure_scaled_speed_mmf(ts);
	if (m_fun == ScaledSpeedJian)
		scale = measure_scaled_speed_jian(ts);
	if (m_fun == LinearSpeed)
		scale = measure_linear_speed(ts);
	if (m_fun == Utility)
		scale = measure_utility(ts);
	if (m_fun == Angle)
		scale = measure_angle(ts);
	if (m_fun == Variance)
		scale = mesaure_variance(ts);
	if (m_fun == Median)
		scale = measure_median(ts);
	if (m_fun == MinWork)
		scale = measure_min_work(ts);

	return scale;
}

void StrategyRR::start()
{
#ifndef NDEBUG
	// std::cerr << std::endl;
	// std::cerr << "RR " << name() << " dt=" << m_time_interval << std::endl;
#endif

	m_weight.resize(m_tasks.size());
	// std::fill(m_weight.begin(), m_weight.end(), 1./m_tasks.size());
	std::fill(m_weight.begin(), m_weight.end(), 1e-17);

	bool printed = false;
	while (!all_completed()) {
		Taskset max_ts;
		double max_scale = std::numeric_limits<double>::lowest();

		for (auto ts : m_system->feasible_combinations()) {

			if (!all_schedulable(ts))
				continue;

			double scale = measure(ts);;

#ifndef NDEBUG
			// std::cerr << "  " << ts << " " << scale << std::endl;
#endif

			if (scale >= max_scale) {
				max_scale = scale;
				max_ts = ts;
			}
		}

		assert(!max_ts.empty());

		double dt = std::min(min_remaining_time(max_ts), m_time_interval);

#ifndef NDEBUG
		std::cerr << std::setw(4) << now() << " ";
		std::stringstream ss;
		ss << max_ts;
		std::cerr << std::setw(10) << ss.str() << " ";
		std::cerr << std::setw(5) << get_rate_scale(max_ts) << " ";
		std::cerr << std::setw(10) << max_scale << " " << dt;
		std::cerr << " | ";

		// std::cerr << now();
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			// double r = 1. + m_weight[i] / m_time_interval;
			double r = 0.;
			// if (now() > 0)
				// r = (m_weight[i]) / now();
			r = m_weight[i];

			// std::cerr << "," << r;
			std::cerr << r << " ";
		}

		std::cerr << " | " << get_fairness();
		std::cerr << " | " << get_avg_speed() << std::endl;
#endif

		run(max_ts, dt);

		for (size_t i = 0; i < m_weight.size(); ++i) {
			if (max_ts[i])
				m_weight[i] += dt * get_rate_scale(i, max_ts);
		}

		// if (now() > 1.) {
		// 	std::cout << "now() = " << now() << std::endl;
		// 	return;
		// }

		if (any_completed(max_ts)) {

			if (!printed) {
				// for (auto ts : m_system->feasible_combinations()) {
				// 	double scale = 0;
                //
				// 	if (get_subset_time(ts) < m_time_interval)
				// 		continue;
                //
				// 	for (size_t i = 0; i < m_tasks.size(); ++i) {
				// 		double a = 1. + m_weight[i] / m_time_interval;
				// 		scale += get_rate_scale(i, ts) / a;
				// 	}
                //
				// 	std::cerr << ts << " " << get_subset_time(ts) << " " << scale << std::endl;
				// }
				// std::cerr << get_avg_speed();
				// std::cerr << get_fairness() << "," << get_avg_speed();
				printed = true;
			}
			if (m_stop_after_first) {
				return;
			}

			std::fill(m_weight.begin(), m_weight.end(), 0);
		}
	}
}

bool StrategyRR::validate(const std::vector<Taskset> &sched)
{
	m_weight.resize(m_tasks.size());
	std::fill(m_weight.begin(), m_weight.end(), 0);

#ifndef NDEBUG
	std::cerr << std::endl;
	std::cerr << "RR " << name() << " dt=" << m_time_interval << std::endl;
#endif

	for (size_t i = 0; i < sched.size(); ++i) {
		if (all_completed()) {
			std::cerr << "All tasks completed before end" << std::endl;
			return false;
		}

		if (!all_schedulable(sched[i])) {
			std::cerr << "Some tasks are not schedulable in " << sched[i] << std::endl;
			return false;
		}

		Taskset max_ts;
		double max_scale = 0;

		for (auto ts : m_system->feasible_combinations()) {
			if (!all_schedulable(ts))
				continue;

			double scale = measure(ts);

#ifndef NDEBUG
			// std::cerr << "  " << ts << " " << scale << std::endl;
#endif

			if (scale >= max_scale) {
				max_scale = scale;
				max_ts = ts;
			}
		}

		double cur_scale = measure(sched[i]);
		if (max_scale - 1e-6 > cur_scale) {
			std::cerr << "cur_scale = " << cur_scale << std::endl;
			std::cerr << "Found " << max_ts << " " << max_scale << std::endl;
			return false;
		}

		double dt = std::min(min_remaining_time(max_ts), m_time_interval);

		run(sched[i], dt);

		for (size_t k = 0; k < m_weight.size(); ++k) {
			if (sched[i][k])
				m_weight[k] += dt * get_rate_scale(k, sched[i]);
		}

		if (any_completed(max_ts))
			std::fill(m_weight.begin(), m_weight.end(), 0);

#ifndef NDEBUG

		std::cerr << std::setw(4) << now() << " ";
		std::stringstream ss;
		ss << sched[i];
		std::cerr << std::setw(10) << ss.str() << " ";
		std::cerr << std::setw(5) << get_rate_scale(max_ts) << " ";
		std::cerr << std::setw(10) << max_scale << " " << dt;

		std::cerr << " | ";

		double sum_r = 0;
		for (size_t i = 0; i < m_tasks.size(); ++i)
			sum_r = 1. + m_weight[i] / m_time_interval;

		double max_r = 0, min_r = 1e9;
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			double r = 1. + m_weight[i] / m_time_interval;
			r /= sum_r;

			if (!m_tasks[i].completed() && m_tasks[i].runnable()) {
				if (r > max_r)
					max_r = r;
				if (r < min_r)
					min_r = r;
			}
			std::cerr << r << " ";
		}

		std::cerr << " | " << min_r / max_r << std::endl;
#endif
	}

	if (!all_completed()) {
		std::cerr << "Some tasks did not completed" << std::endl;
		return false;
	}

	return true;
}

void StrategyRR::set_acq_fun(AccFun fn)
{
	m_fun = fn;
}

void StrategyRR::set_interval(double dt)
{
	m_time_interval = dt;
}

