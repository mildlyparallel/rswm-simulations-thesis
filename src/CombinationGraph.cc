#include <cassert>
#include <iostream>

#include "CombinationGraph.hh"

void CombinationGraph::add_node()
{
	m_edges.push_back({});
}

void CombinationGraph::add_edge(size_t u, size_t v)
{
	assert(u < nr_nodes());
	assert(v < nr_nodes());

	m_edges[u].push_back(v);
	m_nr_edges++;
}

void CombinationGraph::remove_edges(size_t u)
{
	assert(u < nr_nodes());
	m_edges[u].clear();

	for (size_t v = 0; v < nr_nodes(); ++v) {
		for (size_t i = 0; i < m_edges[v].size(); ++i) {
			if (m_edges[v][i] == u)
				m_edges[v][i] = npos;
		}
	}
}

void CombinationGraph::dfs(
	size_t u,
	std::function<bool (size_t u, size_t v)> enter_fn,
	std::function<void (size_t u)> leave_fn
) const {

	assert(u < m_edges.size());
	// if (u >= m_edges.size()) {
	// 	if (leave_fn)
	// 		leave_fn(u);
	// 	return;
	// }

	for (size_t i = 0; i < m_edges[u].size(); ++i) {
		size_t v = m_edges[u][i];
		if (v == npos)
			continue;
		if (enter_fn(u, v))
			dfs(v, enter_fn, leave_fn);
	}

	if (leave_fn)
		leave_fn(u);
}

bool CombinationGraph::have_loop() const
{
	std::vector<bool> black(nr_nodes());
	std::vector<bool> gray(nr_nodes());

	std::fill(black.begin(), black.end(), false);

	for (size_t node = 0; node < nr_nodes(); ++node) {
		if (black[node])
			continue;

		std::fill(gray.begin(), gray.end(), false);
		gray[node] = true;
		bool found = false;

		dfs(node, 
			[&](size_t, size_t v) {
				if (found)
					return false;

				if (black[v])
					return false;

				if (gray[v]) {
					found = true;
					return false;
				}

				gray[v] = true;
				return true;
			},
			[&](size_t u) {
				assert(gray[u]);
				gray[u] = false;
				black[u] = true;
			}
		);

		if (found)
			return true;
	}

	return false;
}
