#include <cassert>
#include <cmath>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "StrategySearch.hh"

StrategySearch::StrategySearch()
{  }

StrategySearch::~StrategySearch()
{  }

void StrategySearch::init()
{
	m_interpolators.resize(m_tasks.size());
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		m_interpolators[i].set_task_id(i);
		m_interpolators[i].set_nr_tasks(m_tasks.size());
	}

}

void StrategySearch::open_log_file(const std::string &p)
{
	m_log = std::ofstream(p);
	m_log << "time,"; 
	m_log << "measured,"; 
	m_log << "acq_fun_error,";
	m_log << "rmsd,";
	m_log << "mae\n";
}

const char *StrategySearch::name() const
{
	if (!m_name.empty())
		return m_name.c_str();
	std::stringstream ss;
	ss << "comb-e" << m_efficiency << "-f" <<  m_fairness;
	static std::string s;
	s = ss.str();
	return s.c_str();
}

void StrategySearch::collect_all_ideal()
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		Taskset ts({i});

		run(ts, m_time_interval);

		double a = m_tasks[i].get_rate_last();
		assert(fabs(a - 1.) < 1e-3);
	}
}

void StrategySearch::run_and_measure(Taskset ts)
{
	assert(!any_completed(ts));

	double dt = std::min(min_remaining_time(ts), m_time_interval);

	run(ts, dt);

	m_measured_ts.insert(ts);

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		double a = m_tasks[i].get_rate_last();
		m_interpolators[i].add(ts, a);

		// TODO: store and compute error
	}
}

std::tuple<double, double> StrategySearch::measure(Taskset ts) const
{
	double mean = 0;
	double var = 0;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;
		if (!ts[i])
			continue;

		auto est = m_interpolators[i].get(ts);

		double m = est.mean;
		double s = est.delta / m_quratile;

		double ri = std::pow(m_speed_sum[i], m_fairness);
		mean += m / ri;
		var += s * s / (ri * ri);
	}

	var = std::sqrt(var);

	return {mean, var};
}

double StrategySearch::measure_ucb(Taskset ts) const
{
	double f = 0;

	double s = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		if (!ts[i])
			continue;

		auto est = m_interpolators[i].get(ts);

		double a = est.mean + est.delta * m_quratile;

		s += a;

		double ri = std::pow(m_speed_sum[i], m_fairness);
		f += a / ri;
	}

	f *= std::pow(s / ts.count(), m_efficiency);

	return f;
}

double StrategySearch::expected_improvement(Taskset ts) const
{
	auto [mean, var] = measure(ts);

	if (var == 0) {
		if (mean <= m_last_acq_fun_val)
			return 0;
		return mean - m_last_acq_fun_val - m_explore;
	}

	double e = mean - m_last_acq_fun_val - m_explore;
	double x = mean / var;
	return e * pnorm(x) + var * dnorm(x, 0, 1);
}

double StrategySearch::measure_exact(Taskset ts) const
{
	double f = 0;

	double s = 0;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		if (!ts[i])
			continue;

		s += get_rate_scale(i, ts);

		double a = std::pow(m_speed_sum[i], m_fairness);
		f += get_rate_scale(i, ts) / a;
	}

	f *= std::pow(s / ts.count(), m_efficiency);

	return f;
}

double StrategySearch::find_max_acq_fun() const
{
	Taskset max_ts;
	double max_f = std::numeric_limits<double>::lowest();

	for (auto ts : m_system->feasible_combinations()) {
		if (!all_schedulable(ts))
			continue;

		double f = measure_exact(ts);;

		if (f >= max_f) {
			max_f = f;
			max_ts = ts;
		}
	}

	return max_f;
}

std::tuple<double, double> StrategySearch::measure_est_error() const
{
	size_t nr_total = 0;
	double err_rmsd = 0;
	double err_mae = 0;

	for (auto ts : m_system->feasible_combinations()) {
		if (!all_schedulable(ts))
			continue;

		if (m_measured_ts.contains(ts))
			continue;

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (m_tasks[i].completed() || !m_tasks[i].runnable())
				continue;
			if (!ts[i])
				continue;

			auto est = m_interpolators[i].get(ts);
			double exact = get_rate_scale(i, ts);

			err_rmsd += (est.mean - exact) * (est.mean - exact);
			err_mae += std::abs(est.mean - exact);

			nr_total++;
		}
	}

	if (nr_total == 0)
		return {0, 0};

	err_rmsd = std::sqrt(err_rmsd / nr_total);
	err_mae /= nr_total;

	return {err_rmsd, err_mae};
}

Taskset StrategySearch::run_next()
{
	assert(!all_completed());

	Taskset max_ts;
	double max_f = std::numeric_limits<double>::lowest();

	size_t nr_total_comb = 0;
	size_t nr_measured = 0;

	for (auto ts : m_system->feasible_combinations()) {

		if (!all_schedulable(ts))
			continue;
		nr_total_comb++;
		if (m_measured_ts.contains(ts))
			nr_measured++;

		// double f = expected_improvement(ts);;
		double f = measure_ucb(ts);;

		if (f >= max_f) {
			max_f = f;
			max_ts = ts;
		}
	}

	assert(!max_ts.empty());

	double exact_acq_fun = measure_exact(max_ts);
	double acq_fun_best = find_max_acq_fun();

	run_and_measure(max_ts);
	m_last_acq_fun_val = max_f;

	double f_error = exact_acq_fun / acq_fun_best;

	auto [rmsd, mae] = measure_est_error();

	double measured = 1. * nr_measured / nr_total_comb;

	m_log << now() << ",";
	m_log << measured << ",";
	m_log << f_error << ",";
	m_log << rmsd << ",";
	m_log << mae << "\n";

	return max_ts;
}

void StrategySearch::start()
{
	init();

	m_speed_sum.resize(m_tasks.size());
	std::fill(m_speed_sum.begin(), m_speed_sum.end(), m_speed_eps);

	// collect_all_ideal();

	while (!all_completed()) {
		Taskset ts = run_next();

		for (size_t i = 0; i < m_speed_sum.size(); ++i) {
			if (ts[i])
				m_speed_sum[i] += get_rate_scale(i, ts);
		}

		if (any_completed(ts)) {
			std::fill(m_speed_sum.begin(), m_speed_sum.end(), m_speed_eps);
		}
	}
}

void StrategySearch::set_quartile(double q)
{
	m_quratile = q;
}

double StrategySearch::dnorm(double x, double a, double v)
{
	double m = (x - a) / v;
	double d = 1;
	d *= 1. / (v * std::sqrt(2. * M_PI));
	d *= std::exp(-0.5 * (m * m));
	return m;
}

double StrategySearch::pnorm(double x)
{
	double p = 0.5 * (1 + std::erf(x / std::sqrt(2)));
	return p;
}

void StrategySearch::set_explore_coeff(double e)
{
	m_explore = e;
}

void StrategySearch::set_fairness(double f)
{
	m_fairness = f;
}

void StrategySearch::set_efficiency(double e)
{
	m_efficiency = e;
}

void StrategySearch::set_interval(double dt)
{
	m_time_interval = dt;
}

void StrategySearch::set_name(const std::string &name)
{
	m_name = name;
}

