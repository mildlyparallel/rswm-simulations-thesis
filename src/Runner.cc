#include <cassert>

#include "Runner.hh"

#include "Strategy.hh"
#include "SystemStationary.hh"

Runner::Runner()
{  }

Runner::~Runner()
{  }

void Runner::add(Strategy *strategy)
{
	m_strategies.push_back(strategy);
}

void Runner::open(const std::string &filepath)
{
	assert(!m_out.is_open());

	m_out = std::ofstream(filepath);
}

void Runner::print_header()
{
	m_out << "id,seed,size";

	for (auto &s : m_strategies)
		m_out << "," << s->name();

	m_out << std::endl;
}

void Runner::run(const SystemStationary &system, bool is_fairness)
{
	m_out << system.get_id();
	m_out << "," << system.get_seed();
	m_out << "," << system.size();

	for (auto &s : m_strategies) {
		s->reset();

		s->set_input(&system);

		if (is_fairness)
			s->stop_after_first(true);

		s->start();

		m_out << ",";

		if (is_fairness)
			m_out << s->get_fairness_jian();
		else
			m_out << s->get_makespan();

	}

	m_out << std::endl;
}
