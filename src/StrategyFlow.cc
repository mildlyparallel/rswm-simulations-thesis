#include <cassert>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>

#include <Problem.hh>

#include "StrategyFlow.hh"

StrategyFlow::StrategyFlow()
{  }

StrategyFlow::~StrategyFlow()
{  }

const char *StrategyFlow::name() const
{
	return "flowtimeopt";
}

void StrategyFlow::start()
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	Answer ans;
	ans.order.resize(nr_tasks);
	ans.distr.resize(nr_combs);
	solve(ans);

	// std::cout << "Order: "; for (auto i : ans.order)
	// 	std::cout << i << " ";
	// std::cout << " ->  "  << ans.obj << std::endl;
	// is_in_order(ans);

	for (auto i : ans.order) {
		for (size_t j = 0; j < nr_combs; ++j) {
			taskset ts(j + 1);
			if (!ts[i])
				continue;

			if (ans.distr[j] == 0)
				continue;

			// std::bitset<8> ts8(j+1);
			// std::cerr << "run " << ts8 << " " << ans.distr[j] << " ";
			// for (size_t i = nr_tasks; i > 0; i--) {
			// 	if (ts[nr_tasks - i])
			// 		std::cerr << get_rate_scale(nr_tasks - i, ts) << " ";
			// 	else
			// 		std::cerr << 0 << " ";
			// }
			// std::cerr << std::endl;

			run(ts, ans.distr[j]);

			ans.distr[j] = 0;
		}
	}

	assert(all_completed());
	assert(std::fabs(ans.obj - get_flowtime()) < 1e-3);
}

void StrategyFlow::solve(StrategyFlow::Answer &answer)
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	Answer ans;
	ans.order.resize(nr_tasks);
	ans.distr.resize(nr_combs);

	for (size_t i = 0; i < nr_tasks; ++i)
		ans.order[i] = i;

	// ans.order = {0, 2, 1, 3};

	answer.obj = std::numeric_limits<double>::max();

	do {
		std::fill(ans.distr.begin(), ans.distr.end(), 0);

		// for (size_t i = 0; i < nr_tasks; ++i)
		// 	std::cout << ans.order[i] << " ";

		solve_one(ans);

		// std::cout << " -> " << ans.obj << std::endl;

		if (ans.obj < answer.obj) {
			answer.obj = ans.obj;

			for (size_t i = 0; i < nr_tasks; ++i) {
				answer.order[i] = ans.order[i];
			}

			for (size_t j = 0; j < nr_combs; ++j) {
				answer.distr[j] = ans.distr[j];
			}
		}

		// break;

	} while(std::next_permutation(ans.order.begin(), ans.order.end()));
}

void StrategyFlow::solve_one(Answer &ans)
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	using namespace lpcpp;

	Problem lp(nr_combs);

	lp.set_verbose(Problem::Important);

	for (size_t i = 0; i < nr_tasks; ++i) {
		for (size_t j = 0; j < nr_combs; ++j)
			lp[j] = get_rate_scale(i, taskset(j+1));

		lp.add_constraint(Problem::Equal, get_work_total(i));
	}

	lp.set_bound_lower_all(0);

	lp.fill_row(0);
	for (size_t k = 0; k < nr_tasks; ++k) {
		for (size_t j = 0; j < nr_combs; ++j) {
			taskset ts(j+1);

			bool has = false;

			for (size_t i = 0; i <= k; ++i) {
				if (ts[ans.order[i]]) {
					has = true;
					break;
				}
			}

			if (has)
				lp[j] += get_priority(ans.order[k]);
		}
	}

	lp.set_objective_minim();

	// lp.print_lp();

	lp.optimize();

	ans.obj = lp.get_objective();

	for (size_t j = 0; j < nr_combs; j++) {
		if (lp[j] <= 1e-6) {
			ans.distr[j] = 0;
			continue;
		}

		ans.distr[j] = lp[j];
	}

}

bool StrategyFlow::is_in_order(const Answer &ans)
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	std::vector<double> state(nr_tasks, 0);
	std::vector<double> proctime(nr_tasks, 0);
	std::vector<double> idletime(nr_tasks, 0);
	std::vector<double> distr(ans.distr);

	for (size_t i = 0; i < nr_tasks; ++i) {
		state[i] = get_work_total(i);
		proctime[i] = 0;
	}

	for (auto k : ans.order) {
		for (size_t j = 0; j < nr_combs; ++j) {
			taskset ts(j + 1);
			if (!ts[k])
				continue;
			if (distr[j] == 0)
				continue;

			for (size_t i = 0; i < nr_tasks; ++i) {
				if (!ts[i]) {
					if (state[i] > 1e-6)
						idletime[i] += distr[j];
					continue;
				}
				proctime[i] += distr[j];
				state[i] -= distr[j] * get_rate_scale(i, ts);
			}

			distr[j] = 0;
		}
	}
	std::cerr << std::endl;

	size_t w = 12;
	std::cerr << std::setw(8) << "work: ";
	for (size_t i = 0; i < nr_tasks; ++i) {
		double v = get_work_total(ans.order[i]);
		std::cerr << std::setw(w) << v << " ";
	}
	std::cerr << std::endl;

	std::cerr << std::setw(8) << "prio: ";
	for (size_t i = 0; i < nr_tasks; ++i) {
		double v = get_priority(ans.order[i]);
		std::cerr << std::setw(w) << v << " ";
	}
	std::cerr << std::endl;

	std::cerr << std::setw(8) << "speed: ";
	for (size_t i = 0; i < nr_tasks; ++i) {
		size_t k = ans.order[i];
		double v = get_work_total(k) / proctime[k];
		std::cerr << std::setw(w) << v << " ";
	}
	std::cerr << std::endl;

	std::cerr << std::setw(8) << "proc: ";
	for (size_t i = 0; i < nr_tasks; ++i) {
		double v = proctime[ans.order[i]];
		std::cerr << std::setw(w) << v << " ";
	}
	std::cerr << std::endl;

	std::cerr << std::setw(8) << "idle: ";
	for (size_t i = 0; i < nr_tasks; ++i) {
		double v = idletime[ans.order[i]];
		std::cerr << std::setw(w) << v << " ";
	}
	std::cerr << std::endl;

	std::cerr << std::setw(8) << "time: ";
	double fl = 0;
	for (size_t i = 0; i < nr_tasks; ++i) {
		size_t k = ans.order[i];
		double v = idletime[k] + proctime[k];
		std::cerr << std::setw(w) << v << " ";
		fl += v * get_priority(k);
	}
	std::cerr << std::endl;

	std::cerr << "fl = " << fl << std::endl;

	return false;
}

bool StrategyFlow::is_interrupted(const Answer &ans)
{
	size_t nr_tasks = size();
	size_t nr_combs = (1 << size()) - 1;

	std::vector<unsigned> state(nr_tasks, 0);
	std::vector<double> distr(ans.distr);
	
	for (auto k : ans.order) {
		for (size_t j = 0; j < nr_combs; ++j) {
			taskset ts(j + 1);
			if (!ts[k])
				continue;
			if (distr[j] == 0)
				continue;

			distr[j] = 0;

			// std::cerr << ts << " ";
			for (size_t i = 0; i < nr_tasks; ++i) {
				// std::cerr << state[i] << "-";
				if (ts[i]) {
					// scheduled after interrupt
					if (state[i] == 2)
						return true;

					state[i] = 1;
					continue;
				}

				// not started
				if (state[i] == 0)
					continue;

				// interrupted at previous
				state[i] = 2;
			}

			// for (size_t i = 0; i < nr_tasks; ++i) {
			// 	std::cerr << state[i] << "-";
			// }
			// std::cerr << std::endl;
		}
	}

	return false;
}
