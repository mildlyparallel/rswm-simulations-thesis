#include <cassert>
#include <fstream>
#include <chrono>
#include <algorithm>

#include "Graph.hh"

Graph::Graph(size_t nodes)
: m_edges(nodes)
{ }

Graph::~Graph()
{  }

size_t Graph::add_node()
{
	m_edges.push_back({});

	if (m_edges.size() > Nodeset::max_size)
		throw std::runtime_error("Too much nodes");

	return m_edges.size() - 1;
}

void Graph::add_edge(size_t u, size_t v)
{
	assert(u < nr_nodes());
	assert(v < nr_nodes());

	if (!have_edge(u, v))
		m_nr_edges++;

	m_edges[u].set(v);
}

void Graph::remove_edges(size_t u)
{
	assert(u < nr_nodes());
	m_edges[u].clear();

	for (size_t v = 0; v < nr_nodes(); ++v) {
		m_edges[v].clear(u);
	}
}

bool Graph::have_edge(size_t u, size_t v) const
{
	assert(u < nr_nodes());
	assert(v < nr_nodes());

	return m_edges[u][v];
}

void Graph::dfs(
	size_t u,
	std::function<bool (size_t u, size_t v)> enter_fn,
	std::function<void (size_t u)> leave_fn
) const {

	if (m_edges[u].empty()) {
		if (leave_fn)
			leave_fn(u);
		return;
	}

	for (size_t v = 0; v < nr_nodes(); ++v) {
		if (!have_edge(u, v))
			continue;

		if (enter_fn(u, v))
			dfs(v, enter_fn, leave_fn);
	}

	if (leave_fn)
		leave_fn(u);
}

void Graph::reduce()
{
	m_nr_edges = 0;

	std::vector<int> markers(nr_nodes(), 0);

	for (size_t node = 0; node < nr_nodes(); ++node) {
		
		if (m_edges[node].count() <= 1) {
			m_nr_edges += m_edges[node].count();
			continue;
		}

		std::fill(markers.begin(), markers.end(), 0);

		markers[node] = 1;

		dfs(node, 
			[&](size_t u, size_t v) {
				assert(markers[u] >= 0);

				// std::cerr << u << " ";
				// std::cerr << "(" << markers[u] << ")"  << "  ->  ";
				// std::cerr << v << " ";
				// std::cerr << "(" << markers[v] << ")"  << std::endl;

				if (markers[v] == 0) {
					// not visisted
					markers[v] = markers[u] + 1;
					return true;
				}

				if (markers[v] < 0) {
					// already left
					markers[v] = -std::max(-markers[v], markers[u] + 1);
					assert(markers[v] <= 0);
					return false;
				}

				if (markers[v] > 0) {
					// not left yet, in a loop
					return false;
				}

				return true;
			},
			[&](size_t u) {
				assert(markers[u] >= 0);
				markers[u] *= -1;
			}
		);

		for (size_t u = 0; u < nr_nodes(); ++u) {
			if (-markers[u] > 2)
				m_edges[node].clear(u);
		}

		m_nr_edges += m_edges[node].count();
	}
}

void Graph::complement()
{
	for (size_t node = 0; node < nr_nodes(); ++node) {
		if (m_edges[node].empty())
			continue;
		
		Nodeset visited;
		visited.set(node);

		dfs(node, [&](size_t u, size_t v) {
			if (visited[v])
				return false;

			visited.set(v);

			if (node != u)
				m_links.insert({node, v});

			return true;
		});
	}
}

bool Graph::reachable(size_t u, size_t v) const
{
	if (have_edge(u, v))
		return true;

	auto it = m_links.find({u, v});
	return it != m_links.end();
}

bool Graph::reachable(const Nodeset &nodes) const
{
	for (size_t u = 0; u < nr_nodes(); ++u) {
		if (!nodes[u])
			continue;

		for (size_t v = 0; v < nr_nodes(); ++v) {
			if (u == v)
				continue;
			if (!nodes[v])
				continue;

			if (reachable(u, v))
				return true;
		}
	}

	return false;
	// return reachable_any(nodes, nodes);
}

bool Graph::is_leaf(size_t u) const
{
	for (size_t v = 0; v < nr_nodes(); ++v) {
		if (have_edge(u, v))
			return false;
	}
	return true;
}

bool Graph::is_leaf(const Nodeset &nodes) const
{
	for (size_t i = 0; i < nr_nodes(); ++i) {
		if (!nodes[i])
			continue;
		if (!is_leaf(i))
			return false;
	}

	return true;
}

bool Graph::is_root(size_t v) const
{
	for (size_t u = 0; u < nr_nodes(); ++u) {
		if (have_edge(u, v))
			return false;
	}
	return true;
}

bool Graph::is_root(const Nodeset &nodes) const
{
	for (size_t i = 0; i < nr_nodes(); ++i) {
		if (!nodes[i])
			continue;
		if (!is_root(i))
			return false;
	}

	return true;
}

bool Graph::reachable_any(const Nodeset &nu, const Nodeset &nv) const
{

	for (size_t u = 0; u < nr_nodes(); ++u) {
		if (!nu[u])
			continue;

		for (size_t v = 0; v < nr_nodes(); ++v) {
			if (u == v)
				continue;
			if (!nv[v])
				continue;

			// if (have_edge(u, v))
			if (reachable(u, v))
				return true;
		}
	}

	return false;
}

bool Graph::is_tree(const Nodeset &nodes) const
{
	Nodeset entered, left;

	for (size_t node = 0; node < nr_nodes(); ++node) {
		if (!nodes[node])
			continue;
		
		if (left[node])
			continue;

		entered.set(node);

		bool found_loop = false;

		dfs(node, [&](size_t, size_t v) {
			if (found_loop)
				return false;

			if (nodes[v] == 0)
				return false;

			if (entered[v]) {
				if (!left[v])
					found_loop = true;

				return false;
			}

			entered.set(v);
			return true;
		}, [&] (size_t v) {
			assert(entered[v]);
			assert(!left[v]);
			left.set(v);
		});

		if (found_loop)
			return false;
	}

	return true;
}

const std::vector<Graph::Nodeset> &Graph::get_fvs() const
{
	return m_fvs;
}

const std::vector<Graph::Nodeset> &Graph::list_maximal_fvs()
{
	if (!m_fvs.empty())
		return m_fvs;

	size_t nr_tested = 0;

	std::cerr << std::endl;

	auto start_ts = std::chrono::high_resolution_clock::now();

	for (size_t i = 0; i < nr_nodes(); ++i) {
		Nodeset nodes;
		for (size_t j = 0; j < nr_nodes(); ++j)
			nodes.set(j, j >= i);

		bool all_trees = true;

		do {
			// size_t nnn = 0;
			// for (size_t i = 0; i < nr_nodes(); ++i) {
			// 	std::cout << nodes[i] << " ";
			// 	nnn += nodes[i];
			// }
			// std::cout << "| " << nnn;

			if (nodes.subset_of_any(m_fvs)) {
				// std::cout << " | subset " << std::endl;
				continue;
			}

			if (have_loop(nodes)) {
			// if (!is_tree(nodes)) {
				// std::cout << " | cycle" << std::endl;
				all_trees = false;
			} else {
				m_fvs.push_back(nodes);
				// std::cout << " | tree" << std::endl;
			}

			nr_tested++;

			if ((nr_tested & ((1 << 25) - 1)) == 0) {
				std::cerr << "Found/Tested: " << m_fvs.size() << "/" << nr_tested << "; ";

				auto end_ts = std::chrono::high_resolution_clock::now();
				double dt = std::chrono::duration_cast<std::chrono::milliseconds>(end_ts - start_ts).count() / 1e3;

				std::cerr << "Passed " << dt << " sec; ";
				std::cerr << "Avg: " << (nr_tested/1e6/dt) << " test/sec\n";
			}

		} while (nodes.next_permutation(0, nr_nodes()));

		if (all_trees)
			break;
	}

#ifndef NDEBUG
	for (auto nodes : m_fvs)
		assert(is_tree(nodes));
#endif

	return m_fvs;
}

std::string Graph::get_node_label(size_t) const
{
	return {};
}

void Graph::print_dot_nodes(std::ostream &out, const Nodeset &filter) const
{
	for (size_t u = 0; u < nr_nodes(); ++u) {
		if (!filter.empty() && !filter[u])
			continue;

		out << u << " [";

		// bool comma = false;
		std::string label = get_node_label(u);
		if (!label.empty()) {
			out << "label=\"" << label << "\"";
			// comma = true;
		}

		// if (marked[u]) {
		// 	if (comma)
		// 		out << ", ";
		// 	out << "fillcolor=gray69, style=filled";
		// 	comma = true;
		// }

		out << "];\n";
	}
}

void Graph::print_dot_directed(const std::string &file, const Nodeset &filter) const
{
	std::ofstream out(file);

	out << "digraph {\n";

	print_dot_nodes(out, filter);

	if (!filter.empty()) {
		auto edges = reduce_on_filtered(filter);
		for (const auto &[u, v] : edges) {
			out << u << " -> " << v << ";\n";
		}
	} else {
		for (size_t u = 0; u < nr_nodes(); ++u) {
			for (size_t v = 0; v < nr_nodes(); ++v) {
				if (!have_edge(u, v))
					continue;

				out << u << " -> " << v << ";\n";
			}
		}

		if (m_links.size() < 20) {
			for (const auto &[u, v] : m_links)
				out << u << " -> " << v << "[color=grey80];\n";
		}
	}

	out << "}\n";
}

void Graph::write(const std::string &file) const
{
	std::ofstream out(file);
	out << nr_nodes() << "\n";

	for (size_t u = 0; u < nr_nodes(); ++u) {
		for (size_t v = 0; v < nr_nodes(); ++v) {
			out << have_edge(u, v) << " ";
		}
		out << "\n";
	}

	out << m_links.size() << "\n";
	for (auto [u, v] : m_links)
		out << u << " " << v << "\n";

	out << m_fvs.size() << "\n";
	for (const auto &nodes : m_fvs) {
		out << nodes.count() << " ";
		for (size_t u = 0; u < nr_nodes(); ++u) {
			if (nodes[u])
				out << u << " ";
		}
		out << "\n";
	}
}

void Graph::read(const std::string &file)
{
	std::ifstream in(file);
	size_t n = 0;
	in >> n;

	m_edges.resize(n);

	for (size_t u = 0; u < nr_nodes(); ++u) {
		for (size_t v = 0; v < nr_nodes(); ++v) {
			bool edge = false;
			in >> edge;
			if (edge)
				add_edge(u, v);
		}
	}

	m_links.clear();
	size_t nr_links = 0;
	in >> nr_links;
	for (size_t i = 0; i < nr_links; ++i) {
		size_t u = 0, v = 0;
		in >> u >> v;
		m_links.insert({u, v});
	}

	m_fvs.clear();
	size_t nr_fvs = 0;
	in >> nr_fvs;
	for (size_t i = 0; i < nr_fvs; ++i) {
		size_t n = 0;
		in >> n;
		Nodeset nodes;
		for (size_t j = 0; j < n; ++j) {
			size_t u = 0;
			in >> u;
			nodes.set(u);
		}

		m_fvs.push_back(nodes);
	}
}

void Graph::generage_dag(
	std::function<size_t()> breadth_fn,
	std::function<bool(size_t, size_t)> edge_fn
) {
	size_t n = 0;

	while (n < nr_nodes()) {
		size_t breadth = breadth_fn();

		for (size_t p = n; p != 0; p--) {
			size_t u = p - 1;
			for (size_t b = 0; b < breadth; ++b) {
				size_t v = n + b;
				if (v >= nr_nodes())
					break;
				if (reachable(u, v))
					continue;
				if (!edge_fn(u, v))
					continue;
				add_edge(u, v);
			}
		}

		n += breadth;
	}
}

void Graph::add_loop(const Nodeset &nodes)
{
	bool added = false;
	for (auto &loop : m_loops) {
		if (loop.subset_of(nodes))
			return;
		if (nodes.subset_of(loop)) {
			loop = nodes;
			added = true;
		}
	}

	if (!added)
		m_loops.push_back(nodes);
}

const std::vector<Graph::Nodeset> &Graph::list_loops()
{
	m_loops.clear();

	for (size_t node = 0; node < nr_nodes(); ++node) {

		Nodeset currentpath;
		Nodeset entered({node});

		dfs(node, 
			[&](size_t, size_t v) {
				if (v == node) {
					add_loop(entered);
					return false;
				}

				if (entered[v])
					return false;

				entered.set(v);
				return true;
			},
			[&](size_t u) {
				assert(entered[u]);
				entered.clear(u);
			}
		);
	}

	std::set<Nodeset> ns(m_loops.begin(), m_loops.end());
	m_loops.assign(ns.begin(), ns.end());

	std::sort(m_loops.begin(), m_loops.end(), [](const Nodeset &a, const Nodeset &b) {
		return a.count() < b.count();
	});

	return m_loops;
}

bool Graph::have_loop(const Nodeset &nodes) const
{
	size_t n = nodes.count();
	for (const auto &loop : m_loops) {
		if (loop.count() > n)
			break;

		if (loop.subset_of(nodes))
			return true;
	}

	return false;
}

std::set<std::pair<size_t, size_t>> Graph::reduce_on_filtered(const Nodeset &filter) const
{
	assert(is_tree(filter));

	std::set<std::pair<size_t, size_t>> edges;

	std::vector<int> markers(nr_nodes(), 0);

	for (size_t node = 0; node < nr_nodes(); ++node) {
		if (!filter[node])
			continue;
		
		std::fill(markers.begin(), markers.end(), 0);

		markers[node] = 1;

		dfs(node, 
			[&](size_t u, size_t v) {
				if (!filter[v])
					return false;

				bool visited = markers[v] > 0;

				markers[v] = std::max(markers[v], markers[u] + 1);
				return !visited;
			}
		);

		for (size_t u = 0; u < nr_nodes(); ++u) {
			if (markers[u] == 2)
				edges.insert({node, u});
		}
	}

	return edges;
}

bool Graph::have_loop() const
{
	Nodeset black;

	for (size_t node = 0; node < nr_nodes(); ++node) {
		if (black[node])
			continue;

		Nodeset gray({node});
		bool found = false;

		dfs(node, 
			[&](size_t, size_t v) {
				if (found)
					return false;

				if (black[v])
					return false;

				if (gray[v]) {
					found = true;
					return false;
				}

				gray.set(v);
				return true;
			},
			[&](size_t u) {
				assert(gray[u]);
				gray.clear(u);
				black.set(u);
			}
		);

		if (found)
			return true;
	}

	return false;

}

