#include <cassert>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "SystemStationary.hh"

SystemStationary::SystemStationary()
: System()
{ }

SystemStationary::SystemStationary(size_t n)
: System(n)
{ }

SystemStationary::~SystemStationary()
{  }

void SystemStationary::init_base_rate(double r, double dr)
{
	for (auto &t : m_tasks) {

		auto rate_fn = [r](double) {
			return r;
		};

		t.set_base_rate_fn(rate_fn);

		r += dr;
	}
}

void SystemStationary::init_base_rate(std::function<double (const Task &)> rate_gen_fn)
{
	for (auto &t : m_tasks) {

		double rate = rate_gen_fn(t);

		auto rate_fn = [rate](double) {
			return rate;
		};

		t.set_base_rate_fn(rate_fn);
	}
}

void SystemStationary::init_rate_scale(std::function<double (Taskset)> scale_gen_fn)
{
	Taskset ts;
	while (ts.next_combination(0, m_tasks.size())) {

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double scale = scale_gen_fn(ts);

			assert(scale > 0 && scale <= 1);

			auto scale_fn = [scale](double) {
				return scale;
			};

			m_tasks[i].set_rate_scale_fn(ts, scale_fn);
		}
	}
}

void SystemStationary::init_rate_scale(std::function<double (Taskset, const Task &)> scale_gen_fn)
{
	Taskset ts;
	while (ts.next_combination(0, m_tasks.size())) {

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double scale = scale_gen_fn(ts, m_tasks[i]);

			set_rate_scale(i, ts, scale);
		}
	}
}

double SystemStationary::get_rate_scale(size_t task_id, Taskset ts) const
{
	assert(task_id < m_tasks.size());

	return m_tasks[task_id].get_rate_scale(ts, 0);
}

double SystemStationary::get_work_total(size_t task_id) const
{
	assert(task_id < m_tasks.size());

	return m_tasks[task_id].get_work_total();
}

void SystemStationary::init_scale_rnd(std::function<double (size_t)> scale_fn)
{
	for (size_t i = 0; i < m_tasks.size(); ++i)
		set_rate_scale(i, Taskset({i}), 1);

	for (const auto &ts : m_feasible_ts) {
		if (ts.count() == 1)
			continue;

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double min_speed = find_min_scale(i, ts);
			// std::cerr << n << " " << ts << " " << min_speed << std::endl;
			double s = std::clamp(scale_fn(i), 0., 1.) * min_speed;
			set_rate_scale(i, ts, s);
		}
	}
}

double SystemStationary::find_min_scale(size_t task_id, Taskset ts) const
{
	assert(ts[task_id]);

	double min_s = 1;
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (i == task_id)
			continue;

		if (!ts[i])
			continue;

		ts.clear(i);

		double s = m_tasks[task_id].get_rate_scale(ts, 0);
		// std::cout << "--" << ts << " " << s << std::endl;
		if (s < min_s)
			min_s = s;

		ts.set(i);
	}

	return min_s;
}

void SystemStationary::set_rate_scale(size_t task_id, Taskset ts, double scale)
{
	auto scale_fn = [scale](double) {
		return scale;
	};

	m_tasks[task_id].set_rate_scale_fn(ts, scale_fn);
}

bool SystemStationary::validate(Taskset tss) const
{
	bool ok = true;
	for (auto &ts : m_feasible_ts) {
		if (!ts.subset_of(tss))
			continue;

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			double v_par = m_tasks[i].get_rate_scale(tss, 0);
			double v_sub = m_tasks[i].get_rate_scale(ts, 0);

			if (v_par > 1) {
				std::cerr << "invalid " << tss << " T_" << i << ": " << v_par << "\n";
				ok = false;
			}

			if (v_par <= v_sub)
				continue;

			ok = false;
			std::cerr << "invalid " << tss << " " << ts << " T_" << i << ": ";
			std::cerr << v_par << " >= " << v_sub << std::endl;
		}
	}

	return ok;
}

bool SystemStationary::validate() const
{
	for (auto &ts : m_feasible_ts) {
		if (!validate(ts))
			return false;
	}

	return true;
}

void SystemStationary::print_row(const std::string &label, std::function<double (const Task &task)> fn) const
{
	std::cout << std::setw(5) << label << " = [";

	bool comma = false;

	for (auto &t : m_tasks) {
		if (comma)
			std::cout << ", ";
		comma = true;

		std::cout << std::setw(4) << fn(t);
	}

	std::cout << "]" << std::endl;
}

void SystemStationary::print_comb(
	const std::string &label,
	std::function<double (const Task &task, Taskset)> fn
) const {
	double w = 5;

	std::cout << std::setw(5) << label << " | ";

	for (auto &ts : m_feasible_ts)
		std::cout << std::setw(w) << ts.to_ullong() << " ";

	std::cout << "\n";

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		std::cout << std::setw(5) << i << " | ";

		for (auto &ts : m_feasible_ts)
			std::cout << std::setw(w) << std::setprecision(3) << fn(m_tasks[i], ts) << " ";

		std::cout << "\n";
	}

	std::cout << std::setw(5) << "sum" << " | ";

	for (auto &ts : m_feasible_ts) {
		double sum = 0;
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (ts[i])
				sum += fn(m_tasks[i], ts);
		}

		std::cout << std::setw(w) << sum << " ";
	}

	// std::cout << "\n";
    //
	// std::cout << std::setw(5) << "eff" << " | ";
    //
	// for (auto &ts : m_feasible_ts) {
	// 	double sum = 0;
	// 	for (size_t i = 0; i < m_tasks.size(); ++i) {
	// 		if (ts[i])
	// 			sum += fn(m_tasks[i], ts);
	// 	}
	// 	double e = sum * sum / ts.count();
    //
	// 	std::cout << std::setw(w) << e << " ";
	// }

	std::cout << std::endl;
}

void SystemStationary::print_val(const std::string &label, std::function<double ()> fn) const
{
	std::cout << std::setw(5) << label << " = " << fn() << std::endl;
}

void SystemStationary::print() const
{
	print_val("size", [&](){ return m_tasks.size(); });

	print_row("work", [](const Task &task) {
			return task.get_work_total();
	});

	print_row("rel", [](const Task &task) {
			return task.get_release_time();
	});

	print_row("base", [](const Task &task) {
			return task.get_base_rate(0);
	});

	// if (m_feasible_ts.size() < 33) {
	if (true) {
		std::cerr << std::endl;

		print_comb("scale", [](const Task &task, Taskset ts) {
			return task.get_rate_scale(ts, 0);
		});

		// std::cerr << std::endl;
        //
		// print_comb("rate", [](const Task &task, Taskset ts) {
		// 	return task.get_rate(ts, 0);
		// });
	}
}

void SystemStationary::write(const std::string &outdir) const
{
	assert(!outdir.empty());

	std::stringstream outfile;
	outfile << outdir << "/system-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "work,base";
	for (auto &ts : m_feasible_ts)
		out << ",S" << ts.to_ullong();

	out << "\n";

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		out << m_tasks[i].get_work_total() << ",";
		out << m_tasks[i].get_base_rate(0);

		for (auto &ts : m_feasible_ts)
			out << "," << m_tasks[i].get_rate_scale(ts, 0);
		out << "\n";
	}
}

void SystemStationary::write_by_task(const std::string &outdir) const
{
	assert(!outdir.empty());

	std::stringstream outfile;
	outfile << outdir << "/speedup-task-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "comb,task,size,speedup\n";

	for (auto &ts : m_feasible_ts) {
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			out << ts.to_ullong() << ",";
			out << i << ",";
			out << ts.count() << ",";
			out << m_tasks[i].get_rate_scale(ts, 0) << "\n";
		}
	}
}

void SystemStationary::write_by_comb(const std::string &outdir) const
{
	assert(!outdir.empty());

	std::stringstream outfile;
	outfile << outdir << "/speedup-comb-" << get_id() << ".csv";

	std::ofstream out(outfile.str());

	out << "comb,size,speedup\n";

	for (auto &ts : m_feasible_ts) {

		double s = 0;
		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;
			s += m_tasks[i].get_rate_scale(ts, 0);
		}

		out << ts.to_ullong() << ",";
		out << ts.count() << ",";
		out << s << "\n";
	}
}
