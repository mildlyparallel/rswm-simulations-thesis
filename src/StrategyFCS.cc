#include <cassert>
#include <limits>
#include <iostream>
#include <cmath>

#include "StrategyFCS.hh"

StrategyFCS::StrategyFCS()
{  }

StrategyFCS::~StrategyFCS()
{  }

void StrategyFCS::set_efficiency(double alpha)
{
	m_efficiency = alpha;
}

const char *StrategyFCS::name() const
{
	if (m_fun == CombSpeed)
		return "fcs";
	if (m_fun == SpeedEfficiencySq)
		return "eff";
	if (m_fun == SpeedEfficiencyExp)
		return "eff-exp";
	return "unknown";
}

double StrategyFCS::measure_speed(Taskset ts) const
{
	return get_rate_scale(ts);
}

double StrategyFCS::measure_eff_sq(Taskset ts) const
{
	double a = get_rate_scale(ts);
	double n = ts.count();
	return a * std::pow(a / n, m_efficiency);
}

double StrategyFCS::measure_eff_exp(Taskset ts) const
{
	double a = get_rate_scale(ts);
	double n = ts.count();
	return std::exp(a) / n;
}

double StrategyFCS::measure(Taskset ts) const
{
	double scale = 0;

	if (m_fun == CombSpeed)
		scale = measure_speed(ts);
	if (m_fun == SpeedEfficiencySq)
		scale = measure_eff_sq(ts);
	if (m_fun == SpeedEfficiencyExp)
		scale = measure_eff_exp(ts);

	return scale;
}

bool StrategyFCS::validate(const std::vector<Taskset> &sched)
{
	std::cerr << std::endl;
	std::cerr << "FCS " << name() << std::endl;

	for (size_t i = 0; i < sched.size(); ++i) {
		if (all_completed()) {
			std::cerr << "All tasks completed before end" << std::endl;
			return false;
		}

		if (!all_schedulable(sched[i])) {
			std::cerr << "Some tasks are not schedulable in " << sched[i] << std::endl;
			return false;
		}

		Taskset max_ts;
		double max_scale = 0;

		for (auto ts : m_system->feasible_combinations()) {
			if (!all_schedulable(ts))
				continue;

			double scale = measure(ts);
			if (scale >= max_scale) {
				max_scale = scale;
				max_ts = ts;
			}
		}

		double cur_scale = measure(sched[i]);
		if (max_scale - 1e-6 > cur_scale) {
			std::cerr << "cur_scale = " << cur_scale << std::endl;
			std::cerr << "Found " << max_ts << " " << max_scale << std::endl;
			return false;
		}

		double dt = min_remaining_time(sched[i]);
		std::cerr << sched[i] << " " << dt << std::endl;
		run(sched[i], dt);
	}

	if (!all_completed()) {
		std::cerr << "Some tasks did not completed" << std::endl;
		return false;
	}

	return true;
}

void StrategyFCS::start()
{
#ifndef NDEBUG
	std::cerr << std::endl;
	std::cerr << "FCS " << name() << std::endl;
#endif

	while (!all_completed()) {
		Taskset max_ts;
		double max_scale = 0;

		for (auto ts : m_system->feasible_combinations()) {

			if (!all_schedulable(ts))
				continue;

			double scale = 0;

			if (m_fun == CombSpeed)
				scale = measure_speed(ts);
			if (m_fun == SpeedEfficiencySq)
				scale = measure_eff_sq(ts);
			if (m_fun == SpeedEfficiencyExp)
				scale = measure_eff_exp(ts);

#ifndef NDEBUG
			// std::cerr << "  " << ts << " " << scale << std::endl;
#endif

			if (scale >= max_scale) {
				max_scale = scale;
				max_ts = ts;
			}
		}

		assert(!max_ts.empty());

		double dt = min_remaining_time(max_ts);

#ifndef NDEBUG
		std::cerr << max_ts << " " << dt << std::endl;
#endif

		run(max_ts, dt);
	}
}

void StrategyFCS::set_acq_fun(AccFun fn)
{
	m_fun = fn;
}
