#include <iostream>
#include <random>
#include <thread>
#include <atomic>
#include <queue>
#include <mutex>
#include <sstream>
#include <chrono>
#include <iomanip>

#include "Config.hh"
#include "TaskGraph.hh"
#include "Runner.hh"
#include "SystemStationary.hh"

#include "StrategySequential.hh"
#include "StrategyNaive.hh"
#include "StrategyOptimal.hh"
#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyFair.hh"
#include "StrategyCombined.hh"

void fill_system_rnd(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	sys.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	std::uniform_real_distribution<double> res_distr(config.min_resources, config.max_resources);

	std::vector<double> res1(sys.size());
	std::vector<double> res2(sys.size());
	for (size_t i = 0; i < sys.size(); ++i) {
		res1[i] = res_distr(gen);
		res2[i] = res_distr(gen);
	}

	std::sort(res1.begin(), res1.end());
	std::sort(res2.begin(), res2.end());

	std::uniform_real_distribution<double> slope_distr(config.min_slope_scale, config.max_slope_scale);

	std::vector<double> slope1(sys.size());
	std::vector<double> slope2(sys.size());
	for (size_t i = 0; i < sys.size(); ++i) {
		slope1[i] = slope_distr(gen);
		slope2[i] = slope_distr(gen);
	}

	for (auto &ts : sys.feasible_combinations()) {
		double left1 = 1;
		double left2 = 1;
		size_t left_tasks = ts.count();

		for (size_t i = 0; i < sys.size(); ++i) {
			if (!ts[i])
				continue;

			double r1 = res1[i];
			double r2 = res2[i];
			double s1 = std::min(r1, left1 / left_tasks);
			double s2 = std::min(r2, left2 / left_tasks);

			double a = 1. - slope1[i] * (r1 - s1) - slope2[i] * (r2 - s2);

			sys[i].set_rate_scale(ts, std::clamp(a, 1e-2, 1.));

			left1 -= s1;
			left2 -= s2;
			left_tasks--;
		}
	}

	// sys.print();

	// std::cerr << "Validating ";
	// bool tasks_ok = sys.validate();
	// if (tasks_ok) {
	// 	std::cerr << " Ok" << std::endl;
	// } else {
	// 	std::cerr << " Not valid" << std::endl;
	// 	std::abort();
	// }
    //
	// exit(0);
}

SystemStationary create_instance(
	size_t id,
	const TaskGraph &tasks,
	const Config &config
) {
	SystemStationary sys(tasks.nr_nodes());

	sys.set_id(id);
	sys.set_precedence(tasks);
	sys.set_seed(config.seed + id);

	std::default_random_engine rndgen;
	rndgen.seed(sys.get_seed());

	fill_system_rnd(config, sys, rndgen);

	// sys.write(config.output);
	// sys.write_by_task(config.output);
	// sys.write_by_comb(config.output);

	return sys;
}

void worker_fn(
	size_t wid,
	const Config &config,
	const TaskGraph &tasks,
	std::atomic_size_t &nr_runs_claimed
) {
	Runner runner;

	StrategySequential stg_seq;
	runner.add(&stg_seq);

	StrategyNaive stg_naive;
	runner.add(&stg_naive);

	StrategyOptimal stg_optimal;
	runner.add(&stg_optimal);

	StrategyCombined stg_fcs;
	stg_fcs.set_fairness(0);
	stg_fcs.set_efficiency(0);
	stg_fcs.set_name("fcs");
	runner.add(&stg_fcs);

	StrategyCombined stg_pe;
	stg_pe.set_fairness(0);
	stg_pe.set_efficiency(1);
	stg_pe.set_name("pe");
	runner.add(&stg_pe);

	StrategyCombined stg_pf;
	stg_pf.set_fairness(1);
	stg_pf.set_efficiency(0);
	stg_pf.set_name("pf");
	runner.add(&stg_pf);

	StrategyCombined stg_comb;
	stg_comb.set_fairness(1);
	stg_comb.set_efficiency(1);
	stg_comb.set_name("comb");
	runner.add(&stg_comb);

	std::stringstream out;
	out << config.output << "/results-" << wid << ".csv";

	runner.open(out.str());

	if (wid == 0) {
		runner.print_header();
		std::cerr << std::endl;
	}

	auto st = std::chrono::high_resolution_clock::now();

	while (true) {
		size_t run_id = nr_runs_claimed.fetch_add(1);
		if (run_id >= config.nr_runs)
			break;

		SystemStationary sys = create_instance(run_id, tasks, config);

		if (wid == 0) {
			auto en = std::chrono::high_resolution_clock::now();
			double dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count();
			dt /= 1e3;
			double rate = 1. * run_id / dt;
			size_t left = config.nr_runs - run_id;
			double eta = 1. * left / rate;
			double eta_m = std::floor(eta / 60);
			double eta_h = std::floor(eta_m / 60);

			std::cerr << "left: " << std::setw(5) << left << "; ";
			std::cerr << "time: " << std::setw(5) << dt << " sec; ";
			std::cerr << "rate: " << std::setw(8) << std::setprecision(3) << rate << " runs/sec; ";
			std::cerr << "eta: " << std::setw(5);

			if (eta_h > 1)
				std::cerr << eta_h << " hr ";
			else if (eta_m > 1)
				std::cerr << eta_m << " min ";
			else
				std::cerr << eta << " sec ";
			std::cerr << "\r";
		}

		runner.run(sys, config.fairness);
	}

	if (wid == 0)
		std::cerr << std::endl;
}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	TaskGraph tasks;
	if (config.dag_dir.empty())
		tasks.add_tasks_no_prec(config.nr_tasks);

	std::cout << "Total simulations:" << config.nr_runs << std::endl;

	std::atomic_size_t nr_runs_claimed(0);

	std::vector<std::thread> threads;

	for (size_t i = 0; i < config.nr_threads; ++i) {
		threads.emplace_back(
			worker_fn,
			i,
			std::ref(config),
			std::ref(tasks),
			std::ref(nr_runs_claimed)
		);
	}

	for (auto &th : threads) {
		th.join();
	}

	std::cout << "\nDone.\n";

	return 0;
}

