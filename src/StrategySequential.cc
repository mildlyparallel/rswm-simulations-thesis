#include "StrategySequential.hh"

StrategySequential::StrategySequential()
{  }

StrategySequential::~StrategySequential()
{  }

void StrategySequential::start()
{
	while (!all_completed()) {
		size_t i = get_next_schedulable();

		if (i >= size())
			break;

		Taskset ts({i});
		run(ts);
	}
}

const char *StrategySequential::name() const
{
	return "sequential";
}
