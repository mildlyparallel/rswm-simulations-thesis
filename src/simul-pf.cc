#include <iostream>
#include <random>
#include <thread>
#include <atomic>
#include <queue>
#include <mutex>
#include <sstream>
#include <chrono>
#include <iomanip>

#include "Config.hh"
#include "TaskGraph.hh"
#include "Runner.hh"
#include "SystemStationary.hh"

#include "StrategySequential.hh"
#include "StrategyNaive.hh"
#include "StrategyOptimal.hh"
#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyFair.hh"

void fill_system_rnd_w(
	const Config &,
	SystemStationary &sys,
	std::default_random_engine &
) {
	sys.init_base_rate(1);
	sys.init_total_work(200);

	size_t nr_tasks = sys.size();

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				sys[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				sys[i].set_rate_scale(ts, 1);
				break;
			}

			if (ts.count() > 2)
				continue;

			if (i == 0) {
				sys[i].set_rate_scale(ts, 1);
			} else {
				sys[i].set_rate_scale(ts, 1.);
			}
		}
	}

	sys[0].set_rate_scale({0, 1}, 0.9);
	sys[1].set_rate_scale({0, 1}, 0.9);

	sys[0].set_rate_scale({0, 2}, 0.7);
	sys[2].set_rate_scale({0, 2}, 0.7);

	sys[1].set_rate_scale({1, 2}, 0.3);
	sys[2].set_rate_scale({1, 2}, 0.3);

	sys.print();

	std::cerr << "Validating ";
	bool tasks_ok = sys.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

}

void fill_system_rnd(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	sys.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	std::uniform_real_distribution<double> slope_coeff(
		config.min_slope_scale,
		config.max_slope_scale
	);

	double slope_distr_min[Taskset::max_size];

	for (size_t i = 0; i < sys.size(); ++i)
		slope_distr_min[i] = slope_coeff(gen);

	sys.init_scale_rnd([&] (size_t task_id) -> double {
		std::uniform_real_distribution<double> slope_coeff(slope_distr_min[task_id], 1);
		return slope_coeff(gen);
	});

	Taskset ts;
	// double a_star = 3;
	while (ts.next_combination(0, sys.size())) {
	// 	double scale = 0;
	// 	for (size_t i = 0; i < sys.size(); ++i) {
	// 		if (ts[i])
	// 			scale += sys[i].get_rate_scale(ts, 0);
	// 	}
    //
	// 	if (!ts[0] && scale < a_star)
	// 		continue;
    //
	// 	double v = std::min(a_star, 1. * ts.count()) / ts.count();
    //
	// 	for (size_t i = 0; i < sys.size(); ++i) {
	// 		if (!ts[i])
	// 			continue;
    //
	// 		if (ts.count() > a_star) {
	// 			sys[i].set_rate_scale(ts, 0.001);
	// 		} else {
	// 			sys[i].set_rate_scale(ts, v);
	// 		}
	// 	}
	}

	// sys.print();
}

SystemStationary create_instance(
	size_t id,
	const TaskGraph &tasks,
	const Config &config
) {
	SystemStationary sys(tasks.nr_nodes());

	sys.set_id(id);
	sys.set_precedence(tasks);
	sys.set_seed(config.seed + id);

	std::default_random_engine rndgen;
	rndgen.seed(sys.get_seed());

	// fill_system_rnd(config, sys, rndgen);
	fill_system_rnd_w(config, sys, rndgen);

	// sys.print();
	// sys.write(config.output);
	// sys.write_by_task(config.output);
	// sys.write_by_comb(config.output);

	return sys;
}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	TaskGraph tasks;
	if (config.dag_dir.empty())
		tasks.add_tasks_no_prec(config.nr_tasks);
	// else
	// 	tasks.read_all(config.dag_dir);

	std::cout << "Total simulations:" << config.nr_runs << std::endl;


	std::ofstream out("avg_speed.csv");
	out << "seed,af\n";

	// double min_speed = 100;

	for (size_t i = 0; i < config.nr_runs; ++i) {
		StrategyRR stg;
		// stg.set_acq_fun(StrategyRR::LinearSpeed);
		stg.set_acq_fun(StrategyRR::ScaledSpeed3);
		stg.set_interval(config.time_interval);
		stg.stop_after_first(true);

		SystemStationary sys = create_instance(i, tasks, config);

		sys.print();

		stg.reset();

		stg.set_input(&sys);

		stg.start();

		// if (stg.get_avg_speed() < min_speed) {
		// 	min_speed = stg.get_avg_speed();
		// 	out << sys.get_seed() << ",";
		// 	out << stg.get_avg_speed() << "\n";
		// 	sys.print();
		// }

		double a_sum = 0;
		double a_time_sum = 0;
		for (auto ts : sys.feasible_combinations()) {
			double d = stg.get_subset_time(ts);
			if (d < config.time_interval)
				continue;

			// double scale = stg.get_linear_speed(ts);
			double scale = stg.get_scaled_speed_3(ts);

			double a = stg.get_rate_scale(ts);
			a_sum += a * ts.count();
			a_time_sum += d * a;

			std::cout << scale << " " << d << " " << a << " " << ts << std::endl;
		}

		std::cerr << "a_sum = " << a_sum << std::endl;
		std::cerr << "a_time_sum = " << a_time_sum << std::endl;
		std::cerr << "a_sum/a_time_sum = " << a_sum/a_time_sum << std::endl;
		std::cerr << "a_time_sum/a_sum = " << a_time_sum/a_sum << std::endl;

		// if (stg.get_avg_speed() == 3)
		// 	continue;

		// std::cout << sys.get_seed() << " " << stg.get_avg_speed() << std::endl;
	}

	std::cout << "\nDone.\n";

	return 0;
}

