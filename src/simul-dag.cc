#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <cassert>
#include <random>

#include "Config.hh"
#include "SystemStationary.hh"
#include "TaskGraph.hh"
#include "StrategyCombined.hh"

#include <Problem.hh>
#include <armadillo>
#include <lpsolve/lp_lib.h>

TaskGraph create_nodes(size_t n) {
	TaskGraph g;

	for (size_t i = 0; i < n; ++i) {
		Task task;
		task.set_id(i);
		g.add_task(task);
	}

	return g;
}

TaskGraph get_graph_1() {
	TaskGraph g = create_nodes(4);

	g.add_edge(0, 1);
	g.add_edge(2, 3);

	return g;
}

TaskGraph get_graph_2() {
	TaskGraph g = create_nodes(6);

	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(3, 4);
	g.add_edge(4, 5);

	return g;
}

TaskGraph get_graph_3() {
	TaskGraph g = create_nodes(6);

	g.add_edge(0, 1);
	g.add_edge(2, 3);
	g.add_edge(4, 5);

	return g;
}

TaskGraph get_graph_4() {
	size_t k = 6;
	TaskGraph g = create_nodes(k * k);

	for (size_t i = 0; i < k; ++i) {
		for (size_t j = 0; j < k; ++j) {
			size_t u = j * k + i;

			for (size_t i1 = i+1; i1 < k; ++i1) {
				for (size_t j1 = 0; j1 < k; ++j1) {
					if (j1 < j && j - j1 >= i1 - i )
						continue;

					size_t v = j1 * k + i1;
					g.add_edge(u, v);
				}
			}
		}
	}

	return g;
}


int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	std::default_random_engine rndgen;
	rndgen.seed(config.seed);

	TaskGraph tasks;
	if (!config.input.empty())
		tasks.read_dot(config.input);
	else
		tasks = get_graph_4();

	// tasks.print_dot_directed("graph.dot");

	tasks.reduce();
	tasks.complement();
	tasks.list_feasible();
	// tasks.find_any_lockfree(rndgen);

	std::ofstream out(config.output + "/dag.env");
	out << "nodes=" << tasks.nr_nodes() << "\n";
	out << "edges=" << tasks.nr_edges() << "\n";
	out << "combinations=" << tasks.get_feasible().size() << "\n";

	return 0;
}

