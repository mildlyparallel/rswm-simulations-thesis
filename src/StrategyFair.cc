#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <cmath>

#include <Problem.hh>

#include "StrategyFair.hh"

StrategyFair::StrategyFair()
{  }

StrategyFair::~StrategyFair()
{  }

const char *StrategyFair::name() const
{
	return "fair";
}

std::vector<std::pair<Taskset, double>> StrategyFair::solve() const
{
	size_t nr_tasks = size();
	const auto &tss = get_schedulable_combinations();
	size_t nr_combs = tss.size();

	if (nr_combs == 1) {
		std::vector<std::pair<Taskset, double>> ans;
		ans.push_back({tss[0], 1});
		return ans;
	}

	using namespace lpcpp;

	Problem lp(nr_combs);

	lp.set_verbose(Problem::Important);

	for (size_t i = 0; i < nr_tasks; ++i) {
		if (m_tasks[i].completed() || !m_tasks[i].runnable())
			continue;

		for (size_t j = 0; j < nr_combs; ++j)
			lp[j] = get_rate_scale(i, tss[j]);

		// lp.add_constraint(Problem::Equal, 1);
		lp.add_constraint(Problem::Equal, get_work_total(i));
	}

	lp.set_bound_lower_all(0);

	for (size_t j = 0; j < nr_combs; ++j)
		lp[j] = 1;

	lp.set_objective_minim();

	// lp.print_lp();

	double opt = lp.optimize();

	// std::cerr << "opt = " << opt << std::endl;

	std::vector<std::pair<Taskset, double>> ans;
	double s = 0;

	for (size_t j = 0; j < nr_combs; j++) {
		if (lp[j] <= 1e-7)
			continue;

		s += lp[j];
		ans.push_back({tss[j], lp[j]});
	}

	normalize(ans);

	// for (auto &a : ans) {
	// 	std::cerr << a.first << "  " << a.second << "\n";
	// }

	return ans;
}

double StrategyFair::normalize(std::vector<std::pair<Taskset, double>> &ans)
{
	double s = 0;
	for (auto &a : ans) {
		s += a.second;
	}

	for (auto &a : ans) {
		a.second /= s;
	}

	return s;
}

double StrategyFair::normalize(std::vector<double> &ans)
{
	double s = 0;
	for (auto &a : ans) {
		s += a;
	}

	for (auto &a : ans) {
		a /= s;
	}

	return s;
}

void StrategyFair::start()
{
#ifndef NDEBUG
	std::cerr << std::endl;
	std::cerr << "FAIR " << name() << " dt=" << m_time_interval << std::endl;
#endif

	while (!all_completed()) {
		auto dest = solve();
		std::vector<double> curr(dest.size(), 1.);

		while (true) {
			Taskset max_ts;
			double max_d = 0;
			size_t max_i = 0;

			double curr_sum = 0;
			for (auto &c : curr) {
				curr_sum += c;
			}

			// for (auto &c : curr) {
			// 	std::cerr << c / curr_sum << ",";
			// }
			// std::cerr << std::endl;

			for (size_t i = 0; i < dest.size(); ++i) {
				double d = dest[i].second - curr[i] / curr_sum;

				if (i == 0 || d > max_d) {
					max_ts = dest[i].first;
					max_d = d;
					max_i = i;
				}
			}

			double dt = std::min(min_remaining_time(max_ts), m_time_interval);

			// #ifndef NDEBUG
			// 			std::cerr << ">  " << max_ts << " " << dt << std::endl;
			// #endif

			run(max_ts, dt);

#ifndef NDEBUG
			std::cerr << std::setw(4) << now() << " ";
			std::stringstream ss;
			ss << max_ts;
			std::cerr << std::setw(10) << ss.str() << " ";
			std::cerr << std::setw(5) << get_rate_scale(max_ts) << " ";

			std::cerr << " | ";

			double sum_r = 0;
			for (size_t i = 0; i < m_tasks.size(); ++i)
				sum_r = 1. + m_tasks[i].get_work_done() / m_time_interval;

			// std::cerr << now();

			double max_r = 0, min_r = 1e9;
			for (size_t i = 0; i < m_tasks.size(); ++i) {
				double r = 1. + m_tasks[i].get_work_done() / m_time_interval;
				// r /= sum_r;

				if (!m_tasks[i].completed() && m_tasks[i].runnable()) {
					if (r > max_r)
						max_r = r;
					if (r < min_r)
						min_r = r;
				}
				// std::cerr << "," << r;
				std::cerr << r << " ";
			}
			// std::cerr << std::endl;

			std::cerr << " | " << min_r / max_r << std::endl;
#endif

			curr[max_i] += dt;

			if (any_completed(max_ts)) {
				if (m_stop_after_first)
					return;
				break;
			}
		}

		// break;
	}
}

void StrategyFair::set_interval(double dt)
{
	m_time_interval = dt;
}

