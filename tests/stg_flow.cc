#include <iomanip>
#include <cmath>
#include <random>
#include "gtest/gtest.h"

#include "StrategyFlow.hh"
#include "StrategyFCS.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyCmaxOptimal, solve_1) {
	static const size_t nr_tasks = 6;
	SystemStationary tasks(nr_tasks);
	tasks.init_base_rate(1, 0);

	std::default_random_engine gen;
	std::uniform_real_distribution<double> distribution(0.6, 1);

	auto scale_fn = [&] (size_t) {
		return distribution(gen);
	};

	tasks.init_scale_rnd(scale_fn);

	double work_total[nr_tasks] = {
		180, 626, 937, 1043, 1054, 1235
	};

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks.init_total_work(i, work_total[i]);

	double priority[nr_tasks] = {
		1, 6, 3, 3, 4, 5
	};

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks.init_priority(i, priority[i]);

	StrategyFlow stg;
	stg.add(tasks);
	stg.start();

	std::cerr << "stg.get_flowtime() = " << stg.get_flowtime() << std::endl;
	// std::cerr << "stg.get_makespan() = " << stg.get_makespan() << std::endl;

	// EXPECT_NEAR(stg.get_makespan(), 626.25, 1e-6);
}

