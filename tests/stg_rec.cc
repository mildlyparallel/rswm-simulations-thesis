#include "gtest/gtest.h"

#include "StrategyREC.hh"
#include "StrategyOptimal.hh"
#include "StrategyFCS.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyRECTest, solve_1)
{
	SystemStationary tasks = get_stationary_1();

	StrategyREC stg;
	stg.set_input(&tasks);
	stg.start();

	std::cerr << "stg = " << stg.get_makespan() << std::endl;
}

TEST(StrategyRECTest, solve_example)
{
	SystemStationary tasks = get_paper_example_2(7, 12);
	tasks.print();

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();

	StrategyREC stg_rec;
	stg_rec.set_input(&tasks);
	stg_rec.start();

	std::cerr << std::endl;

	std::cerr << "opt = " << stg_opt.get_makespan() << std::endl;

	double rho_rec = stg_rec.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rec = " << stg_rec.get_makespan() << " " << rho_rec << std::endl;
}

TEST(StrategyRECTest, solve_2)
{
	static const size_t nr_tasks = 3;
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	tasks[0].set_rate_scale({0}, 1);
	tasks[1].set_rate_scale({1}, 1);
	tasks[2].set_rate_scale({2}, 1);

	tasks[0].set_rate_scale({0, 1}, 1);
	tasks[1].set_rate_scale({0, 1}, 0.5);

	tasks[0].set_rate_scale({0, 2}, 1);
	tasks[2].set_rate_scale({0, 2}, 0.75);

	tasks[1].set_rate_scale({1, 2}, 0.5);
	tasks[2].set_rate_scale({1, 2}, 1);

	tasks[0].set_rate_scale({0, 1, 2}, 1);
	tasks[1].set_rate_scale({0, 1, 2}, 0.5);
	tasks[2].set_rate_scale({0, 1, 2}, 0.25);

	tasks[0].set_work_total(1);
	tasks[1].set_work_total(2);
	tasks[2].set_work_total(4);

	StrategyREC stg;
	stg.set_input(&tasks);
	stg.start();

	std::cerr << "stg = " << stg.get_makespan() << std::endl;
}
