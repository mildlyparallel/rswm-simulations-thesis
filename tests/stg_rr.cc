#include "gtest/gtest.h"
#include <cassert>
#include <algorithm>

#include "StrategyRR.hh"
#include "StrategyFCS.hh"
#include "StrategyOptimal.hh"
#include "StrategyFair.hh"
#include "TaskGraph.hh"

#include "common.hh"

using namespace ::testing;

// TEST(StrategyRRTest, solve_1)
// {
// 	SystemStationary tasks = get_stationary_1();
//
// 	StrategyRR stg;
// 	stg.set_input(&tasks);
// 	stg.open_log("tp-fcs-log.csv");
// 	stg.set_acq_fun(StrategyRR::MinWork);
// 	stg.start();
//
// 	std::cerr << "stg = " << stg.get_makespan() << std::endl;
// }

void set_comb_speed_eq(SystemStationary &tasks, Taskset ts, double val) {
	for (size_t i = 0; i < tasks.size(); ++i) {
		if (ts[i])
			tasks[i].set_rate_scale(ts, val);
	}
}

void print(SystemStationary &tasks, Taskset ts)
{
	std::cerr << ts << " {";
	bool comma = false;
	double s = 0;
	for (size_t i = 0; i < tasks.size(); ++i) {
		if (!ts[i])
			continue;
		double v = tasks[i].get_rate_scale(ts, 0);
		s += v;
		if (comma)
			std::cerr << ", ";
		comma = true;

		std::cerr << v;
	}

	std::cerr << "} sum=" << s << "\n";
}

TEST(StrategyCmaxOptimal, solve_eq_speed)
{
	size_t nr_tasks = 4;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	double w = 30;
	for (size_t i = 0; i < nr_tasks; ++i) {
		tasks[i].set_work_total(w);
		w += 20;
	}

	for (auto &ts : tasks.feasible_combinations()) {
		set_comb_speed_eq(tasks, ts, 1);
	}

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed);
	stg_rr.set_interval(10);
	stg_rr.start();
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	// double rho = stg_rr.get_makespan() / stg_opt.get_makespan();
	// std::cerr << rho << std::endl;
}

TEST(StrategyCmaxOptimal, solve_paper_example)
{
	size_t nr_tasks = 5;

	SystemStationary tasks = get_paper_example(nr_tasks);

	// tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed);
	stg_rr.set_interval(2);
	stg_rr.start();
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	StrategyFCS stg_fcs;
	stg_fcs.set_input(&tasks);
	stg_fcs.start();
	std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	double rho_fcs = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
	std::cerr << "rho_fcs = " << rho_fcs << std::endl;
}

TEST(StrategyCmaxOptimal, solve_worst_case)
{
	size_t nr_tasks = 5;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	size_t k = 3;
	double a = 0.2;

	tasks[0].set_work_total((nr_tasks - 1) / (k - 1));
	for (size_t i = 1; i < nr_tasks; ++i) {
		tasks[i].set_work_total(1);
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {

		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			if (ts.count() > k) {
				tasks[i].set_rate_scale(ts, 0.0001);
				continue;
			}

			if (ts[0] || ts.count() < k) {
				tasks[i].set_rate_scale(ts, 1);
			} else {
				tasks[i].set_rate_scale(ts, a);
			}
		}
	}

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	// stg_rr.set_acq_fun(StrategyRR::Utility);
	// stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedJian);
	stg_rr.set_interval(0.01);
	stg_rr.start();
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// StrategyFair stg_fair;
	// stg_fair.set_input(&tasks);
	// stg_fair.set_interval(5);
	// // stg_fair.stop_after_first(true);
	// stg_fair.start();
	// std::cerr << "stg_fair.get_makespan() = " << stg_fair.get_makespan() << std::endl;
	// std::cerr << "stg_fair.get_fairness() = " << stg_fair.get_fairness() << std::endl;

	// StrategyFCS stg_fcs;
	// stg_fcs.set_input(&tasks);
	// stg_fcs.start();
	// std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;
    //
	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	// double rho_rr_model = -(a_star*a_star - 2.*nr_tasks*a_star + nr_tasks) / (a_star * (nr_tasks-1));
	// std::cerr << "rho_rr_model = " << rho_rr_model << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	// double rho_fair = stg_fair.get_makespan() / stg_opt.get_makespan();
	// double rho_fcs = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
	// std::cerr << "rho_fcs = " << rho_fcs << std::endl;
	// std::cerr << "rho_fair = " << rho_fair << std::endl;
}

TEST(StrategyCmaxOptimal, solve_worst_case_2)
{
	size_t nr_tasks = 5;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	// tasks[0].set_work_total(200);
	// tasks[1].set_work_total(200);
	// tasks[2].set_work_total(200);
	// tasks[3].set_work_total(400);
	// tasks[4].set_work_total(200);
	// tasks[4].set_work_total(600);

	tasks[0].set_work_total((nr_tasks - 1) * 200);
	for (size_t i = 1; i < nr_tasks; ++i) {
		tasks[i].set_work_total(200);
	}
	// tasks[0].set_work_total((nr_tasks - 1) * 200);

	double a_star = 1.5;
	// tasks[0].set_work_total((nr_tasks - 1) * 200 * (1. / (a_star - 1)));
	// for (size_t i = 1; i < nr_tasks; ++i) {
	// 	tasks[i].set_work_total(200);
	// }

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		double left = std::min(a_star, 1. * ts.count());
		size_t left_cnt = ts.count();
		// left = 1.5;
		// left = a_star;

		if (ts.count() == 3) {
			// left = 1.5;
			// left = 1.87;
			// left = 1.99;
			// left = 2;
		}

		// Taskset ts012 = Taskset({0, 1, 2});

		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			// if (ts.count() == 3 && (ts & ts012).count() == 2) {
			// 	tasks[i].set_rate_scale(ts, a_star / ts.count());
			// 	continue;
			// }
//
			// if (ts.count() == 2 && (ts & ts012).count() == 2) {
			// 	tasks[i].set_rate_scale(ts, 0.69);
			// 	continue;
			// }

			if (ts.count() > 3) {
				tasks[i].set_rate_scale(ts, 0.01);
				continue;
			}

			double v = left / left_cnt;
			left -= v;
			left_cnt -= 1;
			tasks[i].set_rate_scale(ts, v);
		}
	}

	double v01 = 1.8;
	tasks[0].set_rate_scale(Taskset({0,1}), v01/2);
	tasks[1].set_rate_scale(Taskset({0,1}), v01/2);

	// tasks[0].set_rate_scale(Taskset({0,1}), 1.5/2);
	// tasks[1].set_rate_scale(Taskset({0,1}), 1.5/2);

	double aa = 2;
	tasks[2].set_rate_scale(Taskset({2,3,4}), aa/3);
	tasks[3].set_rate_scale(Taskset({2,3,4}), aa/3);
	tasks[4].set_rate_scale(Taskset({2,3,4}), aa/3);

	tasks[0].set_rate_scale(Taskset({0,1,3}), aa/3);
	tasks[1].set_rate_scale(Taskset({0,1,3}), aa/3);
	tasks[3].set_rate_scale(Taskset({0,1,3}), aa/3);

	// tasks[0].set_rate_scale(Taskset({0,1,3}), (v01/2) - 0.01);
	// tasks[1].set_rate_scale(Taskset({0,1,3}), (1.5 - (v01/2) + 0.01)/2);
	// tasks[3].set_rate_scale(Taskset({0,1,3}), (1.5 - (v01/2) + 0.01)/2);

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	// stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
	// stg_rr.set_acq_fun(StrategyRR::Utility);
	stg_rr.set_interval(1);

	stg_rr.stop_after_first(true);
	stg_rr.start();

	std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
	std::cerr << "stg_rr.get_fairness_jian() = " << stg_rr.get_fairness_jian() << std::endl;
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// std::vector<Taskset> sched;
	// for (size_t i = 0; i < 5; ++i) {
	// 	sched.push_back({1, 2, 3});
	// 	sched.push_back({0, 2, 3, 4});
	// 	sched.push_back({0, 1, 3, 4});
	// 	sched.push_back({0, 1, 2, 4});
	// 	sched.push_back({0, 1, 2, 3});
	// }
	// stg_rr.validate(sched);

	// std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// StrategyFCS stg_fcs;
	// stg_fcs.set_input(&tasks);
	// stg_fcs.start();
	// std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	// StrategyFair stg_fair;
	// stg_fair.set_input(&tasks);
	// stg_fair.set_interval(5);
	// stg_fair.start();
	// std::cerr << "stg_fair.get_makespan() = " << stg_fair.get_makespan() << std::endl;
	// std::cerr << "stg_fair.get_fairness() = " << stg_fair.get_fairness() << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	// double rho_fcs = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
	// std::cerr << "rho_fcs = " << rho_fcs << std::endl;
}

TEST(StrategyCmaxOptimal, solve_worst_case_3)
{
	size_t nr_tasks = 5;
	SystemStationary tasks = get_not_fair_case_1(nr_tasks);

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
	// stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	stg_rr.stop_after_first(true);
	stg_rr.set_interval(10);
	stg_rr.start();
	std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
	std::cerr << "stg_rr.get_fairness_jian() = " << stg_rr.get_fairness_jian() << std::endl;

}

TEST(StrategyCmaxOptimal, solve_worst_case_4)
{
	size_t nr_tasks = 5;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	double nnn = 10;
	tasks[0].set_work_total(200 * nnn);
	// tasks[1].set_work_total(200 * 1);
	tasks[1].set_work_total(200);
	tasks[2].set_work_total(200);
	tasks[3].set_work_total(200);
	tasks[4].set_work_total(200);

	// tasks[0].set_work_total(800);
	// tasks[1].set_work_total(800);
	// tasks[2].set_work_total(200);
	// tasks[3].set_work_total(200);
	// tasks[4].set_work_total(200);

	// tasks[0].set_work_total((nr_tasks - 1) * 200);
	// for (size_t i = 1; i < nr_tasks; ++i) {
	// 	tasks[i].set_work_total(200);
	// }
	// tasks[0].set_work_total((nr_tasks - 1) * 200);

	// double a_star = 2;
	// tasks[0].set_work_total((nr_tasks - 1) * 200 * (1. / (a_star - 1)));
	// for (size_t i = 1; i < nr_tasks; ++i) {
	// 	tasks[i].set_work_total(200);
	// }

	// double v0 = 1.41;

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}


			if (i == 0) {
				tasks[i].set_rate_scale(ts, 1);
			// } else if (ts.count() < nr_tasks -1 && !ts[0]) {
			// } else if (ts.subset_of({1,2,3,4})) {
				// tasks[i].set_rate_scale(ts, std::min(v0, 1. * ts.count()) / ts.count());
			} else {
				tasks[i].set_rate_scale(ts, 1. / (nnn));
			}
		}
	}

	// ts = {1,2,3,4};
	// tasks[1].set_rate_scale(ts, v0/ts.count());

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedJian);
	// stg_rr.set_acq_fun(StrategyRR::Angle);
	// stg_rr.set_acq_fun(StrategyRR::Utility);
	stg_rr.set_interval(1);

	// stg_rr.stop_after_first(true);
	stg_rr.start();

	std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// std::vector<Taskset> sched;
	// for (size_t i = 0; i < 5; ++i) {
	// 	sched.push_back({1, 2, 3});
	// 	sched.push_back({0, 2, 3, 4});
	// 	sched.push_back({0, 1, 3, 4});
	// 	sched.push_back({0, 1, 2, 4});
	// 	sched.push_back({0, 1, 2, 3});
	// }
	// stg_rr.validate(sched);

	// std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// StrategyFCS stg_fcs;
	// stg_fcs.set_input(&tasks);
	// stg_fcs.start();
	// std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	// StrategyFair stg_fair;
	// stg_fair.set_input(&tasks);
	// stg_fair.set_interval(5);
	// stg_fair.start();
	// std::cerr << "stg_fair.get_makespan() = " << stg_fair.get_makespan() << std::endl;
	// std::cerr << "stg_fair.get_fairness() = " << stg_fair.get_fairness() << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	// double rho_fcs = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
	// std::cerr << "rho_fcs = " << rho_fcs << std::endl;
}

SystemStationary get_system_case_5(size_t nr_tasks, double a_star, double vv)
{

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);


	tasks[0].set_work_total((nr_tasks - 1) * 20 * (1. / (a_star - 1)));
	// tasks[0].set_work_total((nr_tasks - 1) * 20);
	for (size_t i = 1; i < nr_tasks; ++i) {
		tasks[i].set_work_total(20);
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		double left = std::min(a_star, 1. * ts.count());
		size_t left_cnt = ts.count();

		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				continue;
			}

			if (ts.count() > a_star) {
				tasks[i].set_rate_scale(ts, 0.01);
				continue;
			}

			if (ts[0]) {
				double v = left / left_cnt;
				left -= v;
				left_cnt -= 1;
				tasks[i].set_rate_scale(ts, v);
			} else {
				tasks[i].set_rate_scale(ts, vv / ts.count());
			}
		}
	}

	// tasks.print();

	// std::cerr << "Validating ";
	// bool tasks_ok = tasks.validate();
	// if (tasks_ok) {
	// 	std::cerr << " Ok" << std::endl;
	// } else {
	// 	std::cerr << " Not valid" << std::endl;
	// 	std::abort();
	// }
	return tasks;
}

TEST(StrategyCmaxOptimal, solve_worst_case_5)
{
	// std::cerr << "vv,rho" << std::endl;
	std::cerr << "a_star,nr_tasks,vv,theta,af,rho" << std::endl;

	for (size_t a_star = 4; a_star < 5; ++a_star) {
		for (size_t nr_tasks = a_star + 1; nr_tasks < 20; nr_tasks += 2) {
			for (double vv = 0.5 * a_star; vv <= a_star; vv += 0.1) {
				a_star = 3;
				nr_tasks = 4;
				vv = 2.8;

				SystemStationary tasks = get_system_case_5(nr_tasks, a_star, vv);

				StrategyRR stg_rr;
				stg_rr.set_input(&tasks);
				// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
				// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedJian);
				// stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
				stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
				// stg_rr.set_acq_fun(StrategyRR::Utility);
				stg_rr.set_interval(0.1);

				std::cerr << a_star << "," << nr_tasks << "," << vv << ",";
				stg_rr.start();

				// std::cerr << stg_rr.get_fairness() << ",";
				// std::cerr << stg_rr.get_avg_speed() << "\n";

				// std::cerr << "vv = " << vv << std::endl;
				// std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
				// std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

				StrategyOptimal stg_opt;
				stg_opt.set_input(&tasks);
				stg_opt.start();
				// std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

				double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
				std::cerr << "," << rho_rr << std::endl;
				break;
			}
				break;
		}
				break;
	}
}


TEST(StrategyCmaxOptimal, convergence_test)
{
	double x = 0;
	double y = 0;

	double ay = 1./9;
	double ax = 1;

	int ny = 9;
	int nx = 1;

	int T = 1000;

	double ax1 = ax / nx;
	double ay1 = ay / ny;
	double tx = 0;
	double ty = 0;
	double dt = 1;

	for (double t = 0; t < T; t += dt) {
		double fx = (1. + x) / ax;
		double fy = (1. + y) / ay;

		std::cout << t << ",";
		std::cout << fx << "," << fy << ",";
		std::cout << y << "," << x << ",";

		if (fx < fy) {
			tx++;
			x += ax1 * dt;
			std::cout << "x,";
		} else {
			ty++;
			y += ay1 * dt;
			std::cout << "y,";
		}
		// std::cout << std::endl;
		double theta = std::min(x, y) / std::max(x, y);

		std::cout << theta << std::endl;
	}

	std::cout << "ax1 = " << ax1 << std::endl;
	std::cout << "ay1 = " << ay1 << std::endl;

	std::cerr << "ax1/ay1 = " << ax1/ay1 << std::endl;
	std::cerr << "ay1/ax1 = " << ay1/ax1 << std::endl;

	std::cout << "(x*nx + y*ny)/T = " << (x*nx + y*ny)/T << std::endl;

	double dty = 1. * ny / nx;
	double dtx = 1. * nx / ny;
	double slope_min = (ax1)/(1. + dty);
	double slope_max = (ay1*dty)/(1. + dty);

	double alpha = 1. * nx / (nx + ny);
	std::cout << "alpha = " << alpha << std::endl;

	std::cout << "ax1*alpha = " << ax1*alpha << std::endl;
	std::cout << "ay1*(1-alpha) = " << ay1*(1-alpha) << std::endl;

	std::cout << "dty = " << dty << std::endl;
	std::cout << "dtx = " << dtx << std::endl;
	std::cout << "slope_min = " << slope_min << std::endl;
	std::cout << "slope_max = " << slope_max << std::endl;
	std::cout << "slope_avg = " << (slope_min + slope_max)/2 << std::endl;
	std::cout << "slope_max/slope_min = " << slope_max/slope_min << std::endl;
	std::cout << "ay/ax = " << ay/ax << std::endl;

	std::cout << "(ax+ay*dty)/(1.+dty) = " << (ax+ay*dty)/(1.+dty) << std::endl;
	std::cout << "(alpha*ax+(1-alpha)*ay) = " << (alpha*ax+(1-alpha)*ay) << std::endl;

	std::cout << "(x+y)/(2.*T) = " << (x+y)/(2.*T) << std::endl;
}

TEST(StrategyCmaxOptimal, convergence_test_2)
{
	double xx = 1;
	double xz = 1;
	double zz = 1;
	double yy = 1;
	double yz = 1;

	double ax = 2;
	double az = ax;
	double ay = 1.8;

	double nxx = 2;
	double nxz = 1;
	double nzz = 0;
	double nyz = 2;
	double nyy = 0;

	double nx = nxx + nxz;
	double nz = nxz + nzz + nyz;
	double ny = nyz + nyy;

	int T = 30;

	double ax1 = ax / nx;
	double ay1 = ay / ny;
	double az1 = az / nz;

	double dt = 0.5;

	std::ofstream out("ctest2.csv");

	out << "t,";
	out << "fx,fy,fz,";
	out << "xx,xz,zz,yz,yy,util,choice\n";

	for (double t = 0; t < T; t += dt) {
		double fx = ax1 * (nxx/xx + nxz/xz);
		double fy = ay1 * (nyy/yy + nyz/yz);
		double fz = az1 * (nxz/xz + nzz/zz + nyz/yz);

		out << t << ",";
		out << fx << "," << fy << "," << fz << ",";
		out << xx << "," << xz << "," << zz << "," << yz << "," << yy << ",";

		double util = xx * xz * zz * yz * yy;
		out << util << ",";

		if (fx > std::max(fy, fz)) {
			xx += ax1 * dt;
			xz += ax1 * dt;
			out << "x";
		} else if (fy > std::max(fx, fz)) {
			yy += ay1 * dt;
			yz += ay1 * dt;
			out << "y";
		} else {
			assert(fz >= std::max(fx, fy));
			xz += az1 * dt;
			zz += az1 * dt;
			yz += az1 * dt;
			out << "z";
		}

		out << std::endl;
	}

	double tot_work = 0;
	tot_work += (xx - 1) * nxx + (xz - 1) * nxz;
	tot_work += (yy - 1) * nyy + (yz - 1) * nyz;
	tot_work += (zz - 1) * nzz;

	std::cout << "tot_work = " << tot_work << std::endl;
	double avg_speed = tot_work / T;
	std::cout << "avg_speed = " << avg_speed << std::endl;
}

TEST(StrategyCmaxOptimal, convergence_test_3)
{
	double rp = 1;
	double rq = 1;

	double ap = 2;

	double np = 2;

	int T = 40;

	double dt = 1;

	std::ofstream out("/tmp/ctest3.csv");

	out << "t,";
	out << "fp,fq,";
	out << "rp,rq,choice\n";

	for (double t = 0; t < T; t += dt) {
		double fp = (ap / np) * (1. / rq + (np - 1.) / rp);
		double fq = 1. / rq;

		out << t << ",";
		out << fp << "," << fq << ",";
		out << rp << "," << rq << ",";

		if (fp > fq) {
			rp += (ap / np) * dt;
			rq += (ap / np) * dt;
			out << "p";
		} else {
			rq += dt;
			out << "q";
		}

		out << std::endl;
	}
}

TEST(StrategyCmaxOptimal, convergence_test_4)
{
	double rpp = 1;
	double rpq = 1;
	double rqq = 1;

	double ap = 2;
	double aq = 2;

	double npp = 2;
	double npq = 2;
	double nqq = 8;
	double np = npp + npq;
	double nq = nqq + npq;

	int T = 60;

	double dt = 0.1;

	std::ofstream out("ctest4.csv");

	out << "t,";
	out << "fp,fq,";
	out << "rpp,rpq,rqq,choice\n";

	for (double t = 0; t < T; t += dt) {
		double fp = (ap / np) * (npp / rpp + npq / rpq);
		double fq = (aq / nq) * (nqq / rqq + npq / rpq);

		out << t << ",";
		out << fp << "," << fq << ",";
		out << rpp << "," << rpq << "," << rqq << ",";

		if (fp > fq) {
			rpp += (ap / np) * dt;
			rpq += (ap / np) * dt;
			out << "p";
		} else {
			rqq += (aq / nq) * dt;
			rpq += (aq / nq) * dt;
			out << "q";
		}

		out << std::endl;
	}
}

TEST(StrategyCmaxOptimal, solve_worst_case_6)
{
	size_t nr_tasks = 3;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	tasks[0].set_work_total(400);
	tasks[1].set_work_total(200);
	tasks[2].set_work_total(200);

	// tasks[0].set_work_total(200);
	// tasks[1].set_work_total(200);
	// tasks[2].set_work_total(100);

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			if (ts.count() > 2) {
				tasks[i].set_rate_scale(ts, 0.01);
				continue;
			}
		}
	}

	double vv = 2;
	tasks[0].set_rate_scale(Taskset({0,1}), vv/2);
	tasks[1].set_rate_scale(Taskset({0,1}), vv/2);

	tasks[0].set_rate_scale(Taskset({0,2}), vv/2);
	tasks[2].set_rate_scale(Taskset({0,2}), vv/2);


	vv = 1.53;
	tasks[1].set_rate_scale(Taskset({1,2}), vv/2);
	tasks[2].set_rate_scale(Taskset({1,2}), vv/2);



	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	stg_rr.set_interval(1);

	stg_rr.stop_after_first(true);
	stg_rr.start();

	std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
	std::cerr << "stg_rr.get_fairness_jian() = " << stg_rr.get_fairness_jian() << std::endl;
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
}

TEST(StrategyCmaxOptimal, convergence_test_7)
{
	double r1 = 0;
	double r2 = 0;
	double r3 = 0;

	double a0 = 2;
	double a1 = 1.7;

	double dt = 0.1;
	int T = 40;

	std::ofstream out("/tmp/ctest7.csv");

	out << "t,";
	out << "f0,f1,";
	out << "r1,r2,r3,choice\n";

	size_t n0 = 0;
	size_t n1 = 0;

	for (double t = 0; t < T; t += dt) {
		double f0 = 2. * a0;
		double f1 = 2. * a1;

		if (t > 0) {
			f0 /= r1 + r2;
			f1 /= r2 + r3;
		}

		out << t << ",";
		out << f0 << "," << f1 << ",";
		out << r1 << "," << r2 << "," << r3 << ",";

		if (f0 > f1) {
			r1 += a0 * dt;
			r2 += a0 * dt;
			out << "0";
			n0++;
		} else {
			r2 += a1 * dt;
			r3 += a1 * dt;
			out << "1";
			n1++;
		}

		out << std::endl;
	}

	std::cerr << "n0 = " << n0 << std::endl;
	std::cerr << "n1 = " << n1 << std::endl;
	double alpha = 1. * n0/(n0 + n1);
	std::cerr << "alpha = " << alpha << std::endl;

	double a10 = a1 / a0;
	double alpha_m = (2. - a10) / (4. - a10 - 1./a10);
	std::cerr << "alpha_m = " << alpha_m << std::endl;

	double af = a0 * alpha + a1 * (1. - alpha);
	std::cerr << "af = " << af << std::endl;

	double af_m = a0 * alpha_m + a1 * (1. - alpha_m);
	std::cerr << "af_m = " << af_m << std::endl;
}

template <typename T>
T choose(T n, T k) {
	if (k == 0)
		return 1;
	return n * choose(n - 1, k - 1) / k;
}

TEST(StrategyCmaxOptimal, convergence_test_8)
{
	double r1 = 0;
	double r2 = 0;
	double r3 = 0;
	double r4 = 0;

	double a0 = 1;
	double a1 = 2.8 / 3.;

	double dt = .1;
	int T = 100;

	std::ofstream out("/tmp/ctest8.csv");

	out << "t,";
	out << "f0,f1,f2,f4,";
	out << "r1,r2,r3,r4,choice\n";

	size_t n0 = 0;
	size_t n1 = 0;
	size_t n2 = 0;
	size_t n3 = 0;

	double af = 0;

	for (double t = 0; t < T; t += dt) {
		// double f0 = 3. * a0;
		// double f1 = 3. * a0;
		// double f2 = 3. * a0;
		// double f3 = 3. * a1;

		double f0 = a0;
		double f1 = a0;
		double f2 = a0;
		double f3 = a1;

		if (t > 0) {
			// f0 /= r1 + r2 + r3;
			// f1 /= r1 + r2 + r4;
			// f2 /= r1 + r3 + r4;
			// f3 /= r2 + r3 + r4;

			f0 *= (1./r1 + 1./r2 + 1./r3);
			f1 *= (1./r1 + 1./r2 + 1./r4);
			f2 *= (1./r1 + 1./r3 + 1./r4);
			f3 *= (1./r2 + 1./r3 + 1./r4);
		}

		out << t << ",";
		out << f0 << "," << f1 << "," << f2 << "," << f3 << ",";
		out << r1 << "," << r2 << "," << r3 << "," << r4 << ",";

		if (f0 >= std::max({f1, f2, f3})) {
			r1 += a0 * dt;
			r2 += a0 * dt;
			r3 += a0 * dt;

			af += 3. * a0 * dt;
			out << "0";
			n0++;
		} else if (f1 >= std::max({f0, f2, f3})) {
			r1 += a0 * dt;
			r2 += a0 * dt;
			r4 += a0 * dt;

			af += 3. * a0 * dt;
			out << "1";
			n1++;
		} else if (f2 >= std::max({f0, f1, f3})) {
			r1 += a0 * dt;
			r3 += a0 * dt;
			r4 += a0 * dt;

			af += 3. * a0 * dt;
			out << "2";
			n2++;
		} else {
			assert(f3 > std::max({f0, f1, f2}));
			r2 += a1 * dt;
			r3 += a1 * dt;
			r4 += a1 * dt;

			af += 3. * a1 * dt;
			out << "3";
			n3++;
		}

		out << std::endl;
	}

	af /= T;


	std::cerr << "n0 = " << n0 << std::endl;
	std::cerr << "n1 = " << n1 << std::endl;
	std::cerr << "n2 = " << n2 << std::endl;
	std::cerr << "n3 = " << n3 << std::endl;

	double gamma_exp = 1. * (n0 + n1 + n2) / (n0 + n1 + n2 + n3);
	std::cerr << "gamma_exp = " << gamma_exp << std::endl;

	double af_exp = gamma_exp * a0 + (1. - gamma_exp) * a1;
	af_exp *= 3;
	std::cerr << "af_exp = " << af_exp << std::endl;

	double a10 = a1 / a0;
	double a01 = a0 / a1;
	std::cerr << "a10 = " << a10 << std::endl;
	std::cerr << "a01 = " << a01 << std::endl;

	int k = 3;
	int n = 4;
	int C2 = choose(n - 1, k);
	int C1 = choose(n, k) - C2;
	int R1 = choose(n - 2, n - k);
	int R2 = choose(n - 2, n - k - 1);

	std::cerr << "a0*k = " << a0*k << std::endl;
	std::cerr << "a1*k = " << a1*k << std::endl;

	double RC1 = 1. * R1 / C1;
	double RC2 = 1. * R2 / C2;

	std::cerr << "RC1 = " << RC1 << std::endl;
	std::cerr << "RC2 = " << RC2 << std::endl;

	double gamma_pf_rhs = a10 * RC2;
	double gamma_pf_lhs = a10 * RC2 - RC1 + a10 * k - k + 1;
	double gamma_pf = gamma_pf_rhs / gamma_pf_lhs;
	std::cerr << "gamma_pf = " << gamma_pf << std::endl;

	// double gamma_rhs = RC2 * k - a10 * RC2 * (k - 1);
	// double gamma_lhs = 1. + RC1 * (k - 1) + RC2 * k - a10 * RC2 * (k - 1) - a01 * RC1 * k;
	// double gamma_m = gamma_rhs / gamma_lhs;
	// std::cerr << "gamma_rhs = " << gamma_rhs << std::endl;
	// std::cerr << "gamma_lhs = " << gamma_lhs << std::endl;
	// std::cerr << "gamma_m = " << gamma_m << std::endl;
	// double af_m = (a0 * gamma_m + a1 * (1 - gamma_m)) * k;
	// std::cerr << "af_m = " << af_m << std::endl;
	double af_pf = (a0 * gamma_pf + a1 * (1 - gamma_pf)) * k;
	std::cerr << "af_af = " << af_pf << std::endl;
    //
	double r0_exp = a0 * gamma_exp;
	double r1_exp = a0 * gamma_exp * RC1 + a1 * (1. - gamma_exp) * RC2;

	std::cerr << "r0_exp = " << r0_exp << std::endl;
	std::cerr << "r1_exp = " << r1_exp << std::endl;

	std::cerr << "r1/T = " << r1/T << std::endl;
	std::cerr << "r2/T = " << r2/T << std::endl;
	std::cerr << "r3/T = " << r3/T << std::endl;
	std::cerr << "r4/T = " << r4/T << std::endl;

	double f0_pf = a0 * (1./r0_exp + (k - 1)/r1_exp);
	double f1_pf = a1 * (k/r1_exp);

	std::cerr << "f0_pf/T = " << f0_pf/T << std::endl;
	std::cerr << "f1_pf/T = " << f1_pf/T << std::endl;

	// double f0 = k * a0 / (r0_exp + r1_exp * (k - 1));
	// double f1 = k * a1 / (r1_exp * k);
	// std::cerr << "f0 = " << f0 << std::endl;
	// std::cerr << "f1 = " << f1 << std::endl;


	// double alpha_m = (2. - a10) / (7. - 2.* a10 - 2.*a01);
	// std::cerr << "alpha_m = " << alpha_m << std::endl;
    //
	// // double af = a0 * alpha * 2. + a1 * (1. - 2. * alpha);
	// std::cerr << "af = " << af << std::endl;
    //
	// double f0 = 2 * a0 / (a0 * 3. * alpha_m + a1 * (1. - 2.*alpha_m)) / T;
	// double f2 = 2 * a1 / (a0 * 2. * alpha_m + 2. * a1 * (1. - 2.*alpha_m)) / T;
	// std::cerr << "f0 = " << f0 << std::endl;
	// std::cerr << "f2 = " << f2 << std::endl;
    //
	// double af_m = a0 * 2. * alpha_m + a1 * (1. - 2. * alpha_m);
	// af_m *= 2.;
	// std::cerr << "af_m = " << af_m << std::endl;
}

TEST(StrategyCmaxOptimal, solve_worst_case_9)
{
	size_t nr_tasks = 4;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	size_t k = 2;
	double eps = 0.05;
	// double eps = 0.20;

	for (size_t i = 0; i < k; ++i) {
		tasks[i].set_work_total(1);
	}

	for (size_t i = k; i < nr_tasks; ++i) {
		tasks[i].set_work_total(1. * eps);
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			if (i < k)
				tasks[i].set_rate_scale(ts, 1);
			else
				tasks[i].set_rate_scale(ts, eps);
		}
	}


	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	// stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedMMF);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeedJian);
	// stg_rr.set_acq_fun(StrategyRR::Angle);
	// stg_rr.set_acq_fun(StrategyRR::Utility);
	stg_rr.set_interval(0.001);

	// stg_rr.stop_after_first(true);
	stg_rr.start();

	std::cerr << "stg_rr.get_fairness() = " << stg_rr.get_fairness() << std::endl;
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	double rho_rr = stg_rr.get_makespan() / stg_opt.get_makespan();
	// double rho_fcs = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho_rr = " << rho_rr << std::endl;
	// std::cerr << "rho_fcs = " << rho_fcs << std::endl;
}
