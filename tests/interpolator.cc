#include "gtest/gtest.h"

#include "Interpolator.hh"

using namespace ::testing;

TEST(InterpTest, get__singe_task)
{
	Interpolator intr(0, 4);

	Interpolator::Estimate est = intr.get({0}); 

	EXPECT_EQ(est.mean, 1.);
	EXPECT_EQ(est.delta, 0.);
	EXPECT_EQ(est.sub, Taskset());
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 3}));
}

TEST(InterpTest, get__singe_task_2)
{
	Interpolator intr(0, 4);

	intr.add({0, 1}, 0.6);

	Interpolator::Estimate est = intr.get({0}); 

	EXPECT_EQ(est.mean, 1.);
	EXPECT_EQ(est.delta, 0.);
	EXPECT_EQ(est.sub, Taskset());
	EXPECT_EQ(est.sup, Taskset({0, 1}));
}

TEST(InterpTest, get__all_tasks)
{
	Interpolator intr(0, 4);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.5);
	EXPECT_EQ(est.delta, 0.5);

	EXPECT_EQ(est.sub, Taskset({0}));
	EXPECT_EQ(est.sup, Taskset());
}

TEST(InterpTest, get__all_tasks_2)
{
	Interpolator intr(0, 4);

	intr.add({0, 1}, 0.6);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.3);
	EXPECT_EQ(est.delta, 0.3);

	EXPECT_EQ(est.sub, Taskset({0, 1}));
	EXPECT_EQ(est.sup, Taskset());
}

TEST(InterpTest, get__exact)
{
	Interpolator intr(0, 4);

	intr.add({0, 1}, 0.3);

	Interpolator::Estimate est = intr.get({0, 1}); 

	EXPECT_EQ(est.mean, 0.3);
	EXPECT_EQ(est.delta, 0);
	EXPECT_EQ(est.sub, Taskset());
	EXPECT_EQ(est.sup, Taskset());
}

TEST(InterpTest, get__approx_superset)
{
	Interpolator intr(0, 6);

	intr.add({0, 1}, 0.4);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.2);
	EXPECT_EQ(est.delta, 0.2);

	EXPECT_EQ(est.sub, Taskset({0, 1}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 3, 4, 5}));
}

TEST(InterpTest, get__approx_superset_multiple)
{
	Interpolator intr(0, 6);

	intr.add({0, 1}, 0.5);
	intr.add({0, 2}, 0.2);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.1);
	EXPECT_EQ(est.delta, 0.1);

	EXPECT_EQ(est.sub, Taskset({0, 2}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 3, 4, 5}));
}

TEST(InterpTest, get__approx_superset_multiple_nested)
{
	Interpolator intr(0, 8);

	intr.add({0, 1}, 0.5);
	intr.add({0, 1, 2, 3}, 0.4);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3, 4, 5}); 

	EXPECT_EQ(est.mean, 0.2);
	EXPECT_EQ(est.delta, 0.2);

	EXPECT_EQ(est.sub, Taskset({0, 1, 2, 3}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 3, 4, 5, 6, 7}));
}

TEST(InterpTest, get__approx_subset)
{
	Interpolator intr(0, 6);

	intr.add({0, 1}, 0.8);
	intr.add({0, 1, 2, 3, 4, 5}, 0.2);

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.2 + (0.8 - 0.2)/2);
	EXPECT_EQ(est.delta, 0.3);

	EXPECT_EQ(est.sub, Taskset({0, 1}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 3, 4, 5}));
}

TEST(InterpTest, get__approx_subset_multiple)
{
	Interpolator intr(0, 6);

	intr.add({0, 1, 2, 3}, 0.5);
	intr.add({0, 1, 2, 4}, 0.8);

	Interpolator::Estimate est = intr.get({0, 1}); 

	double y = 0.8 + (1. - 0.8)*(2. - 1.)/(4. - 1.);
	EXPECT_EQ(est.mean, y);
	EXPECT_EQ(est.delta, y - 0.8);

	EXPECT_EQ(est.sub, Taskset({0}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 2, 4}));
}

TEST(InterpTest, get__approx_subset_multiple_nested)
{
	Interpolator intr(0, 4);

	intr.add({0, 1, 3}, 0.8);
	intr.add({0, 1, 3, 5}, 0.7);

	Interpolator::Estimate est = intr.get({0, 1}); 

	double y = 0.8 + (1. - 0.8)*(2. - 1.)/(3. - 1.);
	EXPECT_EQ(est.mean, y);
	EXPECT_EQ(est.delta, y - 0.8);

	EXPECT_EQ(est.sub, Taskset({0}));
	EXPECT_EQ(est.sup, Taskset({0, 1, 3}));
}

TEST(InterpTest, simplify)
{
	Interpolator intr(0, 6);

	intr.add({0, 1}, 0.8);
	intr.add({0, 1, 2, 3}, 0.5);
	intr.add({0, 1, 2, 3, 4, 5}, 0.2);

	intr.simplify();

	Interpolator::Estimate est = intr.get({0, 1, 2, 3}); 

	EXPECT_EQ(est.mean, 0.5);
	EXPECT_EQ(est.delta, 0.3);
}

