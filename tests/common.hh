#pragma once

#include <random>

#include "SystemStationary.hh"
#include "TaskGraph.hh"

SystemStationary get_stationary_1()
{
	static const size_t nr_tasks = 4;
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	auto rate_fn = [&] (Taskset ts) {
		return 1. - 0.2 * (ts.count() - 1);
	};

	tasks.init_rate_scale(rate_fn);

	double work_total[nr_tasks] = {
		90, 226, 337, 425
	};

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks.init_total_work(i, work_total[i]);

	return tasks;
}

SystemStationary get_stationary_2()
{
	std::default_random_engine gen;
	gen.seed(123);

	static const size_t nr_tasks = 4;
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	std::uniform_int_distribution<int> work_distr(100, 200);
	tasks.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});

	std::uniform_real_distribution<double> slope_coeff(0, 1);
	double slope_distr_min[nr_tasks];
	for (size_t i = 0; i < tasks.size(); ++i)
		slope_distr_min[i] = slope_coeff(gen);

	tasks.init_scale_rnd([&] (size_t task_id) -> double {
		std::uniform_real_distribution<double> slope_coeff(slope_distr_min[task_id], 1);
		return slope_coeff(gen);
	});

	return tasks;
}

SystemStationary get_paper_example(size_t nr_tasks, double b1 = 10)
{
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	tasks[0].set_work_total(b1);
	double b2 = b1 * (nr_tasks - 2.) / (nr_tasks - 1.);
	for (size_t i = 1; i < nr_tasks; ++i)
		tasks[i].set_work_total(b2);

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (ts.count() == nr_tasks) {
				tasks[i].set_rate_scale(ts, 1e-3);
				continue;
			}

			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
			} else {
				tasks[i].set_rate_scale(ts, 1);
			}
		}
	}

	return tasks;
}

SystemStationary get_paper_example_2(size_t nr_tasks, double b1 = 10)
{
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	size_t k = nr_tasks - 1;

	double b2 = b1;
	tasks[0].set_work_total(b1);
	for (size_t i = 1; i < nr_tasks; ++i)
		tasks[i].set_work_total(b2);

	double rho = 2. - 2. / (nr_tasks);
	std::cerr << "rho = " << rho << std::endl;

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (ts.count() > k) {
				tasks[i].set_rate_scale(ts, 1e-2);
				continue;
			}

			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
			} else {
				if (ts.count() < k)
					tasks[i].set_rate_scale(ts, 1.);
				else
					tasks[i].set_rate_scale(ts, 1);
			}
		}
	}

	return tasks;
}


// SystemStationary get_prec_example_1()
// {
// 	size_t nr_rows = 3;
// 	size_t nr_cols = 3;
// 	size_t nr_tasks = nr_rows * nr_cols;
//
// 	TaskGraph graph;
// 	graph.add_tasks_no_prec(nr_tasks);
//
// 	for (size_t i = 0; i < nr_rows; ++i) {
// 		
// 	}
//
// 	graph.find_combinations();
//
// 	SystemStationary tasks(nr_tasks);
//
// 	tasks.set_precedence(graph);
//
// 	tasks.init_base_rate(1);
//
// 	size_t k = nr_tasks - 1;
//
// 	double b2 = b1;
// 	tasks[0].set_work_total(b1);
// 	for (size_t i = 1; i < nr_tasks; ++i)
// 		tasks[i].set_work_total(b2);
//
// 	for (auto &ts : tasks.feasible_combinations()) {
// 		for (size_t i = 0; i < nr_tasks; ++i) {
// 			if (ts.count() > k) {
// 				tasks[i].set_rate_scale(ts, 1e-2);
// 				continue;
// 			}
//
// 			if (!ts[i]) {
// 				tasks[i].set_rate_scale(ts, 0);
// 			} else {
// 				if (ts.count() < k)
// 					tasks[i].set_rate_scale(ts, 1.);
// 				else
// 					tasks[i].set_rate_scale(ts, 1);
// 			}
// 		}
// 	}
//
// 	return tasks;
// }

SystemStationary get_not_fair_case_1(size_t nr_tasks = 3)
{
	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks[i].set_work_total(1000);

	double eps = 1. / nr_tasks;

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			if (i == 0) {
				tasks[i].set_rate_scale(ts, 1);
			} else {
				tasks[i].set_rate_scale(ts, eps);
			}
		}
	}

	return tasks;
}

SystemStationary get_not_fair_case_2()
{
	size_t nr_tasks = 3;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);

	tasks.init_base_rate(1);

	tasks[0].set_work_total(100);
	tasks[1].set_work_total(100);
	tasks[2].set_work_total(100);

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}
		}
	}

	tasks[1].set_rate_scale(Taskset({1,2}), 1);
	tasks[2].set_rate_scale(Taskset({1,2}), 1);

	tasks[0].set_rate_scale(Taskset({0,2}), 1);
	tasks[2].set_rate_scale(Taskset({0,2}), 0.4);

	tasks[0].set_rate_scale(Taskset({0,1}), 1);
	tasks[1].set_rate_scale(Taskset({0,1}), 1);

	tasks[0].set_rate_scale(Taskset({0,1,2}), 0.7);
	tasks[1].set_rate_scale(Taskset({0,1,2}), 1);
	tasks[2].set_rate_scale(Taskset({0,1,2}), 0.3);

	return tasks;
}
