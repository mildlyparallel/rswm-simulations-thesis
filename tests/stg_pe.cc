#include "gtest/gtest.h"

#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyOptimal.hh"
#include "StrategyFair.hh"
#include "TaskGraph.hh"

#include "common.hh"

using namespace ::testing;

void set_comb_speed_eq(SystemStationary &tasks, Taskset ts, double val) {
	for (size_t i = 0; i < tasks.size(); ++i) {
		if (ts[i])
			tasks[i].set_rate_scale(ts, val);
	}
}

TEST(StrategyPETest, solve_1)
{
	size_t nr_tasks = 5;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	double alpha = 1;
	double as_max = std::pow(1.*nr_tasks, alpha / (1 + alpha));

	std::cerr << "as_max = " << as_max << std::endl;

	double as_0 = 0.99 * as_max / nr_tasks;

	// tasks[0].set_work_total((nr_tasks - 1) * (1. / (a_star - 1)));
	for (size_t i = 0; i < nr_tasks; ++i) {
		tasks[i].set_work_total(1);
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		double a = std::min(as_0, 1. * ts.count());

		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			tasks[i].set_rate_scale(ts, a);
		}
	}

	tasks.print();

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	StrategyFCS stg_pe;
	stg_pe.set_input(&tasks);
	stg_pe.set_acq_fun(StrategyFCS::SpeedEfficiencySq);
	stg_pe.set_efficiency(alpha);
	stg_pe.start();

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();

	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;
	std::cerr << "stg_pe.get_makespan()  = " << stg_pe.get_makespan() << std::endl;

	double rho = stg_pe.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho = " << rho << std::endl;
}

TEST(StrategyPETest, solve_2)
{
	size_t nr_tasks = 6;

	TaskGraph graph;
	graph.add_tasks_no_prec(nr_tasks);

	SystemStationary tasks(nr_tasks);

	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	size_t k = nr_tasks / 2 + 1;
	std::cerr << "k = " << k << std::endl;
	double alpha = 1;
	double as_max = std::pow(1.*k, alpha / (1 + alpha));

	std::cerr << "as_max = " << as_max << std::endl;

	double as_0 = 1.0001 * as_max / k;

	for (size_t i = 0; i < nr_tasks; ++i) {
		tasks[i].set_work_total(1);
	}

	Taskset ts;
	while (ts.next_combination(0, nr_tasks)) {
		for (size_t i = 0; i < nr_tasks; ++i) {
			if (!ts[i]) {
				tasks[i].set_rate_scale(ts, 0);
				continue;
			}

			if (ts.count() == 1) {
				tasks[i].set_rate_scale(ts, 1);
				break;
			}

			if (ts.count() > k)
				tasks[i].set_rate_scale(ts, 1e-6);
			else
				tasks[i].set_rate_scale(ts, as_0);
		}
	}

	tasks.print();

	// std::cerr << "Validating ";
	// bool tasks_ok = tasks.validate();
	// if (tasks_ok) {
	// 	std::cerr << " Ok" << std::endl;
	// } else {
	// 	std::cerr << " Not valid" << std::endl;
	// 	std::abort();
	// }

	StrategyFCS stg_pe;
	stg_pe.set_input(&tasks);
	stg_pe.set_acq_fun(StrategyFCS::SpeedEfficiencySq);
	stg_pe.set_efficiency(alpha);
	stg_pe.start();

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();

	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;
	std::cerr << "stg_pe.get_makespan()  = " << stg_pe.get_makespan() << std::endl;

	double rho = stg_pe.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho = " << rho << std::endl;
}


