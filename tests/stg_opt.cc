#include <iomanip>
#include <cmath>
#include "gtest/gtest.h"

#include "StrategyOptimal.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyCmaxOptimal, solve_1)
{
	SystemStationary tasks = get_stationary_1();

	tasks.print();

	StrategyOptimal stg;
	stg.set_input(&tasks);
	stg.start();

	EXPECT_NEAR(stg.get_makespan(), 626.25, 1e-6);
}

TEST(StrategyCmaxOptimal, solve_2)
{
	SystemStationary tasks = get_stationary_2();

	tasks.print();

	StrategyOptimal stg;
	stg.set_input(&tasks);
	stg.start();

	std::cerr << "stg.get_makespan() = " << stg.get_makespan() << std::endl;
	EXPECT_NEAR(stg.get_makespan(), 236.013, 1e-4);
}

