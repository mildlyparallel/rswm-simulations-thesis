#include "gtest/gtest.h"

#include "StrategyFair.hh"
#include "StrategyOptimal.hh"
#include "TaskGraph.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyFCSTest, solve_1)
{
	SystemStationary tasks = get_stationary_2();

	StrategyFair stg;
	stg.set_input(&tasks);
	stg.set_interval(1);
	stg.start();

	std::cerr << "stg = " << stg.get_makespan() << std::endl;
}

