#include "gtest/gtest.h"

#include "Bitset.hh"

using namespace ::testing;

TEST(BitsetTest, ctor)
{
	Bitset<8> b({1, 3, 5});
	for (size_t i = 0; i < b.size(); ++i) {
		EXPECT_EQ(b[i], (i == 1) || (i == 3) || (i == 5));
	}
}

TEST(BitsetTest, set_get_clear)
{
	Bitset<8> b;
	EXPECT_FALSE(b[3]);

	b.clear(3);
	EXPECT_FALSE(b[3]);

	b.set(3, true);
	EXPECT_TRUE(b[3]);

	b.set(3, false);
	EXPECT_FALSE(b[3]);

	b.set(3);
	EXPECT_TRUE(b[3]);

	b.clear(3);
	EXPECT_FALSE(b[3]);
}

TEST(BitsetTest, count_1)
{
	Bitset<8> b;
	EXPECT_EQ(b.count(), 0);
	EXPECT_TRUE(b.empty());

	b.set(1);
	EXPECT_EQ(b.count(), 1);

	b.set(2);
	EXPECT_EQ(b.count(), 2);

	EXPECT_EQ(b.size(), 8u);
}

TEST(BitsetTest, less_than)
{
	Bitset<8> a, b;
	EXPECT_FALSE(a < b);

	b.set(3);
	EXPECT_TRUE(a < b);

	a.set(3);
	EXPECT_FALSE(a < b);

	b.set(2);
	EXPECT_TRUE(a < b);
}

TEST(BitsetTest, swap)
{
	Bitset<8> a;
	a.set(2);

	a.swap(1, 2);

	EXPECT_TRUE(a[1]);
	EXPECT_FALSE(a[2]);
}

TEST(BitsetTest, reverse)
{
	Bitset<8> a({2, 4});

	a.reverse(1, 5);

	EXPECT_TRUE(a[1]);
	EXPECT_FALSE(a[2]);
	EXPECT_TRUE(a[3]);
	EXPECT_FALSE(a[4]);
}

TEST(BitsetTest, next_permutation)
{
	Bitset<8> a({2});

	EXPECT_TRUE(a.next_permutation(0, 3));
	EXPECT_FALSE(a[0]);
	EXPECT_TRUE(a[1]);
	EXPECT_FALSE(a[2]);

	EXPECT_TRUE(a.next_permutation(0, 3));
	EXPECT_TRUE(a[0]);
	EXPECT_FALSE(a[1]);
	EXPECT_FALSE(a[2]);

	EXPECT_FALSE(a.next_permutation(0, 3));
}

TEST(BitsetTest, next_combination)
{
	Bitset<8> a({2});

	EXPECT_TRUE(a.next_combination(0, 3));
	EXPECT_FALSE(a[0]);
	EXPECT_TRUE(a[1]);
	EXPECT_FALSE(a[2]);

	EXPECT_TRUE(a.next_combination(0, 3));
	EXPECT_FALSE(a[0]);
	EXPECT_TRUE(a[1]);
	EXPECT_TRUE(a[2]);

	EXPECT_TRUE(a.next_combination(0, 3));
	EXPECT_TRUE(a[0]);
	EXPECT_FALSE(a[1]);
	EXPECT_FALSE(a[2]);

	EXPECT_TRUE(a.next_combination(0, 3));
	EXPECT_TRUE(a[0]);
	EXPECT_FALSE(a[1]);
	EXPECT_TRUE(a[2]);
}

TEST(BitsetTest, subset_of)
{
	Bitset<8> a({2, 4});
	Bitset<8> b1({2, 4, 5});

	EXPECT_TRUE(a.subset_of(b1));
	EXPECT_TRUE(a.subset_of(a));

	b1.clear(2);
	EXPECT_FALSE(a.subset_of(b1));

	a.clear(2);
	EXPECT_TRUE(a.subset_of(b1));
}
