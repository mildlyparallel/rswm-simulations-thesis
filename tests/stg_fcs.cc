#include "gtest/gtest.h"

#include "StrategyFCS.hh"
#include "StrategyRR.hh"
#include "StrategyOptimal.hh"
#include "StrategyFair.hh"
#include "TaskGraph.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyFCSTest, solve_1)
{
	SystemStationary tasks = get_stationary_1();

	StrategyFCS stg;
	stg.set_input(&tasks);
	stg.open_log("tp-fcs-log.csv");
	stg.start();

	std::cerr << "stg = " << stg.get_makespan() << std::endl;
}

void set_comb_speed_eq(SystemStationary &tasks, Taskset ts, double val) {
	for (size_t i = 0; i < tasks.size(); ++i) {
		if (ts[i])
			tasks[i].set_rate_scale(ts, val);
	}
}

void print(SystemStationary &tasks, Taskset ts)
{
	std::cerr << ts << " {";
	bool comma = false;
	double s = 0;
	for (size_t i = 0; i < tasks.size(); ++i) {
		if (!ts[i])
			continue;
		double v = tasks[i].get_rate_scale(ts, 0);
		s += v;
		if (comma)
			std::cerr << ", ";
		comma = true;

		std::cerr << v;
	}

	std::cerr << "} sum=" << s << "\n";
}

TEST(StrategyCmaxOptimal, solve_prec_1)
{
	TaskGraph graph;

	size_t k = 3;
	size_t nr_tasks = k * k;
	graph.add_tasks(nr_tasks);

	for (size_t i = 0; i < k; ++i) {
		for (size_t j = 0; j < k; ++j) {
			size_t u = j * k + i;

			for (size_t i1 = i+1; i1 < k; ++i1) {
				for (size_t j1 = 0; j1 < k; ++j1) {
					if (j1 < j && j - j1 >= i1 - i )
						continue;

					size_t v = j1 * k + i1;
					graph.add_edge(u, v);
				}
			}
		}
	}

	graph.reduce();
	graph.complement();

	std::cerr << "graph.nr_nodes() = " << graph.nr_nodes() << std::endl;
	std::cerr << "graph.nr_edges() = " << graph.nr_edges() << std::endl;

	// auto fvs = graph.find_combinations();
	// std::cerr << "fvs.size() = " << fvs.size() << std::endl;
	// graph.write_all("comb-3");
	// graph.read_all("comb-3");

	SystemStationary tasks(nr_tasks);
	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks[i].set_work_total(1);

	for (auto &ts : tasks.feasible_combinations()) {
		bool on_diag = true;

		size_t diag = tasks.size();
		size_t diag_size = 0;
		for (size_t i = 0; i < tasks.size(); ++i) {
			if (!ts[i])
				continue;

			if (diag == tasks.size()) {
				if (i < k)
					diag_size = i + 1;
				else if (i % k == k - 1)
					diag_size = k - i / k;

				diag = i;
				continue;
			}

			if (diag + k - 1 == i) {
				diag = i;
				continue;
			}

			on_diag = false;
			break;
		}

		std::cerr << ts;

		if (on_diag && diag_size == ts.count()) {
			double v = 1.;
			std::cerr << " DIAG " << v*ts.count() << std::endl;
			set_comb_speed_eq(tasks, ts, v);
			continue;
		}

		double v = 1. / ts.count();
		// if (ts.count() == k)
		// 	v = 2.1 / ts.count();

		set_comb_speed_eq(tasks, ts, v);

		std::cerr << std::endl;

		// if (t1 == t2 % nr_rows && t1 == t3 % nr_rows) {
		// 	set_comb_speed_eq(tasks, ts, 1./3);
		// } else {
		// 	set_comb_speed_eq(tasks, ts, 1);
		// }
	}

	tasks.print();

	// StrategyFCS stg_fcs;
	// stg_fcs.set_input(&tasks);
	// stg_fcs.start();
	// std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;

	StrategyRR stg_rr;
	stg_rr.set_input(&tasks);
	stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	stg_rr.set_interval(0.1);
	stg_rr.start();
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	StrategyOptimal stg_opt;
	stg_opt.set_input(&tasks);
	stg_opt.start();
	std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;

	double rho = stg_rr.get_makespan() / stg_opt.get_makespan();
	// double rho = stg_fcs.get_makespan() / stg_opt.get_makespan();
	std::cerr << "rho = " << rho << std::endl;
}

TEST(StrategyCmaxOptimal, solve_prec_2)
{
	TaskGraph graph;

	size_t k = 4;
	size_t nr_tasks = 2 * k + 1;
	graph.add_tasks(nr_tasks);

	for (size_t i = 0; i < k; ++i) {
		graph.add_edge(2 * i + 1, 2 * i + 2);
	}

	// graph.reduce();
	// graph.complement();

	std::cerr << "graph.nr_nodes() = " << graph.nr_nodes() << std::endl;
	std::cerr << "graph.nr_edges() = " << graph.nr_edges() << std::endl;

	// auto fvs = graph.find_combinations();
	// std::cerr << "fvs.size() = " << fvs.size() << std::endl;

	// graph.write_all("comb-4");
	// graph.read_all("comb-3");

	SystemStationary tasks(nr_tasks);
	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	double b1 = 100;
	double b2 = b1 * (k - 1);

	Taskset row1_mask, row2_mask;

	tasks[0].set_work_total(b2);
	row2_mask.set(0, true);

	for (size_t i = 0; i < k; ++i) {
		tasks[2 * i + 1].set_work_total(b1);
		row1_mask.set(2 * i + 1, true);

		tasks[2 * i + 2].set_work_total(b2);
		row2_mask.set(2 * i + 2, true);
	}

	for (auto &ts : tasks.feasible_combinations()) {
		Taskset r1 = ts & row1_mask;
		Taskset r2 = ts & row2_mask;

		if (ts.count() == 1) {
			set_comb_speed_eq(tasks, ts, 1);
		} else if (r1 && !r2) {
			double v = 1. / ts.count();
			set_comb_speed_eq(tasks, ts, v);
		} else if (!r1 && r2) {
			double v = 1;
			// if (ts.count() == k + 1)
			// 	v = 1. - 1. / (k + 1);
			set_comb_speed_eq(tasks, ts, v);
		} else if (r1.count() == 1 && r2.count() == 1) {
			double v1 = 1. / k;
			double v2 = 1 - v1;

			for (size_t i = 0; i < tasks.size(); ++i) {
				if (!ts[i])
					continue;
				if (r1[i])
					tasks[i].set_rate_scale(ts, v1);
				if (r2[i])
					tasks[i].set_rate_scale(ts, v2);
			}
		} else {
			double v = 1e-3;
			set_comb_speed_eq(tasks, ts, v);
		}

		print(tasks, ts);
	}

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	std::vector<Taskset> s_min;
	s_min.push_back(row1_mask);
	s_min.push_back(row2_mask);

	StrategyFCS fcs_min;
	// fcs_min.set_acq_fun(StrategyFCS::SpeedEfficiencySq);
	fcs_min.set_input(&tasks);
	bool min_ok = fcs_min.validate(s_min);
	assert(min_ok);
	std::cerr << "fcs_min.get_makespan() = " << fcs_min.get_makespan() << std::endl;

	double makespan_min = fcs_min.get_makespan();

	std::vector<Taskset> s_max;
	for (size_t i = 0; i < k; ++i) {
		s_max.push_back({i * 2, i * 2 + 1});
	}
	s_max.push_back({2 * k});

	StrategyFCS fcs_max;
	fcs_max.set_input(&tasks);
	// fcs_max.set_acq_fun(StrategyFCS::SpeedEfficiencySq);

	bool max_ok = fcs_max.validate(s_max);
	assert(max_ok);
	std::cerr << "fcs_max.get_makespan() = " << fcs_max.get_makespan() << std::endl;

	double makespan_max = fcs_max.get_makespan();

	double rho = makespan_max / makespan_min;
	std::cerr << "rho = " << rho << std::endl;

	// StrategyRR stg_rr;
	// stg_rr.set_input(&tasks);
	// stg_rr.set_acq_fun(StrategyRR::ScaledSpeed3);
	// stg_rr.set_interval(10);
	// stg_rr.start();
	// std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// stg_fcs.start();
	// std::cerr << "stg_fcs.get_makespan() = " << stg_fcs.get_makespan() << std::endl;
    //
	// StrategyOptimal stg_opt;
	// stg_opt.set_input(&tasks);
	// stg_opt.start();
	// std::cerr << "stg_opt.get_makespan() = " << stg_opt.get_makespan() << std::endl;
    //
	// std::cerr << "std::sqrt(nr_tasks) = " << std::sqrt(nr_tasks) << std::endl;
    //
	// double rho = stg_fcs.get_makespan() / stg_opt.get_makespan();
	// std::cerr << rho << std::endl;
}

TEST(StrategyCmaxOptimal, solve_prec_3)
{
	TaskGraph graph;

	size_t k = 4;
	size_t nr_tasks = 2 * k + 1;
	graph.add_tasks(nr_tasks);

	for (size_t i = 0; i < k; ++i) {
		graph.add_edge(2 * i + 1, 2 * i + 2);
	}

	std::cerr << "graph.nr_nodes() = " << graph.nr_nodes() << std::endl;
	std::cerr << "graph.nr_edges() = " << graph.nr_edges() << std::endl;

	// graph.write_all("comb-4");

	SystemStationary tasks(nr_tasks);
	tasks.set_precedence(graph);
	tasks.init_base_rate(1);

	double b1 = 100;
	double b2 = b1 * (k - 1);

	Taskset row1_mask, row2_mask;

	tasks[0].set_work_total(b2);
	row2_mask.set(0, true);

	double b22 = 0;
	double r1_work = 0;
	for (size_t i = 0; i < k; ++i) {
		b22 += b1 * (k - 1) / (k - i + 1);
		tasks[2 * i + 1].set_work_total(b22);
		row1_mask.set(2 * i + 1, true);
		r1_work += b22;

		tasks[2 * i + 2].set_work_total(b2);
		row2_mask.set(2 * i + 2, true);
	}

	for (auto &ts : tasks.feasible_combinations()) {
		Taskset r1 = ts & row1_mask;
		Taskset r2 = ts & row2_mask;

		if (ts.count() == 1) {
			set_comb_speed_eq(tasks, ts, 1);
			continue;
		}

		if (r1 && !r2) {
			set_comb_speed_eq(tasks, ts, 1. / (ts.count() ));
			continue;
		}

		if (!r1 && r2) {
			set_comb_speed_eq(tasks, ts, 1);
			continue;
		}

		if (r1.count() == 1 && r2.count() == 1) {
			double v1 = 1. / k;
			double v2 = 1 - v1;

			for (size_t i = 0; i < tasks.size(); ++i) {
				if (!ts[i])
					continue;
				if (r1[i])
					tasks[i].set_rate_scale(ts, v1);
				if (r2[i])
					tasks[i].set_rate_scale(ts, v2);
			}
		} else {
			double v = 1e-3;
			set_comb_speed_eq(tasks, ts, v);
		}
	}

	// double eps = 1. / (k + 1);

	for (size_t i = 0; i < k; ++i) {
		Taskset ts;
		ts.set(2*i);

		for (size_t j = i; j < k; ++j) {
			ts.set(2*j+1);
		}

		Taskset ts2;
		for (auto &ts2 : tasks.feasible_combinations()) {
			if (!ts2[2*i])
				continue;
			if (!ts2.subset_of(ts))
				continue;
			if (ts2.count() == 1)
				continue;

			tasks[2*i].set_rate_scale(ts2, 1);
			for (size_t j2 = 0; j2 < nr_tasks; ++j2) {
				if (ts2[j2] && j2 != 2*i)
					tasks[j2].set_rate_scale(ts2, 1./ts.count());
			}
		}
	}

	for (auto &ts : tasks.feasible_combinations()) {
		print(tasks, ts);
	}

	std::cerr << "Validating ";
	bool tasks_ok = tasks.validate();
	if (tasks_ok) {
		std::cerr << " Ok" << std::endl;
	} else {
		std::cerr << " Not valid" << std::endl;
		std::abort();
	}

	double dt = 10;
	std::vector<Taskset> sched;

	for (size_t i = 0; i < k+1; ++i) {
		Taskset ts;
		ts.set(2*i);
		for (size_t j = i; j < k; ++j) {
			ts.set(2*j + 1);
		}

		for (size_t t = 0; t < b2 / dt; ++t) {
			sched.push_back(ts);
		}
	}

	StrategyRR stg_rr;
	stg_rr.set_interval(dt);
	stg_rr.set_acq_fun(StrategyRR::LinearSpeed);
	stg_rr.set_input(&tasks);
	stg_rr.validate(sched);
	// stg_rr.start();
	std::cerr << "stg_rr.get_makespan() = " << stg_rr.get_makespan() << std::endl;

	// StrategyFair stg_fair;
	// stg_fair.set_interval(dt);
	// stg_fair.set_input(&tasks);
	// stg_fair.start();
	// std::cerr << "stg_fair.get_makespan() = " << stg_fair.get_makespan() << std::endl;

	double c_best = r1_work + (b2);
	std::cerr << "c_best = " << c_best << std::endl;
	double rho_rr = stg_rr.get_makespan() / c_best;
	std::cerr << "rho_rr = " << rho_rr << std::endl;

	// double rho_fair = stg_fair.get_makespan() / c_best;
	// std::cerr << "rho_fair = " << rho_fair << std::endl;
}

