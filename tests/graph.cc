#include "gtest/gtest.h"

#include "Graph.hh"

using namespace ::testing;

TEST(GraphTest, nodes_init)
{
	Graph g(3);
	EXPECT_EQ(g.nr_nodes(), 3u);
	EXPECT_EQ(g.nr_edges(), 0u);
}

TEST(GraphTest, nodes_added)
{
	Graph g(3);
	size_t u = g.add_node();

	EXPECT_EQ(u, 3u);
	EXPECT_EQ(g.nr_nodes(), 4u);
	EXPECT_EQ(g.nr_edges(), 0u);
}

TEST(GraphTest, edges__added)
{
	Graph g(3);
	g.add_edge(0, 1);
	g.add_edge(1, 2);

	EXPECT_EQ(g.nr_edges(), 2u);
}


TEST(GraphTest, edges_added_repeated)
{
	Graph g(3);
	g.add_edge(0, 1);
	g.add_edge(0, 1);

	EXPECT_EQ(g.nr_edges(), 1u);
}

TEST(GraphTest, edges__have_edge)
{
	Graph g(3);
	g.add_edge(0, 1);
	g.add_edge(1, 2);

	EXPECT_TRUE(g.have_edge(0, 1));
	EXPECT_TRUE(g.have_edge(1, 2));
	EXPECT_FALSE(g.have_edge(1, 0));
	EXPECT_FALSE(g.have_edge(2, 1));
	EXPECT_FALSE(g.have_edge(0, 2));
}

TEST(GraphTest, reduce__nothing)
{
	Graph g(3);
	g.add_edge(0, 1);
	g.add_edge(1, 2);

	g.reduce();

	EXPECT_EQ(g.nr_edges(), 2);
	EXPECT_TRUE(g.have_edge(0, 1));
	EXPECT_TRUE(g.have_edge(1, 2));
}

TEST(GraphTest, reduce__simple)
{
	Graph g(3);
	g.add_edge(0, 1);
	g.add_edge(1, 2);
	g.add_edge(0, 2);

	ASSERT_EQ(g.nr_edges(), 3);

	g.reduce();

	EXPECT_EQ(g.nr_edges(), 2);

	EXPECT_TRUE(g.have_edge(0, 1));
	EXPECT_TRUE(g.have_edge(1, 2));
	EXPECT_FALSE(g.have_edge(0, 2));
}

TEST(GraphTest, reduce__chain_multiple)
{
	Graph g(6);
	size_t e1[][2] = {{0,1}, {1,2}, {2,3}, {3,4}, {4,5}};
	size_t e2[][2] = {{1,5}, {2,5}, {3,5}};

	for (auto [u, v] : e1) {
		g.add_edge(u, v);
	}

	for (auto [u, v] : e2) {
		g.add_edge(u, v);
	}

	ASSERT_EQ(g.nr_edges(), 8);

	g.reduce();

	EXPECT_EQ(g.nr_edges(), 5);

	for (auto [u, v] : e1) {
		EXPECT_TRUE(g.have_edge(u, v));
	}

	for (auto [u, v] : e2) {
		EXPECT_FALSE(g.have_edge(u, v));
	}
}

TEST(GraphTest, reduce__tree)
{
	Graph g(7);
	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {1,4}, {2,5}, {2,6}};
	size_t e2[][2] = {{0,3}, {0,4}, {0,6}};

	for (auto [u, v] : e1) {
		g.add_edge(u, v);
	}

	for (auto [u, v] : e2) {
		g.add_edge(u, v);
	}

	ASSERT_EQ(g.nr_edges(), 9);

	g.reduce();

	EXPECT_EQ(g.nr_edges(), 6);

	for (auto [u, v] : e1) {
		EXPECT_TRUE(g.have_edge(u, v));
	}

	for (auto [u, v] : e2) {
		EXPECT_FALSE(g.have_edge(u, v));
	}
}

TEST(GraphTest, reduce__dag)
{
	Graph g(4);
	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {2,3}};
	size_t e2[][2] = {{0,3}};

	for (auto [u, v] : e1) {
		g.add_edge(u, v);
	}

	for (auto [u, v] : e2) {
		g.add_edge(u, v);
	}

	ASSERT_EQ(g.nr_edges(), 5);

	g.reduce();

	EXPECT_EQ(g.nr_edges(), 4);

	for (auto [u, v] : e1) {
		EXPECT_TRUE(g.have_edge(u, v));
	}

	for (auto [u, v] : e2) {
		EXPECT_FALSE(g.have_edge(u, v));
	}
}

TEST(GraphTest, complement__chain)
{
	Graph g(5);
	size_t e1[][2] = {{0,1}, {1,2}, {2,3}, {3,4}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	for (size_t u = 0; u < g.nr_edges(); ++u) {
		for (size_t v = u + 1; v < g.nr_edges(); ++v) {
			EXPECT_TRUE(g.reachable(u, v));
		}
	}
}

TEST(GraphTest, complement__tree)
{
	Graph g(7);
	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {1,4}, {2,5}, {2,6}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	for (size_t v = 1; v < g.nr_edges(); ++v)
		EXPECT_TRUE(g.reachable(0, v));
}

TEST(GraphTest, is_tree__tree_whole)
{
	Graph g(7);
	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {1,4}, {2,5}, {2,6}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	Graph::Nodeset nodes;
	for (size_t i = 0; i < g.nr_edges(); ++i)
		nodes.set(i);

	EXPECT_TRUE(g.is_tree(nodes));
}

TEST(GraphTest, is_tree__tree_subtree)
{
	Graph g(7);
	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {1,4}, {2,5}, {2,6}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	Graph::Nodeset n1({1, 3, 4, 2, 5, 6});

	EXPECT_TRUE(g.is_tree(n1));
}

class TreeTest : public testing::TestWithParam<std::tuple<bool, Graph::Nodeset>>
{
public:
	Graph create_graph()
	{
		Graph g(13);
		size_t e1[][2] = {
			{0,1},
			{1,2},
			{2,3},
			{3,4},
			{5,6},
			{6,7},
			{7,0},

			{3,8},
			{8,9},
			{9,10},
			{10,11},
			{11,6},

			{9,4},

			{3,0},

			{2,12},
			{12,0},
		};

		for (auto [u, v] : e1)
			g.add_edge(u, v);

		return g;
	}

};

TEST_P(TreeTest, is_tree) {
	auto [tree, nodes] = GetParam();

	Graph g = create_graph();
	EXPECT_EQ(g.is_tree(nodes), tree);
}

std::tuple<bool, Graph::Nodeset> tree_test_data[] = {
	{false, {0, 1, 2, 3, 4, 5, 6, 7}},
	{false, {0, 1, 2, 12}},
	{false, {0, 1, 2, 3}},
	{false, {3, 8, 9, 10, 11, 6, 0, 1, 2}},
	{false, {3, 8, 9, 4, 5, 6, 7, 0, 1, 2}},
	{false, {0, 1, 2, 12, 4, 5, 6, 10, 9}},
	{false, {0, 1, 2, 3, 4, 6, 7}},
	{true, {0, 1, 2}},
	{true, {0, 1, 12, 9, 4, 5, 6, 7}},
	{true, {0}},
	{true, {3}},
	{true, {1, 2, 12, 3, 8, 9, 4, 5, 6}},
};

INSTANTIATE_TEST_SUITE_P(GraphTest, TreeTest, testing::ValuesIn(tree_test_data));

TEST(GraphTest, fvs) {
	Graph g(4);

	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {2,3}, {1,2}, {2,1}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.list_loops();
	const auto &fvs = g.list_maximal_fvs();

	EXPECT_EQ(fvs.size(), 2);
	for (auto &nodes : fvs) {
		EXPECT_TRUE(nodes[1] ^ nodes[2]);
		EXPECT_TRUE(nodes[0]);
		EXPECT_TRUE(nodes[3]);
		EXPECT_TRUE(g.is_tree(nodes));
	}
}

TEST(GraphTest, reachable)
{
	Graph g(4);

	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {2,3}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	EXPECT_TRUE(g.reachable(0, 3));
	EXPECT_TRUE(g.reachable(0, 1));
	EXPECT_TRUE(g.reachable(0, 2));
	EXPECT_TRUE(g.reachable(1, 3));
	EXPECT_TRUE(g.reachable(2, 3));
	EXPECT_FALSE(g.reachable(1, 2));
	EXPECT_FALSE(g.reachable(2, 1));

	Graph::Nodeset n1({0, 3, 2});
	EXPECT_TRUE(g.reachable(n1));

	Graph::Nodeset n2({1, 2});
	EXPECT_FALSE(g.reachable(n2));
}

TEST(GraphTest, reachable_any__both_directions)
{
	Graph g(4);

	size_t e1[][2] = {{0,1}, {2,3}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	Graph::Nodeset n1({0, 3}), n2({1, 2});
	EXPECT_TRUE(g.reachable_any(n1, n2));
	EXPECT_TRUE(g.reachable_any(n2, n1));
}

TEST(GraphTest, reachable_any__single_directions)
{
	Graph g(4);

	size_t e1[][2] = {{0,1}, {2,3}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	Graph::Nodeset n1({0, 2}), n2({1, 3});
	EXPECT_TRUE(g.reachable_any(n1, n2));
	EXPECT_FALSE(g.reachable_any(n2, n1));
}

TEST(GraphTest, reachable_any__single_directions_single)
{
	Graph g(4);

	size_t e1[][2] = {{0,1}, {2,3}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	Graph::Nodeset n1({0}), n2({1, 3}), n3({2});
	EXPECT_TRUE(g.reachable_any(n1, n2));
	EXPECT_FALSE(g.reachable_any(n2, n1));

	EXPECT_TRUE(g.reachable_any(n3, n2));
	EXPECT_FALSE(g.reachable_any(n1, n3));

	EXPECT_FALSE(g.reachable_any(n1, n3));
}

TEST(GraphTest, is_leaf)
{
	Graph g(5);

	size_t e1[][2] = {{0,1}, {1,2}, {1,3}, {0,4}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	EXPECT_TRUE(g.is_leaf(2));
	EXPECT_TRUE(g.is_leaf(3));
	EXPECT_TRUE(g.is_leaf(4));
	EXPECT_FALSE(g.is_leaf(0));
	EXPECT_FALSE(g.is_leaf(1));

	EXPECT_TRUE(g.is_leaf({2, 4}));
	EXPECT_TRUE(g.is_leaf({2, 3, 4}));
	EXPECT_FALSE(g.is_leaf({1, 3, 4}));
}

TEST(GraphTest, is_root)
{
	Graph g(7);

	size_t e1[][2] = {{0,1}, {1,2}, {3,1}, {4,5}, {6,2}};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	g.complement();

	EXPECT_TRUE(g.is_root(0));
	EXPECT_TRUE(g.is_root(3));
	EXPECT_TRUE(g.is_root(4));
	EXPECT_TRUE(g.is_root(6));

	EXPECT_FALSE(g.is_root(1));
	EXPECT_FALSE(g.is_root(2));
	EXPECT_FALSE(g.is_root(5));

	EXPECT_TRUE(g.is_root({0, 3, 6, 4}));
	EXPECT_TRUE(g.is_root({0, 6, 4}));
	EXPECT_TRUE(g.is_root({6, 4}));
	EXPECT_FALSE(g.is_root({6, 1, 4}));
	EXPECT_FALSE(g.is_root({2, 4}));
}

// TEST(GraphTest, read_write) {
// 	Graph g1(7);
//
// 	size_t e1[][2] = {{0,1}, {0,2}, {1,3}, {2,3}, {1,2}, {2,1}, {3, 4}, {4, 5}, {5, 6}};
//
// 	for (auto [u, v] : e1)
// 		g1.add_edge(u, v);
//
// 	g1.complement();
//
// 	auto fvs1 = g1.list_maximal_fvs();
//
// 	g1.write("test.graph");
//
// 	Graph g2;
// 	g2.read("test.graph");
//
// 	EXPECT_EQ(g1.nr_nodes(), g2.nr_nodes());
// 	EXPECT_EQ(g1.nr_edges(), g2.nr_edges());
//
// 	for (auto [u, v] : e1)
// 		g2.have_edge(u, v);
//
// 	for (size_t u = 0; u < g1.nr_nodes(); ++u) {
// 		for (size_t v = 0; v < g1.nr_nodes(); ++v) {
// 			EXPECT_EQ(g1.reachable(u, v), g2.reachable(u, v));
// 		}
// 	}
//
// 	auto fvs2 = g2.list_maximal_fvs();
//
// 	EXPECT_EQ(fvs1.size(), fvs2.size());
//
// 	for (size_t i = 0; i < fvs1.size(); ++i) {
// 		EXPECT_EQ(fvs1[i], fvs2[i]);
// 	}
// }

// TEST(GraphTest, list_loops_3)
// {
// 	Graph g(3);
// 	g.add_edge(0, 1);
// 	g.add_edge(1, 2);
// 	g.add_edge(2, 0);
//
// 	auto loops = g.list_loops();
//
// 	EXPECT_EQ(loops.size(), 1u);
// 	EXPECT_EQ(loops[0].count(), 3u);
// }

// TEST(GraphTest, list_loops_3_2)
// {
// 	Graph g(3);
//
// 	size_t e1[][2] = {{0,1}, {1,2}, {2,0}, {2,1}};
// 	for (auto [u, v] : e1)
// 		g.add_edge(u, v);
//
// 	auto loops = g.list_loops();
//
// 	ASSERT_EQ(loops.size(), 2u);
// }

// TEST(GraphTest, list_loops_9)
// {
// 	Graph g(9);
//
// 	size_t e1[][2] = {
// 		{0,1}, {1,2}, {2,3}, {3,4}, {4,5}, {5,0},
// 		{2,3}, {4,2},
// 		{2,6}, {6,7}, {7,8}, {8,4}
// 	};
//
// 	for (auto [u, v] : e1)
// 		g.add_edge(u, v);
//
// 	auto loops = g.list_loops();
//
// 	for (auto &loop : loops) {
// 		std::cerr << loop << std::endl;
// 	}
//
// 	ASSERT_EQ(loops.size(), 4u);
//
// 	Graph::Nodeset expected[] = {
// 		{2,3,4},
// 		{2,4,6,7,8},
// 		{0,1,2,3,4,5},
// 		{0,1,2,4,5,6,7,8},
// 	};
//
// 	for (auto e : expected) {
// 		bool found = false;
//
// 		for (auto l : loops) {
// 			if (l == e) {
// 				found = true;
// 				break;
// 			}
// 		}
//
// 		EXPECT_TRUE(found);
// 	}
// }

TEST(GraphTest, find_loop__no_loop_1) {
	Graph g(9);

	size_t e1[][2] = {
		{0,1}, {1,2},
		{3,4}, {4,5},
		{6,7}, {7,8},
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_FALSE(g.have_loop());
}

TEST(GraphTest, find_loop__no_loop_2) {
	Graph g(9);

	size_t e1[][2] = {
		{0,1}, {1,2},
		{3,4}, {4,5},
		{6,7}, {7,8},

		{3,1}, {1,5},
		{4,2}, 
		{3,7},
		{4,8}
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_FALSE(g.have_loop());
}

TEST(GraphTest, find_loop__with_loop) {
	Graph g(3);

	size_t e1[][2] = {
		{0,1}, {1,2}, {2,0}
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_TRUE(g.have_loop());
}

TEST(GraphTest, find_loop__with_loop_2) {
	Graph g(9);

	size_t e1[][2] = {
		{0,1}, {1,2}, {2,3}, {3,4}, {0,4},
		{4,5}, {5,6}, {6,4},
		{2,7}, {7,8}, {2,8}
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_TRUE(g.have_loop());
}

TEST(GraphTest, find_loop__with_loop_3) {
	Graph g(6);

	size_t e1[][2] = {
		{0,1}, {1,2}, {0,2},
		{3,4}, {4,5}, {5,3}
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_TRUE(g.have_loop());
}

TEST(GraphTest, find_loop_remove_edges) {
	Graph g(9);

	size_t e1[][2] = {
		{0,1}, {1,2}, {2,3}, {3,4}, {0,4},
		{4,5}, {5,6}, {6,4},
		{2,7}, {7,8}, {2,8}
	};

	for (auto [u, v] : e1)
		g.add_edge(u, v);

	EXPECT_TRUE(g.have_loop());

	g.remove_edges(4);

	EXPECT_FALSE(g.have_loop());
}
