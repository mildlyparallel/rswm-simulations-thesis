#include "gtest/gtest.h"
#include <random>

#include "StrategySearch.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategySearchTest, solve_1)
{
	static const size_t nr_tasks = 4;
	SystemStationary tasks(nr_tasks);
	tasks.init_base_rate(3, 3);

	// double slopes[nr_tasks] = {
	// 	0.05, 0.1, 0.22, 0.2, 0.2
	// };
    //
	// auto scale_fn = [&] (Task::taskset ts, const Task &t) {
	// 	return 1. - slopes[t.get_id()] * (ts.count() - 1);
	// };
    //
	// tasks.init_rate_scale(scale_fn);

	std::default_random_engine gen;
	std::uniform_real_distribution<double> distribution(0.5, 1);

	auto scale_fn = [&] (size_t) {
		return distribution(gen);
	};

	tasks.init_scale_rnd(scale_fn);

	double work_total[nr_tasks] = {
		180, 626, 937, 1043
	};

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks.init_total_work(i, work_total[i]);

	tasks.print();

	StrategySearch strategy;
	strategy.set_acc_fun(StrategySearch::UCB);
	strategy.set_quartile(0.2);
	strategy.add(tasks);
	strategy.open_log("tp-sampling-log.csv");
	strategy.start();

	std::cerr << "strategy. = " << strategy.get_makespan() << std::endl;
}

